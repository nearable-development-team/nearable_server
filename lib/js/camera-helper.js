
class CommonHelper {
    static GetQueryStringByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    static FormatedNumber(number) {
        return "+" + (number.replace(/ /g, "").replace(")", "").replace("(", "").replace("+", "").replace("-", ""))
    }
}
class PhotoService {
    static getPhotoFromAR() {
        return new Promise((resolve) => {
            let canvas = document.querySelector("canvas")
            let canvas1 = document.createElement("canvas");
            canvas1.width = canvas.width;
            canvas1.height = canvas.height;

            let context = canvas1.getContext('2d');
            context.drawImage(document.querySelector("#arjs-video"), 0, 0, canvas1.width, canvas1.height);
            canvas1.toBlob(async (blob) => {
                resolve(await PhotoService.convertBlobToBase64(blob))
            }, 'image/png');

        })
    }
    static async getPhotoFromInput($event) {
        return await PhotoService.convertBlobToBase64($event.target.files[0]);
    }
    static async convertBlobToBase64(blob) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader;
            reader.onerror = reject;
            reader.onload = () => {
                resolve(reader.result);
            };
            reader.readAsDataURL(blob);
        })
    }
    static uploadPhoto(currentFolderName, sharedFolderId,
        base64, authorId = "") {
        return new Promise(async (resolve, reject) => {
            const fileName = new Date().getTime() + '.png';
            var userInfo = AuthService.GetUserInfoLocal();
            let fileUploadRequest = {
                Name: fileName,
                Base64: base64,
                FolderName: currentFolderName
            };
            let requestType = ''
            if (!sharedFolderId) {
                fileUploadRequest.UserId = authorId;
                requestType = "uploadbase64fileTomultifolder";
            } else {
                fileUploadRequest.SharedFolderId = sharedFolderId;
                requestType = 'uploadbase64filebysharedid';
            }

            if (userInfo.login) {
                fileUploadRequest.UploadedByUserId = userInfo.Id;
            } else {
                fileUploadRequest.UploadedByUserId = "0";
            }

            let response = await HttpService.Post(requestType, fileUploadRequest);
            resolve(response);
        });
    }
    static async getImagesByFolder(FolderName, folderId) {
        let userInfo = await AuthService.GetUserInfoLocal();
        let response = await HttpService.Post("getimages", { UserId: userInfo.Id, FolderName: FolderName, FolderId: folderId });
        return response;
    }
};

class HttpService {
    // "http://localhost/nearable/nearable_server/api.php?type=";//
    static Get(relativeUrl) {
        return new Promise(async (resolve, reject) => {
            let response = await fetch("https://geogifts.app/api.php?type=" + relativeUrl);
            if (response.ok) {
                var rs = (await response.json());
                if (rs.Status == "1") {
                    resolve(rs.Result);
                } else {
                    reject(rs.Message);
                }
            } else {
                reject(response.text);
            }
        })
    }

    static Post(relativeUrl, payload) {
        return new Promise(async (resolve, reject) => {
            let response = await fetch("https://geogifts.app/api.php?type=" + relativeUrl, {
                method: "post",
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify(payload)
            });
            if (response.ok) {
                var rs = (await response.json());
                if (rs.Status == "1") {
                    resolve(rs.Result);
                } else {
                    reject(rs.Message);
                }
            } else {
                reject(response.text);
            }
        })
    }
}
class AuthService {
    static GetUserInfoLocal() {
        var userId = document.querySelector("#h_userId").value;

        return {
            Id: userId,
            login: userId > 0,
        }
    }

    static ConfirmUserEmailId() {
        return HttpService.Post("ConfirmUserEmailId", {});
    }
};

class FolderService {
    static async GetAllFolder() {
        let userInfo = AuthService.GetUserInfoLocal();
        let response = await HttpService.Post("getfoldersbyuser", { UserId: userInfo.Id });
        return response;
    }
    static isFolderAchived(folder) {
        console.log(folder)
        return folder.IsArchived == "1";
    }
    static IsCurrentUserAuthor(folder) {
        let userInfo = AuthService.GetUserInfoLocal();
        return userInfo.Id == folder.AuthorId;
    }
    static async GetFoldersByLocation(userLocation) {
        return await (await HttpService.Post("getfolderbylocation", userLocation)).Folders;

    }
    static async ArchiveFolder(userFolderId) {
        cacheData.myNearbyList.Folders.filter(function (x) { return userFolderId == x.UserFolderId })[0].IsArchived = "1";
        return await HttpService.Post("ArchiveFolder", { Id: userFolderId });
    }

    static async AuthenticateFolder(autdetail) {
        return await HttpService.Post("AuthenticateFolder", autdetail);
    }
    static async GetFolderById(autdetail) {
        return await HttpService.Post("GetFolderById", autdetail);
    }
    static async AddSharedFolder(req) {
        var addSharedFolderRequest = {
            SharedFolderId: req.sharedId,
            UserId: req.userId,
            Markers: req.markers
        };
        var result = await HttpService.Post("JoinByUnquieFolderId", addSharedFolderRequest);
        if (result == 0) {
            alert("This Geo Gift was already redeemed!!");

        }
        else if (result > 0) {
            $("#ufd" + userfolder.UserFolderId).remove();
            alert("Geo Credits have been added to your account");
        }
        else {
            if (result == -1) {
                alert("Gift can't be redeemed by the owner");
            }
            else {
                alert("Gift can't be redeemed");
            }

        }
    }

}

UI = {
    customerLocationMarker: null,
    ConfirmUserEmailId: () => {
        AuthService.ConfirmUserEmailId().then(() => {
            $("#EmailconfirmationPop").remove();
        })
    },

    ViewMarkerClick: async (userfolder) => {
        var folderDetail = await FolderService.GetFolderById({ userFolderId: userfolder.folderId });
        if (folderDetail.IsPrivateFence == "1") {
            var authResult = await UI.VerifyIdentityDetails(userfolder.folderId);
            if (authResult.IsValid) {
                UI.openRedeemGiftPopUp(folderDetail, userfolder);
            }
            else {
                alert(authResult.ErrorMessage);
            }
        } else {
            UI.openRedeemGiftPopUp(folderDetail, userfolder)
        }
    },
    openRedeemGiftPopUp: function (folderDetail, userfolder) {
        $("#giftFoundAmount").text(folderDetail.AmountAttached);
        $("#giftFoundText").text("You found a Geo Credit!");
        if (folderDetail.AmountType.toLowerCase() == "point") {
            $("#nearbyViewMarkerModalRedeemBtn").text("REDEEM POINTS")
        } else {
            $("#nearbyViewMarkerModalRedeemBtn").text("REDEEM GIFT")
        }
        $("#nearbyViewMarkerModalRedeemBtn")
            .unbind("click")
            .click(async () => {
                await FolderService.AddSharedFolder({
                    sharedId: folderDetail.SharedFolderId, userId: $("#h_userId").val(),
                    markers: MapService.GenerateMapPoints({ latitude: userfolder.latitude, longitude: userfolder.longitude }, 50, 5).map((vl) => {
                        return {
                            FolderName: "Geo",
                            Longitude: vl.longitude,
                            Latitude: vl.latitude,
                            FenceAreaInFeet: 0,
                            FolderIcon: "",
                            IsPrivateFence: 0,
                            PhoneNumber: "",
                            Email: "",
                            WorthValue: 1,
                            GiftAttachedType: "Point"
                        }
                    })
                });
                M.Modal.getInstance(document.querySelector("#nearbyViewMarkerModal")).close();
            });

        M.Modal.getInstance(document.querySelector("#nearbyViewMarkerModal")).open();
    },
    VerifyIdentityDetails: (userfolderId) => {

        return new Promise(async (resolve, reject) => {
            var authResult = {};
            var tryCount = 0;
            do {
                var identityDetails = await UI.GetIdentityDetails();
                authResult = await FolderService.AuthenticateFolder({
                    UserfolderId: userfolderId,
                    PhoneNumber: identityDetails.number,
                    Token: identityDetails.token
                });
                if (authResult.Id > 0) {
                    resolve({ IsValid: true })
                    break;
                } else {
                    tryCount++;
                    if (tryCount == 3) {
                        resolve({ IsValid: false, ErrorMessage: "try Count Exceeds" });
                        break;
                    }
                    alert("Invalid Creds");
                }
            }
            while ((authResult.Id || 0) == 0);

        });


    },
    GetIdentityDetails: () => {
        return new Promise((resolve, reject) => {
            $("#nearbyPasswordVerifyModalViewBtnClick").unbind("click").click(() => {
                if ($("#nearbyPasswordVerifyModalPassword").val() && $("#nearbyPasswordVerifyModalToken").val()) {
                    resolve({ number: CommonHelper.FormatedNumber($("#nearbyPasswordVerifyModalPassword").val()), token: $("#nearbyPasswordVerifyModalToken").val() });
                    M.Modal.getInstance(document.querySelector("#nearbyPasswordVerifyModal")).close()
                } else {
                    alert("enter required information");
                }
            })
            M.Modal.getInstance(document.querySelector("#nearbyPasswordVerifyModal")).open();

        })
    },
    PlaceMarkerModalClick: async () => {


        if ($("input[name=GiftShareType]:checked").val() == "QR") {
            if (!($("#h_pattCode").val() && $("#h_qrBaseCode"))) {
                alert("please select QR image");
                return;
            }

        } else {
            if (!$("#nearbyPasswordModalMobile").val()) {
                alert("Please fill valid Mobile Number");
                return;
            }
            var regex = /1[1-9]\d+[10]/g;
            if (!regex.test($("#nearbyPasswordModalMobile").val())) {
                alert("Please fill valid US Mobile Number");
                return;
            }        
        }
        M.Modal.getInstance(document.querySelector("#nearbyGeoMarkerModal")).close();
        if ($("input[name=GiftShareType]:checked").val() == "QR") {
            await UI.CollectInfoAndDropMarker();
        } else {
            if ($("#useCurrentLocation").is(":checked")) {
                await UI.CollectInfoAndDropMarker();
            } else {
                M.Modal.getInstance(document.querySelector("#GeoMarkerLocationSelectionModal")).open();
                gmapObj = new GoogleMapComponent();
                gmapObj.init(document.querySelector("#gmapmodal")).then(async (deta) => {

                    // Create the search box and link it to the UI element.
                    const input = document.getElementById("pac-input");
                    const searchBox = new google.maps.places.SearchBox(input);
                    deta.mapRef.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                    // Bias the SearchBox results towards current map's viewport.
                    deta.mapRef.addListener("bounds_changed", () => {
                        searchBox.setBounds(deta.mapRef.getBounds());
                    });

                    let markers = [];

                    searchBox.addListener("places_changed", () => {
                        const places = searchBox.getPlaces();

                        if (places.length == 0) {
                            return;
                        }
                        // Clear out the old markers.
                        markers.forEach((marker) => {
                            marker.setMap(null);
                        });
                        markers = [];
                        // For each place, get the icon, name and location.
                        const bounds = new google.maps.LatLngBounds();
                        places.forEach((place) => {
                            if (!place.geometry || !place.geometry.location) {
                                console.log("Returned place contains no geometry");
                                return;
                            }
                            UI.customerLocationMarker.position = place.geometry.location;
                            const icon = {
                                url: place.icon,
                                size: new google.maps.Size(71, 71),
                                origin: new google.maps.Point(0, 0),
                                anchor: new google.maps.Point(17, 34),
                                scaledSize: new google.maps.Size(25, 25),
                            };
                            // Create a marker for each place.
                            markers.push(
                                new google.maps.Marker({
                                    map: deta.mapRef,
                                    icon,
                                    title: place.name,
                                    position: place.geometry.location,
                                })
                            );

                            if (place.geometry.viewport) {
                                // Only geocodes have viewport.
                                bounds.union(place.geometry.viewport);
                            } else {
                                bounds.extend(place.geometry.location);
                            }
                        });
                        deta.mapRef.fitBounds(bounds);
                    });

                    cords = await GeoLocationService.GetLocation()
                    UI.customerLocationMarker = gmapObj.AddMovableSimpleMarker(cords.latitude, cords.longitude, { draggable: true })
                });

            }
        }


    },
    SelectLocationModalClick: async () => {
        M.Modal.getInstance(document.querySelector("#GeoMarkerLocationSelectionModal")).close();
        await UI.CollectInfoAndDropMarker();
    },
    CollectInfoAndDropMarker: async () => {
        LoaderService.ShowLoader(2);
        var userInfo = AuthService.GetUserInfoLocal();
        var folder = {
            FolderName: (new Date()).toISOString(),
            UserId: userInfo.Id,
            Longitude: 0,
            Latitude: 0,
            FenceAreaInFeet: 0,
            FolderIcon: $("#slider li:eq(1)").data("value"),
            IsPrivateFence: true,
            PhoneNumber: CommonHelper.FormatedNumber(document.querySelector("#nearbyPasswordModalMobile").value),
            WorthValue: 0,
            GiftAttachedType: "Amount",
            GiftShareType: $("input[name=GiftShareType]:checked").val(),
            PattCode: $("#h_pattCode").val(),
            QRBaseImage: $("#h_qrBaseCode").val()
        };
        if (!$("#useCurrentLocation").is(":checked")) {
            console.log(UI.customerLocationMarker.position);
            folder.Longitude = UI.customerLocationMarker.position.lng()
            folder.Latitude = UI.customerLocationMarker.position.lat()
        }
        let response = await MarkerAR.dropMarker(folder);
        LoaderService.HideLoader();
        if (response.success) {
            alert("Gift placed successfully.");
            location.href = "checkout.php?userfolderId=" + response.userFolderInfo.UserFolderId;
        }
        else {
            alert("unable to drop marker");
        }
    },
    SaveImageClick: async ($event) => {

        let img = await PhotoService.getPhotoFromInput($event);

        let folderInfo = await FolderService.GetAllFolder();
        html = HtmlHlelper.CreateFolderListHtml(folderInfo.Folders);
        document.querySelector("#nearbyFolders").innerHTML = html;
        var instance = M.Modal.getInstance(document.querySelector("#nearbyfolderModal"));
        instance.open();
        var cb = async () => {
            folderList = [];
            LoaderService.ShowLoader(10);
            document.querySelectorAll("#nearbyFolders input:checked").forEach((e) => {
                folderList.push(JSON.parse(e.dataset.folder));
            });

            await PhotoService.uploadPhoto(folderList, null, img, 2)

            document.getElementById("btnNearbyfolderModalSave")
                .removeEventListener("click", cb);
            LoaderService.HideLoader();
        };

        document.getElementById("btnNearbyfolderModalSave")
            .addEventListener("click", cb);
    },

    CreateNearByClick: async () => {
        M.Modal.getInstance(document.querySelector("#nearbyGeoMarkerModal")).open();
        $('.carousel').carousel({
            fullWidth: true
        });
    },
    ViewGalleryClick: () => {
        var instance = M.Modal.getInstance(document.querySelector("#nearbyNameModal"));
        instance.open();
    },
    LoadFolders: async () => {
        cacheData.myNearbyList = await FolderService.GetAllFolder();
        UI.FilterFolder({})
    },
    LoadFolderImages: async () => {
        var folderId = CommonHelper.GetQueryStringByName("fid");
        var folderName = CommonHelper.GetQueryStringByName("fname");
        $("#nearbyName").text(folderName);
        var imagesResponse = await PhotoService.getImagesByFolder(folderName, folderId);
        var html = HtmlHlelper.CreateImageGalleryImagesHtml(imagesResponse);
        document.querySelector("#folderImages").innerHTML = html;
    },
    FilterFolder: (opts) => {

        if (!opts) opts = {};
        if (!opts.keyword) opts.keyword = "";

        if (!opts.isArchived) opts.isArchived = false;
        if (!opts.isShared) opts.isShared = false;
        console.log(opts);
        var filteredFolder = cacheData.myNearbyList.Folders.filter(item => {
            var result = item.Name.toLowerCase().indexOf(opts.keyword.toLowerCase()) > -1
                && (!opts.isArchived || FolderService.isFolderAchived(item))
                && (!opts.isShared || !FolderService.IsCurrentUserAuthor(item));
            console.log(result);
            return result;
        });
        console.log(filteredFolder)
        html = HtmlHlelper.CreateMyNearbyFolderListHtml(filteredFolder);
        document.querySelector("#folderList").innerHTML = html;

    },
    FolderTabFilter: (tabFilter) => {
        var opt = {};
        if (tabFilter == 'archived') {
            opt.isArchived = true;
            opt.isShared = false;
        }
        else if (tabFilter == "sharedOnly") {
            opt.isArchived = false;
            opt.isShared = true;
        } else {
            opt.isShared = opt.isArchived = false;
        }

        UI.FilterFolder(opt);
    },
    FolderKeywordFilter: () => {
        var keyword = $("#folderKeywordFilter").val();
        var opt = {};
        opt.keyword = keyword;
        UI.FilterFolder(opt);
    },
    LoadMapMarkers: (gmap) => {
        GeoLocationService.GetLocation().then((f) => {
            FolderService.GetFoldersByLocation(f).then((folders) => {
                folders.forEach(folder => {
                    gmap.addMarker({
                        Latitude: folder.Latitude,
                        Longitude: folder.Longitude,
                        Title: folder.Name,
                        Label: "",//folder.Name.length > 3 ? folder.Name.substr(0, 3) : folder.Name,
                        Icon: folder.FolderIcon,
                        AdditionalInfo: folder,
                    });
                });
                console.log(folders);
            });
        })
    },
    NearbyShareButtonClick: (code) => {
        ShareService.Share(code);
    },
    NearbyQRShareButtonClick: (code) => {
        ShareService.QRShare(code);
    },
    ArchiveFolder: async (userfolderId) => {
        await FolderService.ArchiveFolder(userfolderId);
        UI.FilterFolder({})
    }
};

cacheData = {
    myNearbyList: {}
}
class HtmlHlelper {
    static CreateImageGalleryImagesHtml(imagesResponse) {
        let html = "";
        console.log(imagesResponse);
        imagesResponse.Images.forEach((image) => {
            html += ` 
             <div class="col s4 ">
                <div class="gal-folder ">
               
                    <div class="img-gl"><img src="${imagesResponse.BasePath + image.FilePath}"></div>

                     <div class="pic-dl">
               
                <strong> ${image.UserName}</strong>
                <span> ${image.ImageUploadTimeStamp}</span>
                <img src="${image.UserImage}"/>
               
                </div>

                </div>
            </div>`;
        });

        return html;
    }
    static CreateMyNearbyFolderListHtml(folders) {
        let html = "";
        console.log(folders);
        folders.forEach((folder) => {
            var qrstr = ""
            if (folder.GiftShareType == "QR") {
                qrstr = `<span class="share-cl"><i onclick="UI.NearbyQRShareButtonClick('${folder.SharedFolderId}')" class="material-icons">share</i></span><span class="qr-placeholder">
                    <i class="fas fa-qrcode"></i></span>`
            } else {
                qrstr = `<span class="share-cl"><i onclick="UI.NearbyShareButtonClick('${folder.SharedFolderId}')" class="material-icons">share</i></span>`
            }

            html += ` 
               <div class="col s4 " data-folder='${JSON.stringify(folder)}'>
                <div class="gal-folder">
                    <div class="img-gl"> 
                    ${qrstr}
                    <img  onclick="location='imagegallery.php?fid=${folder.Id}&fname=${folder.Name}'" src="${cacheData.myNearbyList.BasePath + HtmlHlelper.GetFirstImageFromTopFolderImages(folder.TopImages)}">
                    </div>
                    <strong>${folder.Name}</strong>
                    <span class="gal-count">${folder.TotalFiles}`;

            if (!FolderService.isFolderAchived(folder)) {
                html += `<img onclick="UI.ArchiveFolder('${folder.UserFolderId}')" class="trash-icon" src="./images/trash.svg">`;
            }
            html += `</span>
                </div>
            </div>`;
        });

        return html;
    }
    static GetFirstImageFromTopFolderImages(imgs) {
        if (imgs.length > 0) {
            if (imgs[0].length > 0) {
                return imgs[0];
            }
        }
        return "images/noimg.jpg";
    }
    static CreateFolderListHtml(folders) {
        let html = "";
        console.log(folders);
        folders.forEach((folder) => {
            html += ` <p>
                    <label>
                        <input type="checkbox" class="filled-in" data-folder='${JSON.stringify(folder)}'/>
                        <span>${folder.Name}</span>
                    </label>
                </p>`;
        });

        return html;
    }
}

MarkerAR = {
    config: {
        EarthRadiusMeters: 6371000,
        placeMarkerUrl: "https://us-central1-nearable-ar.cloudfunctions.net/placeMarker",
        NearbyMarkersUrl: "https://us-central1-nearable-ar.cloudfunctions.net/nearbyMarkers",
        lastUpdatedLocation: { latitude: 0.0, longitude: 0.0 },
        MIN_DISTANCE:15,
        minorDistance:1000,
        nearestEntity:null
    },
    init: () => {
        MarkerAR.startGeolocation();
        MarkerAR.loadMarkers();
        document.querySelector('.menu-open-button').addEventListener('click', () => {
            if (MarkerAR.config.minorDistance <= MarkerAR.config.MIN_DISTANCE) {
                // do what you want
                alert('Hey, you just clicked on ' + nearestEntity);
            }
        });
    },

    /**
     * Load the locations of nearby gift boxes from the server.
     * 
     * @returns {Array<{latitude: number, longtiude: number}>} An array of lat/lon coordinates for these gift boxes.
     */
    startGeolocation: () => {
        if (!navigator.geolocation)
            return;

        navigator.geolocation.watchPosition(
            MarkerAR.newLocationCallback,
            error => {
                alert("Unable to load your position in `watchPosition`: " + JSON.stringify(error));
            }, {
            enableHighAccuracy: true
        });
    },

    /**
     * This method is supplied as a callback to the position API when the user's location is updated (or is found for
     * the first time).
     * 
     * A load/redisplay will only be triggered if the user's location has moved more than 8 meters from the last
     * location that caused an update (the `lastUpdatedLocation` variable).
     * 
     * @param {{ coords: { latitude: number, longitude: number }}} location Information on the current position.
     */
    newLocationCallback: (position) => {
        if (position == null || position.coords == null)
            return;
        if (MarkerAR.sphericalCosinus(position.coords, MarkerAR.config.lastUpdatedLocation) < 8.0)
            return;

        MarkerAR.config.lastUpdatedLocation.latitude = position.coords.latitude;
        MarkerAR.config.lastUpdatedLocation.longitude = position.coords.longitude;

        MarkerAR.loadMarkers(position.coords);
    },

    /**
     * Load the markers from the server and render their locations.
     * 
     * @param {{ coords: { latitude: number, longitude: number }}} location The user's current coordinates.
     * @param {boolean} retry Whether to retry loading.
     * @returns {void}
     */
    loadMarkers: (coordinates, retry) => {

        if (coordinates == null) {
            if (retry === false)
                return;

            GeoLocationService.GetLocation().then((cords) => {
                $(".gift-loader").text("loading gifts..").show();
                MarkerAR.loadMarkers(cords, false)
            })

            return;
        }
        var input = { latitude: coordinates.latitude, longitude: coordinates.longitude };
        if (localStorage.TestLocation) {
            input = JSON.parse(localStorage.TestLocation)
        }

        MarkerAR.postData(MarkerAR.config.NearbyMarkersUrl, input).then((data) => {
            const markers = data.markers;
            if (!markers || markers.length == null) {
                alert("Bad response from server when loading locations (!markers).");
                return;
            }

            MarkerAR.renderMarkerLocations(markers);
        })
    },

    /**
     * Render gift boxes at a set of lat/lon coordinates.
     * 
     * @param {Array<{latitude: number, longitude: number}} locations The locations of the gift boxes to render.
     * @returns {void}
     */
    renderMarkerLocations: (locations) => {
        console.log(locations);
        const scene = document.querySelector("a-scene");

        document.querySelectorAll(".marker")
            .forEach(marker => marker.parentElement.removeChild(marker));

        const debugText = document.querySelector("#debugText");
        if (debugText)
            debugText.innerHTML += `${locations.length} markers found.\n`;
        $(".gift-loader").text("total loaded " + location.length);
        locations.forEach(({ latitude, longitude, detail }) => {
            ((detail) => {
                const model = document.createElement("a-entity");
                model.setAttribute("class", "marker");
                model.setAttribute("Id", "ufd" + detail.folderId);
                model.setAttribute("gps-entity-place", `latitude: ${latitude}; longitude: ${longitude};`);

                model.setAttribute("gltf-model", "#" + (detail.folderIcon || "white_purple_box_logo").replace(".", "_").replace(/ /g, "_").toLowerCase());
                model.setAttribute("look-at", "[gps-camera]");
                model.setAttribute("scale", "5 5 5");
                // model.setAttribute("position", "0 0 -4");
                model.setAttribute("animation-mixer", "clip:*");
                model.addEventListener("click", () => {
                    UI.ViewMarkerClick({ folderId: detail.folderId, latitude, longitude })

                    //window.location.href = "/viewmarker.php?id=" + detail.folderId;
                });

                scene.appendChild(model);
            })(detail);
        });
        $(".gift-loader").hide();

       

        // set this code whenever you have already rendered your gps-entities (location based)
        const entities = [...document.querySelectorAll('a-entity[gps-entity-place]')];
        if (!entities || !entities.length) {
            console.error('No entities here!');
            return;
        }

        MarkerAR.config.nearestEntity = entities[0];

        // here we add a listener for each entity, to track the nearest entity to the user, eache gps update
        entities.forEach((entity) => {
            entity.addEventListener('gps-entity-place-update-positon', () => {
                const distance = parseFloat(entity.getAttribute('distance'));

                if (distance < MarkerAR.config.minorDistance) {
                    MarkerAR.config.minorDistance = distance;
                    MarkerAR.config.nearestEntity = entity
                }
            });
        });

        // at any point, we have "nearestEntity" that represents the nearest entity to the user
        // you can attach your click event to any button/element you want, I find that (I guess)
        // what you want is to register a click listener to the label with class "menu-open-button"

       
    },

    /**
     * Drop a marker at the user's current location.
     * 
     * @returns {void}
     */
    dropMarker: (detail) => {
        return new Promise(async (resolve, reject) => {

            if (detail.Longitude == 0 && detail.Latitude == 0 && detail.GiftShareType != "QR") {
                var cords = await GeoLocationService.GetLocation()
                // Compute 10 ft in front of the user.
                const heading = document.querySelector("[gps-camera]").components["gps-camera"].heading;
                if (heading != null) {
                    const coords2 = MarkerAR.calculateOffsetCoordinates({ latitude: cords.latitude, longitude: cords.longitude }, heading, 1.0);
                    cords.latitude = coords2.latitude;
                    cords.longitude = coords2.longitude;
                }

                detail.Longitude = cords.longitude;
                detail.Latitude = cords.latitude;
            }

            let addFolderResponse = await HttpService.Post("addfolderbyUser", detail);
            data = { latitude: detail.Latitude, longitude: detail.Longitude, detail: { folderId: addFolderResponse.UserFolderId, folderIcon: detail.FolderIcon } };
            if (detail.GiftShareType != "QR")
                await MarkerAR.postData(MarkerAR.config.placeMarkerUrl, { markers: [data] });

            resolve({ success: true, userFolderInfo: addFolderResponse })
        });

    },
    dropMarkers: (detail) => {
        return new Promise(async (resolve, reject) => {
            let addFolderResponse = await HttpService.Post("addfolderbyUserBulk", detail);
            // data = { latitude: cords.latitude, longitude: cords.longitude, detail: { folderId: addFolderResponse.UserFolderId } };
            await MarkerAR.postData(MarkerAR.config.placeMarkerUrl, { markers: addFolderResponse });
            resolve({ success: true, userFolderInfo: addFolderResponse });
        });

    },

    /**
     * Compute the distance on the earth's surface, in meters, between two coordinates.
     * 
     * @param {{ latitude: number, longitude: number }} start The coordinates of the first point.
     * @param {{ latitude: number, longitude: number }} end The coordinates of the second point.
     */
    sphericalCosinus: (start, end) => {
        const deltaLongitude = MarkerAR.toRadians(end.longitude - start.longitude),
            lat1radians = MarkerAR.toRadians(start.latitude),
            lat2radians = MarkerAR.toRadians(end.latitude);

        return MarkerAR.config.EarthRadiusMeters * Math.acos(Math.sin(lat1radians) * Math.sin(lat2radians) +
            Math.cos(lat1radians) * Math.cos(lat2radians) * Math.cos(deltaLongitude));
    },
    /**
     * Calculate a new point from a start, bearing, and offset.
     * 
     * @param {{ latitude: number, longitude: number }} start The point to start at.
     * @param {number} bearing The direction to calculate the new point, in degrees.
     * @param {number} distance The distance, in meters, to calculate the offset.
     * @returns {{ latitude: number, longitude: number }} The newly calculated point.
     */
    calculateOffsetCoordinates: (
        { latitude, longitude },
        bearing,
        distance) => {
        const delta = distance / MarkerAR.config.EarthRadiusMeters;
        const theta = MarkerAR.toRadians(bearing);
        const phi1 = MarkerAR.toRadians(latitude);
        const lambda1 = MarkerAR.toRadians(longitude);

        const phi2 = Math.asin(Math.sin(phi1) * Math.cos(delta) + Math.cos(phi1) * Math.sin(delta) * Math.cos(theta));
        const lambda2 = lambda1 + Math.atan2(Math.sin(theta) * Math.sin(delta) * Math.cos(phi1), Math.cos(delta) - Math.sin(phi1) * Math.sin(phi2));

        // Normalize to -180 deg .. 180 deg
        const lambda2norm = (lambda2 + 3 * Math.PI) % (2 * Math.PI) - Math.PI;

        return {
            latitude: MarkerAR.toDegrees(phi2),
            longitude: MarkerAR.toDegrees(lambda2norm)
        };
    },

    /**
     * Convert an angle measured in degrees to radians.
     * 
     * @param {number} degrees The value to convert.
     * @returns {number} The value in radians.
     */
    toRadians: (degrees) => {
        return degrees * Math.PI / 180.0;
    },

    /**
     * Convert an angle measured in degrees to radians.
     * 
     * @param {number} radians The value to convert.
     * @returns {number} The value in degrees.
     */
    toDegrees: (radians) => {
        return radians * 180.0 / Math.PI;
    },

    postData: async (url = '', data = {}) => {
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        });
        return response.json();
    }
}


class GeoLocationService {
    static GetLocation() {
        var locOpt = {
            enableHighAccuracy: false,
            timeout: 5000,
            maximumAge: Infinity
        };

        return new Promise((resolve, reject) => {
            navigator.geolocation.getCurrentPosition((loc) => {
                resolve({ accuracy: loc.coords.accuracy, latitude: loc.coords.latitude, longitude: loc.coords.longitude })
            }, (ar) => {
                console.log(ar);
                reject(null)
            }, locOpt);
        });
    }

    WatchPosition(sCb, eCb) {
        navigator.geolocation.watchPosition(sCb || (() => { }), eCb || (() => { }))
    }

    async getMapMarkerIcons() {
        // return new Promise(async (resolve, reject) => {
        //     var folderIconList = await this.httpService.Get<any>("getmapmarkericons");
        //     var iconFoldernames = Object.keys(folderIconList);
        //     resolve({ FolderNames: iconFoldernames, Icons: folderIconList })
        // });

    }
}
class LoaderService {

    static ShowLoader(percent) {
        // var circle = document.querySelector('circle');
        // var radius = circle.r.baseVal.value;
        // var circumference = radius * 2 * Math.PI;

        // circle.style.strokeDasharray = `${circumference} ${circumference}`;
        // circle.style.strokeDashoffset = `${circumference}`;
        // const offset = circumference - percent / 100 * circumference;
        // circle.style.strokeDashoffset = offset;

        $("#g-loader").show();
        $("#g-loader span").text(percent + "%");
    }
    static HideLoader() {
        $("#g-loader").hide();
    }
}

class ShareService {
    /**
    * Share
    */
    static async QRShare(code) {

        if ($("#qrmodal").length == 0) {
            $(document.body).append(`<div id="qrmodal" class="modal" style="width:95%">
    <div class="modal-content" style="padding:5px">
     <img src="" id="qrscanImage" />
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
  </div>`);
            $('#qrmodal').modal();
        }

        var imgsrc = await HttpService.Post("getqrimage", { UniqueSharedId: code })
        $("#qrscanImage").attr("src", imgsrc);
        M.Modal.getInstance($('#qrmodal')).open();;


    }
    /**
     * Share
     */
    static async Share(code) {
        try {
            await navigator.share({
                title: "Share Code with friend ",
                text: "Nearby Shareable code: " + code
            })

        } catch (err) {
            alert('use mobile to use this feature.')
            console.log(`Couldn't share ${err}`);
        }
    }
}



class MapService {



    //Generate a number of mappoints
    static GenerateMapPoints(centerpoint, distance, amount) {
        var mappoints = [];
        for (var i = 1; i < amount; i++) {
            mappoints.push(MapService.RandomGeo(centerpoint, distance));
        }
        return mappoints;
    }


    static RandomGeo(center, radius) {
        var y0 = center.latitude;
        var x0 = center.longitude;
        var rd = radius / 111300; //about 111300 meters in one degree

        var u = Math.random();
        var v = Math.random();

        var w = rd * Math.sqrt(u);
        var t = 2 * Math.PI * v;
        var x = w * Math.cos(t);
        var y = w * Math.sin(t);

        //Adjust the x-coordinate for the shrinking of the east-west distances
        var xp = x / Math.cos(y0);

        var newlat = y + y0;
        var newlon = x + x0;
        var newlon2 = xp + x0;

        return {
            'latitude': newlat.toFixed(5),
            'longitude': newlon.toFixed(5),
            'longitude2': newlon2.toFixed(5),
            'distance': MapService.Distance(center.latitude, center.longitude, newlat, newlon).toFixed(2),
            'distance2': MapService.Distance(center.latitude, center.longitude, newlat, newlon2).toFixed(2),
        };
    }


    //Calc the distance between 2 coordinates as the crow flies
    static Distance(lat1, lon1, lat2, lon2) {
        var R = 6371000;
        var a = 0.5 - Math.cos((lat2 - lat1) * Math.PI / 180) / 2 + Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) * (1 - Math.cos((lon2 - lon1) * Math.PI / 180)) / 2;
        return R * 2 * Math.asin(Math.sqrt(a));
    }
}
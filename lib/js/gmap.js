class GoogleMapComponent {
    apiKey = "AIzaSyDT01oe_DTxTZ5Ts8ALfkeTCAYuz-S9lJ8";
    mapCircle;
    map;
    infowindow;
    markers = {};
    mapsLoaded = false;
    currentLocation = null;
    networkHandler = null;
    maphtmlElement;
    init(ele) {
        var ref = this;
        this.maphtmlElement = ele;
        return new Promise((resolve, reject) => {

            this.loadSDK().then((res) => {

                this.initMap().then((res) => {
                    resolve({ isloaded: true, mapRef: ref.map});
                }, (err) => {
                    reject(err);
                });

            }, (err) => {

                reject(err);

            });

        });
    }
    loadSDK() {

        console.log("Loading Google Maps SDK");

        return new Promise((resolve, reject) => {

            if (!GoogleMapComponent.mapsLoaded) {

                this.injectSDK().then((res) => {
                    resolve(true);
                }, (err) => {
                    reject(err);
                });

            } else {

                resolve(true);
            }

        });


    }
    injectSDK() {

        return new Promise((resolve, reject) => {

            window['mapInit'] = () => {
                GoogleMapComponent.mapsLoaded = true;
                resolve(true);
            }

            let script = document.createElement('script');
            script.id = 'googleMaps';
            script.src = 'https://maps.googleapis.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit&libraries=places';
            document.body.appendChild(script);

        });

    }

    initMap() {

        let ref = this;
        /**
         * @constructor
         */
        var ClickEventHandler = function (map, origin) {
            this.origin = origin;
            this.map = map;
            this.placesService = new google.maps.places.PlacesService(map);
            this.infowindow = new google.maps.InfoWindow();
            this.infowindowContent = document.getElementById("infowindow-content");
            this.infowindow.setContent(this.infowindowContent);

            // Listen for clicks on the map.
            this.map.addListener("click", this.handleClick.bind(this));
        };

        ClickEventHandler.prototype.handleClick = async function (event) {
            if (event.placeId) {
                event.stop();
                this.getPlaceInformation(event.placeId);
            }
        };



        ClickEventHandler.prototype.getPlaceInformation = function (placeId) {
            var me = this;
            this.placesService.getDetails({ placeId: placeId }, function (place, status) {
                if (status === "OK") {
                    me.infowindow.close();
                    me.infowindow.addListener('domready', function () {
                        document.getElementById("createNearby").addEventListener("click", function (e) {
                            console.log(place.geometry.location);
                            ref.onClickCreateGioGift.emit(place)
                            e.stopPropagation();
                        })
                    });


                    me.infowindow.setPosition(place.geometry.location);
                    me.infowindowContent.children["place-icon"].src = place.icon;
                    me.infowindowContent.children["place-name"].textContent = place.name;
                    //me.infowindowContent.children["place-id"].textContent = place.place_id;
                    me.infowindowContent.children["place-address"].textContent =
                        place.formatted_address;

                    me.infowindow.open(me.map);
                }
            });
        };


        return new Promise((resolve, reject) => {

            if (GoogleMapComponent.currentLocation == null) {
                this.GetLocation().then((position) => {
                    console.log(position);
                    GoogleMapComponent.currentLocation = position

                    let mapOptions = {
                        center: new google.maps.LatLng(GoogleMapComponent.currentLocation.Latitude, GoogleMapComponent.currentLocation.Longitude),
                        zoom: 13,
                        disableDefaultUI: true
                    };
                    this.map = new google.maps.Map(this.maphtmlElement, mapOptions);
                    var clickHandler = new ClickEventHandler(this.map, new google.maps.LatLng(GoogleMapComponent.currentLocation.Latitude, GoogleMapComponent.currentLocation.Longitude));
                    resolve(true);

                }, (err) => {

                    reject('Could not initialise map');

                });
            } else {

                let mapOptions = {
                    center: new google.maps.LatLng(GoogleMapComponent.currentLocation.Latitude, GoogleMapComponent.currentLocation.Longitude),
                    zoom: 13,
                    disableDefaultUI: true
                };
                this.map = new google.maps.Map(this.maphtmlElement, mapOptions);
                var clickHandler = new ClickEventHandler(this.map, new google.maps.LatLng(GoogleMapComponent.currentLocation.Latitude, GoogleMapComponent.currentLocation.Longitude));
                resolve(true);

            }

        });

    }
    GetLocation() {
        return new Promise((resolve, reject) => {
            navigator.geolocation.getCurrentPosition((loc) => {
                resolve({ accuracy: loc.coords.accuracy, Latitude: loc.coords.latitude, Longitude: loc.coords.longitude })
            }, (ar) => {
                console.log(ar);
                reject(null)
            });
        });
    }

    AddMovableSimpleMarker(lat, lng, opts) {
        let latLng = new google.maps.LatLng(lat, lng);
        let marker = new google.maps.Marker({
            map: this.map,
            draggable: opts.draggable || false,
            animation: google.maps.Animation.DROP,
            position: latLng
        });
        return marker;
    }
    addMarker(markerInfo) {
        let ref = this;
        let latLng = new google.maps.LatLng(markerInfo.Latitude, markerInfo.Longitude);

        var v = "Latitude_" + markerInfo.Latitude + "_Longitude_" + markerInfo.Longitude;
        console.log(v)

        if (Object.keys(ref.markers).indexOf(v) == -1) {
            let marker = new google.maps.Marker({
                map: this.map,
                animation: google.maps.Animation.DROP,
                position: latLng,
                icon: {
                    url: "/images/google-map-animation.gif?v=3", // url
                    scaledSize: new google.maps.Size(20, 20), // scaled size
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(0, 0) // anchor
                },// + markerInfo.Icon,
                title: markerInfo.Title,
                label: markerInfo.Label
            });
            ref.markers[v] = {};
            ref.markers[v].marker = marker;
            ref.markers[v].markerInfo = [markerInfo];
            marker.addListener('click', function () {

                var contentString = `<div id="info-content" class="info-content">`;

                ref.markers["Latitude_" + marker.position.lat() + "_Longitude_" + marker.position.lng()].markerInfo.forEach(element => {
                    contentString += ` <h2 id="firstHeading" class="firstHeading" style="color:black">${element.Title}</h2><a class='viewLink' data-id="${element.AdditionalInfo.Id}">View Nearby</a>`
                    console.log(element);
                });

                contentString += `</div>`;

                if (ref.infowindow) {
                    ref.infowindow.close();
                }
                ref.infowindow = new google.maps.InfoWindow({
                    content: contentString
                });

                ref.infowindow.addListener('domready', function () {
                    document.querySelectorAll(".viewLink").forEach(viewLink => {
                        viewLink.addEventListener("click", function (e) {
                            console.log(markerInfo);
                            ref.markers["Latitude_" + marker.position.lat() + "_Longitude_"
                                + marker.position.lng()].markerInfo.forEach(clickedMarkerInfo => {
                                    if (clickedMarkerInfo.AdditionalInfo.Id == this.dataset.id)
                                        ref.onClickViewFolder.emit(clickedMarkerInfo)
                                });
                            e.stopPropagation();
                        });
                    });
                });

                ref.infowindow.open(ref.map, marker);
            });

        } else {
            ref.markers[v].markerInfo.push(markerInfo)
        }

    }

    async drawCircle(center, distanceInMeters) {
        await this.clearCircle()
        this.mapCircle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.2,
            map: this.map,
            center: { lat: center.Latitude, lng: center.Longitude },
            radius: distanceInMeters
        });
    }

    async clearCircle() {
        if (this.mapCircle) {
            this.mapCircle.setMap(null);
        }
    }
    async clearMarker() {

        Object.keys(this.markers).forEach((markerKey) => {
            this.markers[markerKey].marker.setMap(null);
        });
        this.markers = {};
    }
}
<?php include("shared/authorize.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <?php include("shared/termSection.php.php"); ?>
    <?php include("shared/commonCSS.php"); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body style="margin: 0;" onload="UI.LoadFolderImages()">

    <div class="navbar-fixed">
        <nav class="nav-extended">
            <div class="nav-wrapper">
                <a class="back-arrow t-color " href="/index.php"><i class="material-icons">arrow_back</i></a>
                <label class="nav-lab" id="nearbyName"></label>
            </div>
        </nav>
    </div>


    <section class="gallery-section">
        <div class="row" id="folderImages">

        </div>
    </section>


    <?php include("shared/footer.php"); ?>
</body>

</html>
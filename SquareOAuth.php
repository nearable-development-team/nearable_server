<?php

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/Configuration.php';

use Services\CurlSqureService;
use Services\MerchantService;

$merchantId = $_GET['merchantid'];

if ($merchantId != "") {
    global $callback_uri;
    $callback_uri = $callback_uri . "?merchantid=" . $merchantId;
} else {
    echo "Invalid Url";
    exit();
}
if (isset($_GET["code"])) {

    $curlSqureService = new CurlSqureService();
    $tokenDetail = $curlSqureService->GetAccessToken($_GET["code"]);
    echo print_r($tokenDetail);
    $merchantService = new MerchantService();
    $merchantTokenDetail;
    $merchantTokenDetail->MerchantId = $merchantId;
    $merchantTokenDetail->AccessToken = $tokenDetail->access_token;
    $merchantTokenDetail->RefreshToken = $tokenDetail->refresh_token;
    $merchantTokenDetail->SquareMerchantId = $tokenDetail->merchant_id;
    $merchantService->SaveMerchantToken($merchantTokenDetail);

?>
    <script type='text/javascript'>
        window.close();
    </script>
<?php
} else {
    getAuthorizationCode();
}

function getAuthorizationCode()
{
    global $authorize_url, $client_id, $callback_uri;

    $authorization_redirect_url = $authorize_url . "?response_type=code&client_id=" . $client_id . "&redirect_uri=" . $callback_uri . "&scope=MERCHANT_PROFILE_READ+MERCHANT_PROFILE_WRITE";

    header("Location: " . $authorization_redirect_url);
    exit();
}

<?php
require_once(__DIR__ . '/../Configuration.php');
require_once(__DIR__ . '/../vendor/autoload.php');

use Common\Helper;
use Services\SessionService;

$sessionService = new SessionService();
if ($sessionService->GetUser() == null) {
    if (!Helper::EndsWith($_SERVER['REQUEST_URI'], "login.php")) {
        header("Location: login.php");
        exit();
    }
}else{
    if ($user->UserRole == "admin") {
        header("Location: mapsettings.php");
        exit();
    }
}

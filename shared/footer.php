 <?php


    require_once(__DIR__ . '/../Configuration.php');
    require_once(__DIR__ . '/../vendor/autoload.php');

    use Services\SessionService;
    use Services\UserService;

    $curPageName = substr($_SERVER["SCRIPT_NAME"], strrpos($_SERVER["SCRIPT_NAME"], "/") + 1);
    $userService = new UserService();
    $sessionService = new SessionService();

    $sessionUser = $sessionService->GetUser();
    ?>
 <style>
     #slider {
         position: relative;
         overflow: hidden;
         margin: 20px auto 0 auto;
         border-radius: 4px;
     }

     #slider ul {
         position: relative;
         margin: 0;
         padding: 0;
         height: 200px;
         list-style: none;
     }

     #slider ul li {
         position: relative;
         display: block;
         float: left;
         margin: 0;
         padding: 0;
         width: 200px;
         height: 200px;
         text-align: center;
         line-height: 300px;
     }

     a.control_prev,
     a.control_next {
         position: absolute;
         top: 40%;
         z-index: 999;
         display: block;
         padding: 4% 3%;
         width: auto;
         height: auto;
         background: #2a2a2a;
         color: #fff;
         text-decoration: none;
         font-weight: 600;
         font-size: 18px;
         opacity: 0.8;
         cursor: pointer;
     }

     a.control_prev:hover,
     a.control_next:hover {
         opacity: 1;
         -webkit-transition: all 0.2s ease;
     }

     a.control_prev {
         border-radius: 0 2px 2px 0;
     }

     a.control_next {
         right: 0;
         border-radius: 2px 0 0 2px;
     }

     .slider_option {
         position: relative;
         margin: 10px auto;
         width: 160px;
         font-size: 18px;
     }

     input#pac-input {
         background-color: white;
         margin: 10px 0px 0px 5px;
         border-radius: 10px;
         padding: 0 12px 0px 16px;
         width: 88%;
     }

     .pac-card {
         margin: 10px 10px 0 0;
         border-radius: 2px 0 0 2px;
         box-sizing: border-box;
         -moz-box-sizing: border-box;
         outline: none;
         box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
         background-color: #fff;
         font-family: Roboto;
     }

     #pac-container {
         padding-bottom: 12px;
         margin-right: 12px;
     }

     .pac-controls {
         display: inline-block;
         padding: 5px 11px;
     }

     .pac-controls label {
         font-family: Roboto;
         font-size: 13px;
         font-weight: 300;
     }

     #pac-input {
         background-color: #fff;
         font-family: Roboto;
         font-size: 15px;
         font-weight: 300;
         margin-left: 12px;
         padding: 0 11px 0 13px;
         text-overflow: ellipsis;
         width: 400px;
         border-radius: 10px;
         margin-top: 10px;
     }

     #pac-input:focus {
         border-color: #4d90fe;
     }

     #title {
         color: #fff;
         background-color: #4d90fe;
         font-size: 25px;
         font-weight: 500;
         padding: 6px 12px;
     }

     #target {
         width: 345px;
     }

     .pac-container.pac-logo {
         z-index: 9999;
     }

     .menu-item,
     .menu-open-button {
         background: transparent;
         border-radius: 100%;
         width: 100px;
         height: 100px;
         margin-left: -40px;
         position: absolute;
         color: #FFFFFF;
         text-align: center;
         line-height: 80px;
         -webkit-transform: translate3d(0, 0, 0);
         transform: translate3d(0, 0, 0);
         -webkit-transition: -webkit-transform ease-out 200ms;
         transition: -webkit-transform ease-out 200ms;
         transition: transform ease-out 200ms;
         transition: transform ease-out 200ms, -webkit-transform ease-out 200ms;

     }

     .menu-item {
         display: none;
     }

     .menu-item img {
         height: 100px;
         width: 100px;
     }

     .menu-open {
         display: none;
     }

     .lines {
         width: 25px;
         height: 3px;
         background: #596778;
         display: block;
         position: absolute;
         top: 50%;
         left: 50%;
         margin-left: -12.5px;
         margin-top: -1.5px;
         -webkit-transition: -webkit-transform 200ms;
         transition: -webkit-transform 200ms;
         transition: transform 200ms;
         transition: transform 200ms, -webkit-transform 200ms;
     }

     .line-1 {
         -webkit-transform: translate3d(0, -8px, 0);
         transform: translate3d(0, -8px, 0);
     }

     .line-2 {
         -webkit-transform: translate3d(0, 0, 0);
         transform: translate3d(0, 0, 0);
     }

     .line-3 {
         -webkit-transform: translate3d(0, 8px, 0);
         transform: translate3d(0, 8px, 0);
     }

     .menu-open:checked+.menu-open-button .line-1 {
         -webkit-transform: translate3d(0, 0, 0) rotate(45deg);
         transform: translate3d(0, 0, 0) rotate(45deg);
     }

     .menu-open:checked+.menu-open-button .line-2 {
         -webkit-transform: translate3d(0, 0, 0) scale(0.1, 1);
         transform: translate3d(0, 0, 0) scale(0.1, 1);
     }

     .menu-open:checked+.menu-open-button .line-3 {
         -webkit-transform: translate3d(0, 0, 0) rotate(-45deg);
         transform: translate3d(0, 0, 0) rotate(-45deg);
     }

     .menu {
         position: absolute;
         bottom: 55%;
         left: 48%;
         box-sizing: border-box;
         font-size: 25px;
         background-color: transparent;
     }

     .menu-item:nth-child(3) {
         -webkit-transition-duration: 180ms;
         transition-duration: 180ms;
     }

     .menu-item:nth-child(4) {
         -webkit-transition-duration: 180ms;
         transition-duration: 180ms;
     }

     .menu-item:nth-child(5) {
         -webkit-transition-duration: 180ms;
         transition-duration: 180ms;
     }

     .menu-item:nth-child(6) {
         -webkit-transition-duration: 180ms;
         transition-duration: 180ms;
     }

     .menu-item:nth-child(7) {
         -webkit-transition-duration: 180ms;
         transition-duration: 180ms;
     }

     .menu-item:nth-child(8) {
         -webkit-transition-duration: 180ms;
         transition-duration: 180ms;
     }

     .menu-item:nth-child(9) {
         -webkit-transition-duration: 180ms;
         transition-duration: 180ms;
     }

     .menu-open-button {
         z-index: 2;
         -webkit-transition-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1.275);
         transition-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1.275);
         -webkit-transition-duration: 400ms;
         transition-duration: 400ms;
         -webkit-transform: scale(1.1, 1.1) translate3d(0, 0, 0);
         transform: scale(1.1, 1.1) translate3d(0, 0, 0);
         cursor: pointer;
         box-shadow: 3px 3px 0 0 rgba(0, 0, 0, 0.14);
     }

     .menu-open-button:hover {
         -webkit-transform: scale(1.2, 1.2) translate3d(0, 0, 0);
         transform: scale(1.2, 1.2) translate3d(0, 0, 0);
     }

     .menu-open:checked+.menu-open-button {
         -webkit-transition-timing-function: linear;
         transition-timing-function: linear;
         -webkit-transition-duration: 200ms;
         transition-duration: 200ms;
         -webkit-transform: scale(0.8, 0.8) translate3d(0, 0, 0);
         transform: scale(0.8, 0.8) translate3d(0, 0, 0);
     }

     .menu-open:checked~.menu-item {
         -webkit-transition-timing-function: cubic-bezier(0.935, 0, 0.34, 1.33);
         transition-timing-function: cubic-bezier(0.935, 0, 0.34, 1.33);
     }

     .menu-open:checked~.menu-item:nth-child(3) {
         transition-duration: 180ms;
         -webkit-transition-duration: 180ms;
         -webkit-transform: translate3d(0.08361px, -104.99997px, 0);
         transform: translate3d(0.08361px, -104.99997px, 0);
     }

     .menu-open:checked~.menu-item:nth-child(4) {
         transition-duration: 280ms;
         -webkit-transition-duration: 280ms;
         -webkit-transform: translate3d(90.9466px, -52.47586px, 0);
         transform: translate3d(90.9466px, -52.47586px, 0);
     }

     .menu-open:checked~.menu-item:nth-child(5) {
         transition-duration: 380ms;
         -webkit-transition-duration: 380ms;
         -webkit-transform: translate3d(90.9466px, 52.47586px, 0);
         transform: translate3d(90.9466px, 52.47586px, 0);
     }

     .menu-open:checked~.menu-item:nth-child(6) {
         transition-duration: 480ms;
         -webkit-transition-duration: 480ms;
         -webkit-transform: translate3d(0.08361px, 104.99997px, 0);
         transform: translate3d(0.08361px, 104.99997px, 0);
     }

     .menu-open:checked~.menu-item:nth-child(7) {
         transition-duration: 580ms;
         -webkit-transition-duration: 580ms;
         -webkit-transform: translate3d(-90.86291px, 52.62064px, 0);
         transform: translate3d(-90.86291px, 52.62064px, 0);
     }

     .menu-open:checked~.menu-item:nth-child(8) {
         transition-duration: 680ms;
         -webkit-transition-duration: 680ms;
         -webkit-transform: translate3d(-91.03006px, -52.33095px, 0);
         transform: translate3d(-91.03006px, -52.33095px, 0);
     }

     .menu-open:checked~.menu-item:nth-child(9) {
         transition-duration: 780ms;
         -webkit-transition-duration: 780ms;
         -webkit-transform: translate3d(-0.25084px, -104.9997px, 0);
         transform: translate3d(-0.25084px, -104.9997px, 0);
     }

     .blue {
         background-color: #669AE1;
         box-shadow: 3px 3px 0 0 rgba(0, 0, 0, 0.14);
         text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.12);
     }

     .blue:hover {
         color: #669AE1;
         text-shadow: none;
     }

     .green {
         background-color: #70CC72;
         box-shadow: 3px 3px 0 0 rgba(0, 0, 0, 0.14);
         text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.12);
     }

     .green:hover {
         color: #70CC72;
         text-shadow: none;
     }

     .red {
         background-color: #FE4365;
         box-shadow: 3px 3px 0 0 rgba(0, 0, 0, 0.14);
         text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.12);
     }

     .red:hover {
         color: #FE4365;
         text-shadow: none;
     }

     .purple {
         background-color: #C49CDE;
         box-shadow: 3px 3px 0 0 rgba(0, 0, 0, 0.14);
         text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.12);
     }

     .purple:hover {
         color: #C49CDE;
         text-shadow: none;
     }

     .orange {
         background-color: #FC913A;
         box-shadow: 3px 3px 0 0 rgba(0, 0, 0, 0.14);
         text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.12);
     }

     .orange:hover {
         color: #FC913A;
         text-shadow: none;
     }

     .lightblue {
         background-color: #62C2E4;
         box-shadow: 3px 3px 0 0 rgba(0, 0, 0, 0.14);
         text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.12);
     }

     .lightblue:hover {
         color: #62C2E4;
         text-shadow: none;
     }

     .credit {
         margin: 24px 20px 120px 0;
         text-align: right;
         color: #EEEEEE;
     }

     .credit a {
         padding: 8px 0;
         color: #C49CDE;
         text-decoration: none;
         transition: all 0.3s ease 0s;
     }

     .credit a:hover {
         text-decoration: underline;
     }

     #description {
         font-family: Roboto;
         font-size: 15px;
         font-weight: 300;
     }

     #infowindow-content .title {
         font-weight: bold;
     }

     #infowindow-content {
         display: none;
     }

     #map #infowindow-content {
         display: inline;
     }


     #title {
         color: #fff;
         background-color: #4d90fe;
         font-size: 25px;
         font-weight: 500;
         padding: 6px 12px;
     }

     #target {
         width: 345px;
     }

     .GEO_GiftOptions {
         display: none;
     }
 </style>
 <div class="home-footer" style="display: none;">
     <ul>
         <li class="waves-effect " onclick="UI.SaveImageClick()"><a href="#"> <i style="color: #702F8A;" class="fas fa-camera"></i>Camera</a></li>
         <li class="waves-effect "><a href="#"> <i class="fas fa-image" style="color: #009999;"></i> Gallery</a></li>
         <li class="waves-effect " onclick="UI.CreateNearByClick()">
             <a href="#"> <i style="color: #3399CC;    position: relative;" class="fas fa-map-marker-alt"><i id="plus-id" class="fas fa-plus"></i></i> Marker</a>
         </li>
         <li class="waves-effect "><a href="#"> <i style="color: #993399;" class="fas fa-sign-out-alt"></i>Logout</a></li>
     </ul>
 </div>
 <div class="home-footer" style="display: none;">
     <ul>

         <?php if ($curPageName == 'index.php') { ?>
             <li class="waves-effect "><a href="javascript:void(0)"> <img src="images/menu/home-active.svg"></a></li>
         <?php } else { ?>
             <li class="waves-effect "><a href="index.php"> <img src="images/menu/home.svg"></a></li>
         <?php } ?>

         <?php if ($curPageName == 'mapview.php') { ?>
             <li class="waves-effect "><a href="javascript:void(0)"> <img src="images/menu/map-active.svg"></a></li>
         <?php } else { ?>
             <li class="waves-effect "><a href="mapview.php"> <img src="images/menu/map.svg"></a></li>
         <?php } ?>
         <li class="waves-effect">
             <a class="btn-floating  waves-effect waves-light blue ad-action"><i class="material-icons">add</i></a>
         </li>
         <?php if ($curPageName == 'mynearby.php') { ?>
             <li class="waves-effect"><a href="javascript:void(0)"><img src="images/menu/save-active.svg"></a></li>
         <?php } else { ?>
             <li class="waves-effect"><a href="mynearby.php"><img src="images/menu/save.svg"></a></li>
         <?php } ?>
         <li class="waves-effect"><a href="logout.php"><img src="images/menu/logout.svg"></a></li>
     </ul>

     <div class="ot-action" style="display: none;">

         <a href="#"><img src="images/menu/gift.png?v=1" onclick="UI.CreateNearByClick()"></a>
         <a href="#" class="camera-action">
             <!-- <input class="file-skin size-60" id="camera-file-input-1" type="file" onchange="UI.SaveImageClick(event)" accept="image/*;capture=camera"> -->
             <img src="images/menu/camera.png?v=1" onclick="UI.SaveImageClick($event)">
         </a>
     </div>
 </div>


 <div id="nearbyPasswordVerifyModal" class="modal">
     <div class="modal-content">
         <a href="#!" class="modal-close custom-close"><i class="material-icons">close</i></a>
         <div class="modal-skin">
             <div class="icon-set">
                 <img style="width: 24vw;" src="images/Password.gif" alt="lock">
             </div>
             <div class="mod-pop">
                 <h3>Verify Identity</h3>
                 <div class="row">
                     <form class="col s12">
                         <div class=" custom-field col s12">
                             <input id="nearbyPasswordVerifyModalPassword" maxlength="12" type="text" placeholder="Enter Mobile Number">
                         </div>
                         <div class=" custom-field col s12">
                             <input id="nearbyPasswordVerifyModalToken" type="password" placeholder="Enter Token">
                         </div>
                         <div class="sm-space custom-btn col s12">
                             <a class="waves-effect waves-light btn-small" id="nearbyPasswordVerifyModalViewBtnClick">View Gift</a>
                         </div>
                     </form>
                 </div>

             </div>
         </div>
     </div>
 </div>

 <!-- Modal Structure -->
 <div id="nearbyGeoMarkerModal" class="modal">
     <div class="modal-content">
         <a href="#!" class="modal-close custom-close"><i class="material-icons">close</i></a>
         <div class="modal-skin">

             <div class="mod-pop">


                 <form class="col s12">

                     <div class=" custom-field col s12">
                         <label>
                             <input type="radio" name="GiftShareType" value="QR" class="filled-in" checked id="GiftShareType_QR">
                             <span>QR Gift</span>
                         </label>
                         <label>
                             <input type="radio" name="GiftShareType" value="GEO" class="filled-in" id="GiftShareType_Geo">
                             <span>Geo Gift</span>
                         </label>
                         <div class=" custom-field col s12 QR_GiftOptions">
                             Upload the image to create QR of your choice <label id="buttonUpload" for="qrImageUploader" class="waves-effect waves-light btn-small" type="button">
                                 <input type="file" id="qrImageUploader" style="display: none">
                                 Upload
                             </label>
                             <button class="waves-effect waves-light btn-small" id="qrDownloadFullImage" type="button">Download QR</button>

                             <input type="hidden" name="h_pattCode" id="h_pattCode">
                             <input type="hidden" name="h_qrBaseCode" id="h_qrBaseCode">
                         </div>
                     </div>
                     <div class=" custom-field col s12 GEO_GiftOptions">
                         <input id="nearbyPasswordModalMobile" type="email" minlength="5" placeholder="1xxxxxxxxxx">
                     </div>


                     <div class=" custom-field col s12" style="position: RELATIVE;">
                         <a href="#" class="control_next">></a>
                         <a href="#" class="control_prev">
                             < </a>
                                 <div id="slider">
                                     <ul>

                                         <?php $files = preg_grep('~\.(png)$~', scandir(__DIR__ . "/../images/ar-models"));

                                            foreach ($files as $file) { ?>
                                             <li data-value="<?php echo str_replace(".png", "", $file); ?>"><img style='height:200;width:200px' src="/images/ar-models/<?php echo $file; ?>"></li>


                                         <?php } ?>
                                     </ul>
                                 </div>
                     </div>

                     <div class="row">
                         <div class="sm-space custom-btn col s12 GEO_GiftOptions">
                             <label>
                                 <input type="checkbox" name="useCurrentLocation" class="filled-in" checked id="useCurrentLocation">
                                 <span>Use Current Location</span>
                             </label>

                         </div>

                         <div class="sm-space custom-btn col s12">
                             <a class="waves-effect waves-light btn-small" id="nearbyGeoMarkerModalSubmitBtn" onclick="UI.PlaceMarkerModalClick()">Plant</a>
                         </div>
                     </div>
                 </form>


             </div>
         </div>
     </div>
 </div>


 <!-- Modal Structure -->
 <div id="GeoMarkerLocationSelectionModal" class="modal" style="width: 95%;">
     <div class="modal-content-1">
         <div class="modal-skin">
             <div class="mod-pop">
                 <div class="row">
                     <form class="col s12">
                         <div class="row">
                             <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                             <div id="gmapmodal" style="height: 50vh;">
                                 Loading Map.....</div>
                             <blockquote style="margin: 10px 0;">
                                 *Enter Recipients Address
                             </blockquote>

                             <div class="custom-btn col s12">
                                 <a class="waves-effect waves-light btn-small" id="GeoMarkerLocationSelectionModalSubmitBtn" onclick="UI.SelectLocationModalClick()">Select</a>
                             </div>
                         </div>
                     </form>
                 </div>

             </div>
         </div>
     </div>
 </div>



 <!-- Modal Structure -->
 <div id="nearbyViewMarkerModal" class="modal">
     <div class="modal-content">
         <a href="#!" class="modal-close custom-close"><i class="material-icons">close</i></a>
         <div class="modal-skin">
             <div class="icon-set">
                 <img style="width: 15vw;" src="images/gold coin.png?v=1" alt="Location">
             </div>
             <div class="mod-pop">
                 <div class="row">
                     <div class="container">
                         <H4 id="giftFoundText">You found a Geo Credit!</H4>
                         <h1 id="giftFoundAmount" style="color: red;">$50</h1>
                         <p>Credits can be used towards Geo Gift purchases. Go Find more!</p>
                         <button class="waves-effect waves-light btn" id="nearbyViewMarkerModalRedeemBtn">REDEEM POINTS</button>
                     </div>
                 </div>

             </div>
         </div>
     </div>
 </div>

 <div id="nearbyfolderModal" class="modal modal-fixed-footer">
     <div class="modal-content">
         <h4>Select Nearby</h4>
         <div class="nearby" id="nearbyFolders">
         </div>
     </div>
     <div class="modal-footer">
         <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancel</a>
         <a href="#!" class="modal-close waves-effect waves-green btn-flat" id="btnNearbyfolderModalSave">Save</a>
     </div>
 </div>
 <div class="g-wrapper-loader" id="g-loader" style="display: none;">

     <div class="preloader-wrapper big active" style=" top: 37vh;left: 37%; ">
         <div class="spinner-layer spinner-blue">
             <div class="circle-clipper left">
                 <div class="circle"></div>
             </div>
             <div class="gap-patch">
                 <div class="circle"></div>
             </div>
             <div class="circle-clipper right">
                 <div class="circle"></div>
             </div>
         </div>

         <div class="spinner-layer spinner-red">
             <div class="circle-clipper left">
                 <div class="circle"></div>
             </div>
             <div class="gap-patch">
                 <div class="circle"></div>
             </div>
             <div class="circle-clipper right">
                 <div class="circle"></div>
             </div>
         </div>

         <div class="spinner-layer spinner-yellow">
             <div class="circle-clipper left">
                 <div class="circle"></div>
             </div>
             <div class="gap-patch">
                 <div class="circle"></div>
             </div>
             <div class="circle-clipper right">
                 <div class="circle"></div>
             </div>
         </div>

         <div class="spinner-layer spinner-green">
             <div class="circle-clipper left">
                 <div class="circle"></div>
             </div>
             <div class="gap-patch">
                 <div class="circle"></div>
             </div>
             <div class="circle-clipper right">
                 <div class="circle"></div>
             </div>
         </div>
     </div>
     <!-- <div class="g-loader"> <span> 50%</span>
         <svg class="progress-ring" width="90" height="90">

             <circle class="progress-ring__circle" stroke="purple" stroke-width="6" fill="transparent" r="40" cx="44" cy="45" />
         </svg>
     </div> -->
 </div>
 <?php if ($sessionUser != null && $sessionUser->IsEmailConfirmed == 0 && 1 == 0) { ?>
     <div style="padding: 10px;background-color: #ef7d1b;color: white;margin: 0px auto;position: fixed;width: 100%;top: 0px;left: 0px;font-weight: bold;" Id="EmailconfirmationPop">Make sure you have valid email register to claim public gift. Your current email Id: <span style="font-weight: bold;    text-transform: uppercase;"><?php echo $sessionUser->Email;  ?></span> <span style="text-decoration: underline;" onclick="UI.ConfirmUserEmailId()">Confirm</span></div>

 <?php } ?>

 <div class="menu" style="display: none;">
     <input type="checkbox" href="#" class="menu-open" name="menu-open" id="menu-open" />
     <label class="menu-open-button" for="menu-open">
         <span class="lines line-1"></span>
         <span class="lines line-2"></span>
         <span class="lines line-3"></span>
     </label>

     <a onclick="UI.CreateNearByClick()" class="menu-item">
         <img src="/images/nav/add gift.png?v=2">
     </a>
     <!-- <a href="leaderboard.php" class="menu-item "> <img src="/images/nav/Leader Board.png?v=2"> </a>
     <a href="#" class="menu-item">
         <input class="file-skin size-60" id="camera-file-input-1" type="file" onchange="UI.SaveImageClick(event)" accept="image/*;capture=camera">
         <img src="/images/nav/Camera.png?v=2">

     </a>
     <a href="mynearby.php" class="menu-item"> <img src="/images/nav/Box.png?v=2"> </a>
     <a href="logout.php" class="menu-item"> <img src="/images/nav/Sign Out.png?v=2"> </a>
     <a href="mapview.php" class="menu-item"> <img src="/images/nav/Map.png?v=2"></a> -->
 </div>
 <input type="hidden" id="h_userId" name="h_userId" value="<?php echo $sessionUser->Id; ?>">
 <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
 <script src="lib/js/custom-slider.js?v=1"></script>
 <script src="lib/js/gmap.js?v=4"></script>
 <script src="lib/js/camera-helper.js?v=121"></script>
 <script src="lib/js/threeXpatternFileMaker.js?v=1"></script>
 <!-- Compiled and minified JavaScript -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
 <script>
     $(document).ready(() => {
         $("#menu-open").change(() => {
             if ($("#menu-open").prop("checked"))
                 $(".menu-item").show()
             else
                 setTimeout(() => {
                     $(".menu-item").hide()
                 }, 180)
         })
         $("input[name='GiftShareType']").change(function() {
             if ($(this).val() == "QR") {
                 $(".QR_GiftOptions").show();
                 $(".GEO_GiftOptions").hide();
             } else if ($(this).val() == "GEO") {
                 $(".QR_GiftOptions").hide();
                 $(".GEO_GiftOptions").show();
             }
         });
         $("#useCurrentLocation").change(function() {
             if ($(this).is(":checked")) {
                 $("#nearbyGeoMarkerModalSubmitBtn")
                     .fadeOut(100, function() {
                         $(this).text('Plant').fadeIn(500);
                     });
             } else {
                 $("#nearbyGeoMarkerModalSubmitBtn")
                     .fadeOut(100, function() {
                         $(this).text('Select Location').fadeIn(500);
                     });
             }
         });
         var elems = document.querySelectorAll('.modal');
         var instances = M.Modal.init(elems, {
             dismissible: false
         });

         $('.tabs').tabs();

         $('.ad-action').click(function() {
             $('.ot-action').toggle();
             $(this).find('i').text($(this).find('i').text() == 'add' ? 'clear' : 'add');
         });

         $('#qrImageUploader').change(function() {
             var file = this.files[0];
             imageName = file.name
             // remove file extension
             imageName = imageName.substring(0, imageName.lastIndexOf('.')) || imageName

             // debugger

             var reader = new FileReader();
             reader.onload = function(event) {
                 innerImageURL = event.target.result
                 updateFullMarkerImage()
             };
             reader.readAsDataURL(file);
         });
         document.querySelector('#qrDownloadFullImage').addEventListener('click', function() {
             // debugger
             if (innerImageURL === null) {
                 alert('upload a file first')
                 return
             }

             // tech from https://stackoverflow.com/questions/3665115/create-a-file-in-memory-for-user-to-download-not-through-server
             var domElement = window.document.createElement('a');
             domElement.href = fullMarkerURL;
             domElement.download = "pattern-" + (imageName || 'marker') + '.png';
             document.body.appendChild(domElement)
             domElement.click();
             document.body.removeChild(domElement)
         })

     })

     function updateFullMarkerImage() {
         // get patternRatio
         var patternRatio = .80 //document.querySelector('#patternRatioSlider').value/ 100
         var imageSize = "512"
         var borderColor = "black"

         function hexaColor(color) {
             return /^#[0-9A-F]{6}$/i.test(color);
         };

         var s = new Option().style;
         s.color = borderColor;
         if (borderColor === '' || (s.color != borderColor && !hexaColor(borderColor))) {
             // if color not valid, use black
             borderColor = 'black';
         }

         THREEx.ArPatternFile.buildFullMarker(innerImageURL, patternRatio, imageSize, borderColor, function onComplete(markerUrl) {
             fullMarkerURL = markerUrl

             var fullMarkerImage = document.createElement('img')
             fullMarkerImage.src = fullMarkerURL
             $("#h_qrBaseCode").val(fullMarkerURL);
             //  // put fullMarkerImage into #imageContainer
             //  var container = document.querySelector('#qrImageContainer')
             //  while (container.firstChild) container.removeChild(container.firstChild);
             //  container.appendChild(fullMarkerImage)
         })
         THREEx.ArPatternFile.encodeImageURL(innerImageURL, function onComplete(patternFileString) {
             $("#h_pattCode").val(patternFileString);
         })
     }
 </script>
 <script src="https://browser.sentry-cdn.com/5.26.0/bundle.min.js" integrity="sha384-VGljl8BTZL6Py4DmlOaYmfkOwp8mD3PrmD2L+fN446PZpsrIHuDhX7mnV/L5KuNR" crossorigin="anonymous"></script>
 <script>
     Sentry.init({
         dsn: "https://4198f686a2ed439cb02eec97df7b6cc5@o217467.ingest.sentry.io/5467730'",
     });
 </script>
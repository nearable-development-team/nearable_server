<?php include("shared/authorize.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <?php include("shared/termSection.php.php"); ?>
    <?php include("shared/commonCSS.php"); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
    <style>
        .gift-loader {
            position: fixed;
            top: 0px;
            background-color: orange;
            padding: 2px 8px;
            left: 46vw;
            border-radius: 3px;
            border: 1px solid #99680f;
        }

        .modal {
            background-color: #ffffff;
        }

        .scan-btn-link {
            position: absolute;
            right: 2%;
            top: 2%;

        }

        div.menu {
            display: block !important;
        }
    </style>
</head>

<body style="margin: 0; overflow: hidden;">
    <div class="scan-btn-link"> <a href="/qr-code-scanner.php" class="btn-floating btn-large waves-effect waves-light" style="
    background-color: transparent;
">
            <i class="fas fa-qrcode"></i>
        </a></div>

    <div class="gift-loader"></div>
    <button class="gallery-btn hide  waves-effect waves-teal" onclick="UI.ViewGalleryClick()"><img src="images/gallery.svg"></button>

    <button class="distance-btn hide waves-effect waves-teal" onclick="UI.CreateNearByClick()"><img src="images/distance.svg"></button>

    <button class="d-camera-btn hide waves-effect waves-teal"><img src="images/device-camera.svg"></button>

    <button class="camera-btn hide waves-effect waves-teal" onclick="UI.SaveImageClick()"><i></i></button>

    <a-scene renderer="logarithmicDepthBuffer: true" vr-mode-ui="enabled: false" embedded arjs="sourceType: webcam; debugUIEnabled: false;">
        <a-assets>

            <?php $files = preg_grep('~\.(gif)$~', scandir(__DIR__ . "/images/ar-models"));

            foreach ($files as $file) { ?>
                <a-asset-item id="<?php echo strtolower(str_replace(" ", "_", str_replace(".gif", "", $file))); ?>" response-type="arraybuffer" src="images/ar-models/<?php echo str_replace(".gif", ".glb", $file); ?>"></a-asset-item>
            <?php } ?>
        </a-assets>

        <a-camera gps-camera rotation-reader>
            <a-entity cursor="fuse: true; maxDistance: 30; timeout: 500" position="0 0 -1" height="55px" width="55px" geometry="primitive: ring" material="color: white; shader: flat">
            </a-entity>
        </a-camera>
    </a-scene>

    <?php include("shared/footer.php"); ?>
    <script>
        MarkerAR.init();
    </script>
</body>

</html>
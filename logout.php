<?php
require __DIR__ . '/vendor/autoload.php';
require_once(__DIR__ . '/Configuration.php');

use Services\SessionService;
use Services\UserService;

$userService = new UserService();
$user = new stdClass();
$sessionService = new SessionService();

$user->Id = $sessionService->GetUser()->Id;
$user->Action = 'logout';
$userLoginDetail =    $userService->SaveLoginDetail($user);

$sessionService->UnSet();
session_destroy();
header("Location: login.php");

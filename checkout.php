<?php


use Stripe\Terminal\Location;

require __DIR__ . '/vendor/autoload.php';
require_once(__DIR__ . '/Configuration.php');
include("shared/header.php"); ?>

<!DOCTYPE html>
<html>

<head>
    <?php
    if (!isset($_GET["userfolderId"])) {
        header("Location: index.php");
        die();
    }
    include("shared/commonCSS.php"); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>

<body class="thanks-screen">


    <div class="thank-container">

        <div class="thank-info">
            <img class="thumb" src="images/online_payment.svg">

            <h2>Gift Amount</h2>
            <div class="row">
                <form class="col s12">
                    <div class="row">

                        <div class="input-field col s12">
                            <select name="amount" id="amount">
                                <option value="10.00">$10.00</option>
                                <option value="15.00">$15.00</option>
                                <option value="50.00">$50.00</option>
                                <option value="75.00">$75.00</option>
                                <option value="100.00">$100.00</option>
                                <option value="125.00">$125.00</option>
                                <option value="150.00">$150.00</option>
                            </select>
                            <label class="active" for="amount">Enter Amount</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <label id="errorMessage"></label>
                        </div>
                    </div>
                    <button class="waves-effect waves-light btn-small t-bg t-bg-action" type="button" id="checkout-button">Checkout</button>

                </form>
            </div>


        </div>


    </div>

    <div class="row" style="display: none;">
        <div class="col s12">
            <div class="row">
                <div class="input-field col s6">
                    <select name="amount" id="amount">
                        <option value="10.00">$10.00</option>
                        <option value="15.00">$15.00</option>
                        <option value="50.00">$50.00</option>
                        <option value="75.00">$75.00</option>
                        <option value="100.00">$100.00</option>
                        <option value="125.00">$125.00</option>
                        <option value="150.00">$150.00</option>
                    </select>
                    <label class="active" for="amount">Enter Amount</label>
                </div>
            </div>


            <div class="row">
                <div class="input-field col s6">
                    <button class="waves-effect waves-light btn-small" type="button" id="checkout-button">Checkout</button>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <label id="errorMessage"></label>
                </div>
            </div>
            </form>
        </div>
        <script src="https://js.stripe.com/v3/"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="lib/js/camera-helper.js?v=121"></script>
        <script type="text/javascript">
            // Create an instance of the Stripe object with your publishable API key
            var stripe = Stripe('<?php echo STRIPE_PUBLISHABLE_KEY; ?>');

            $("#checkout-button").click(async function() {

                HttpService.Post("createcheckoutsession", {
                        Amount: $("#amount").val(),
                        UserFolderId: "<?php echo $_GET["userfolderId"] ?>"
                    })
                    .then(function(rstl) {
                        stripe.redirectToCheckout({
                            sessionId: rstl.SessionId
                        })
                    }).catch(function(er) {
                        $("#errorMessage").text(er);
                    });
            })
        </script>
</body>

</html>
<?php
require __DIR__ . '/vendor/autoload.php';
require_once(__DIR__ . '/Configuration.php');

use Services\SessionService;
use Services\UserService;

$sessionService = new SessionService();

//redirect to login page if user not admin
if ($sessionService->GetUser() == null) {
    header("Location: login.php");
    exit();
}

$userService = new UserService();
$user = new stdClass();

$userPayments =    $userService->GetPaymentTransaction();

?>
<!DOCTYPE html>
<html>
<title>W3.CSS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="css/w3.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
	.page-content{
		margin-left:25%
	}
	 @media screen and (max-width: 600px){
		.page-content{
			margin-left:41%
		}	    
	}
</style>
<body>

<!-- Sidebar -->
<div class="w3-sidebar w3-light-grey w3-bar-block" style="width:25%">
  <h3 class="w3-bar-item"><i class="fa fa-bars"></i> Menu</h3>
  <a href="adminLoginLog.php" class="w3-bar-item w3-button w3-border-top"><i class="fa fa-history"></i> Login Log</a>
  <a href="adminUsers.php" class="w3-bar-item w3-button"><i class="fa fa-user"></i> Users</a>
  <a href="adminCreateUser.php" class="w3-bar-item w3-button"><i class="fa fa-user"></i>Create User</a>
  <a href="adminPaymentTransaction.php" class="w3-bar-item w3-button w3-black"><i class="fa fa-exchange"></i> Payment Transactions</a>
  <a href="logout.php" class="w3-bar-item w3-button"><i class="fa fa-sign-out"></i> Logout</a>
</div>

<!-- Page Content -->
<div class="page-content">

<div class="w3-container w3-white w3-border-bottom">
  <h1>Hi Admin</h1>
</div>

<!-- <img src="img_car.jpg" alt="Car" style="width:100%"> -->

<div class="w3-container">
	<h2>User's Payment</h2>	
	<table class="w3-table-all">
	    <tr>
	      <th>User Name</th>
	      <th>Order Id</th>
	      <th>Name Card</th>
	      <th>Amount</th>	      
	      <th>Action</th>
	    </tr>
		<?php if ( !empty($userPayments) ) { ?>	
			<?php while($row = $userPayments->fetch_assoc()) { 

				$dt = '<a href="invoice.php" class="w3-btn w3-round w3-purple" download>
  						Download Invoice
					</a>
					<a href="#" class="w3-btn w3-round w3-purple send-invoice">
  						Send Invoice
					</a>';
			    echo '<tr> <td>'.$row['user_name'].'</td> <td>'.$row['order_id'].'</td> <td>'.$row['name_card'].'</td> <td>'.$row['amount'].'</td> <td>'.$dt.'<td> </tr>';
			 } ?>
		<?php } ?>   
  </table>
</div>

</div>

<script src="lib/js/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.send-invoice').click(function(){
			$.ajax({
				url: "sendMailInvoice.php",
				data: "12",
				success: function(result){
					alert(result);		      		
		    	}
			});
		});
	});
</script>      
</body>
</html>

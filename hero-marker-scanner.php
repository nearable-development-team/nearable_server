<?php

use Services\FolderService;

require __DIR__ . '/vendor/autoload.php';
require_once(__DIR__ . '/Configuration.php');
$folderService = new FolderService()
?>

<!DOCTYPE html>
<html>
<script src="https://aframe.io/releases/0.9.2/aframe.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/donmccurdy/aframe-extras@v6.1.1/dist/aframe-extras.min.js"></script>
<script src="https://raw.githack.com/AR-js-org/AR.js/master/aframe/build/aframe-ar-nft.js"></script>
<script>
    // This script is responsible for handling the events triggered by the markers
    AFRAME.registerComponent('clickhandler', {
        init: function() {
            this.el.addEventListener('click', () => {

                alert("this istestig ")
            });
        }
    })
</script>

<body style="margin : 0px; overflow: hidden;">
    <a-scene cursor='rayOrigin: mouse; fuse: true; fuseTimeout: 0;' raycaster="objects: [clickhandler];" vr-mode-ui="enabled: false" renderer="logarithmicDepthBuffer: true" embedded arjs='sourceType: webcam;'>
        <?php foreach ($folderService->GetQRFolders() as $qrFolder) { ?>

            <a-marker preset="custom" type='pattern' url='/<?php echo $qrFolder->PattFileLocation; ?>'>
                <a-entity scale="2 2 2" clickhandler gltf-model="/images/ar-models/<?php echo  $qrFolder->FolderIcon . ".glb"; ?>" data-marker-info="{'patfile':'<?php echo $qrFolder->PattFileLocation; ?>'}"></a-entity>
            </a-marker>

        <?php }   ?>
        <a-entity camera>
            <!-- <a-cursor height="55px" width="55px" geometry="primitive: ring" color="transparent"></a-cursor> -->
        </a-entity>
    </a-scene>

</body>

</html>
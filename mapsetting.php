<?php
require_once(__DIR__ . '/Configuration.php');
require_once(__DIR__ . '/vendor/autoload.php');

use Common\Helper;
use Services\SessionService;

$sessionService = new SessionService();
$user = $sessionService->GetUser();
if ($user == null) {
    header("Location: adminlogin.php");
    exit();
} else {
    if ($user->UserRole != "admin") {
        header("Location: index.php");
        exit();
    }
}

?>
<!doctype html>
<html>

<head>
    <?php include("shared/termSection.php.php"); ?>
    <script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyDT01oe_DTxTZ5Ts8ALfkeTCAYuz-S9lJ8"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="lib/js/camera-helper.js?v=121"></script>

    <script>
        var mapZoomLevel = 11;
        var locationindex = 0;
        var locations = [{
                'name': 'Oxford, England',
                'latitude': 51.73213,
                'longitude': -1.20631
            },
            {
                'name': 'Quito, Ecuador',
                'latitude': -0.2333,
                'longitude': -78.5167
            },
            {
                'name': 'Ushuaia, Argentina',
                'latitude': -54.8000,
                'longitude': -68.3000
            },
            {
                'name': 'McMurdo Station, Antartica',
                'latitude': -77.847281,
                'longitude': 166.667942
            },
            {
                'name': 'Norilsk, Siberia',
                'latitude': 69.3333,
                'longitude': 88.2167
            },
            {
                'name': 'Greenwich, England',
                'latitude': 51.4800,
                'longitude': 0.0000
            },
            {
                'name': 'Suva, Fiji',
                'latitude': -18.1416,
                'longitude': 178.4419
            },
            {
                'name': 'Tokyo, Japan',
                'latitude': 35.6833,
                'longitude': 139.6833
            },
            {
                'name': 'Mumbai, India',
                'latitude': 18.9750,
                'longitude': 72.8258
            },
            {
                'name': 'New York, USA',
                'latitude': 40.7127,
                'longitude': -74.0059
            },
            {
                'name': 'Moscow, Russia',
                'latitude': 55.7500,
                'longitude': 37.6167
            },
            {
                'name': 'Cape Town, South Africa',
                'latitude': -33.9253,
                'longitude': 18.4239
            },
            {
                'name': 'Cairo, Egypt',
                'latitude': 30.0500,
                'longitude': 31.2333
            },
            {
                'name': 'Sydney, Australia',
                'latitude': -33.8650,
                'longitude': 151.2094
            },
        ];
    </script>
    <style>
        div#topbar {
            position: absolute;
            width: 350px;
            right: 20px;
            z-index: 1;
            background: white;
            padding: 10px;
        }
    </style>

</head>

<body style="margin: 0px;">
    <div id="topbar">
        <img src="https://maps.gstatic.com/mapfiles/ms2/micons/red.png" style="height:15px;"> = Center
        <img src="https://maps.gstatic.com/mapfiles/ms2/micons/pink.png" style="height:15px;"> = Random
        <br><br>
        <select id="location_switch" style="display: none;">
            <script>
                for (i = 0; i < locations.length; i++) {
                    document.write('<option value="' + i + '">' + locations[i].name + '</option>');
                }
            </script>
        </select>
        Number Of Markers <input type="number" id="numberRandomPoints" value="200" placeholder="enter number of Random markers">

        <br><br>
        Distance in Meters <input type="number" id="distanceLimit" value="2000" placeholder="Distance in meters">
        <br><br>
        Marker's Worth <input type="number" id="worthofPnt" value="1" placeholder="Marker's Worth">
        <br><br>
        Campaign Name <input type="text" id="CampaignName" placeholder="Campaign Name">
        <br><br>
        <input type="button" id="ClrMarker" value="Clear Markers" onclick="clearMarkers()">

        <input type="button" id="SaveMrkrs" value="Save Markers" onclick="SavePointerMarkers()">
        <a href="/logout.php">Logout</a>
        <br>
        <label id="showProgress"></label>
    </div>

    <div id="map_canvas" style="position:absolute;  height:100%;width:100%;"></div>

    <script>
        var markers = [];
        var currentcircle;
        //Create the default map
        var mapcenter = new google.maps.LatLng(locations[locationindex].latitude, locations[locationindex].longitude);
        var myOptions = {
            zoom: mapZoomLevel,
            scaleControl: true,
            center: mapcenter
        };
        var map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);


        google.maps.event.addListener(map, 'click', function(event) {
            startDroppingMarker(event.latLng.lat(), event.latLng.lng())
        });


        //Add a unique center marker
        function addCenterMarker(centerposition, title) {
            var newmarker = new google.maps.Marker({
                position: mapcenter,
                map: map,
                title: title,
                zIndex: 3
            });

            markers.push(newmarker);
            return newmarker;
        }

        //Draw a circle on the map
        function drawRadiusCircle(map, marker, distance) {
            currentcircle = new google.maps.Circle({
                map: map,
                radius: distance
            });
            currentcircle.bindTo('center', marker, 'position');
        }

        //Create markers for the randomly generated points
        function createRandomMapMarkers(map, mappoints) {
            for (var i = 0; i < mappoints.length; i++) {
                //Map points without the east/west adjustment
                var newmappoint = new google.maps.LatLng(mappoints[i].latitude, mappoints[i].longitude);
                var marker = new google.maps.Marker({
                    position: newmappoint,
                    icon: 'https://maps.gstatic.com/mapfiles/ms2/micons/pink.png',
                    map: map,
                    title: mappoints[i].latitude + ', ' + mappoints[i].longitude + ' | ' + mappoints[i].distance + 'm',
                    zIndex: 2
                });
                markers.push(marker);

            }
        }

        //Destroy all markers
        function clearMarkers() {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(null);
            }
            markers = [];
        }

        $('#location_switch').change(function() {
            var newlocation = $(this).val();
            startDroppingMarker(locations[newlocation].latitude, locations[newlocation].longitude)
        });

        function startDroppingMarker(latitude, longitude) {
            mapcenter = new google.maps.LatLng(latitude, longitude);
            map.panTo(mapcenter);
            centermarker = addCenterMarker(mapcenter, latitude + ', ' + longitude);
            mappoints = MapService.GenerateMapPoints({
                latitude,
                longitude
            }, parseInt($("#distanceLimit").val()), parseInt($("#numberRandomPoints").val()));
            createRandomMapMarkers(map, mappoints);
        }

        async function SavePointerMarkers() {
            var mrs = [];

            if ($("#CampaignName").val() == "") {
                alert("Please Add Campaign Name");

                return;

            }
            var msk = [];
            for (let index = 0; index < markers.length; index++) {
                const element = markers[index];

                var folder = {
                    FolderName: $("#CampaignName").val(),
                    UserId: '<?php echo $user->Id ?>',
                    Longitude: element.position.lng(),
                    Latitude: element.position.lat(),
                    FenceAreaInFeet: 0,
                    FolderIcon: "",
                    IsPrivateFence: 0,
                    PhoneNumber: "",
                    Email: "",
                    WorthValue: $("#worthofPnt").val(),
                    MarkerType: "Point"
                };

                msk.push(folder);
            }
            let response = await MarkerAR.dropMarkers(msk);
            $("#showProgress").text("")
            alert("saved");
            LoaderService.HideLoader();

        }
    </script>
</body>

</html>
<?php
require __DIR__ . '/vendor/autoload.php';
include(__DIR__ . "/Configuration.php");

use Services\SessionService;
use Services\UserService;

$userService = new UserService();
$sessionService = new SessionService();
if (isset($_POST['code'])) {

  if (isset($_REQUEST['error'])) {
    header("Location: login.php");
  }


  $response = http('https://appleid.apple.com/auth/token', [
    'grant_type' => 'authorization_code',
    'code' => $_POST['code'],
    'redirect_uri' => APPLE_CLIENT_REDIRECT_URL,
    'client_id' => APPLE_CLIENT_ID,
    'client_secret' => APPLE_CLIENT_SECRET,
  ]);

  if (!isset($response->access_token)) {
    header("Location: login.php");
  }

  $claims = explode('.', $response->id_token)[1];
  $claims = json_decode(base64_decode($claims));


  $user = new stdClass();
  $user->Email = $claims->email;
  $user->Name  = $claims->email;
  $user->Image = "";

  //Check user is blocked or not

  $userBlock =    $userService->CheckUserBlock($user);
  if ($userBlock == '0') {
    $userLoginInfo =    $userService->SaveUser($user);
    $sessionService->SetUser($userLoginInfo);
    
    //save login log
    $user_detail = new stdClass();
    $user_detail->Id = $userLoginInfo->Id;
    $user_detail->Action = 'login';
    $userLoginDetail =  $userService->SaveLoginDetail($user_detail);
    header("Location: index.php");
  } else {
    header("Location: login.php");
  }
}

function http($url, $params = false)
{
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  if ($params)
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
  curl_setopt($ch, CURLOPT_HTTPHEADER, [
    'Accept: application/json',
    'User-Agent: curl', # Apple requires a user agent header at the token endpoint
  ]);
  $response = curl_exec($ch);
  return json_decode($response);
}

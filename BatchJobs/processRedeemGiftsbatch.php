<?php
require_once(__DIR__ . '/../Configuration.php');
require_once(__DIR__ . '/../vendor/autoload.php');

use DataAccessLayer\UserFolderDbService;
use Enums\TremendousCallTypeConstant;
use Services\CurlTremendousService;

class ProcessRedeemGiftsBatch
{

    private $userFolderDbService;
    private $curlTremendousService;
    public function __construct()
    {
        $this->userFolderDbService = new UserFolderDbService();
        $this->curlTremendousService = new CurlTremendousService();
    }

    public function Start()
    {

        $userList = $this->userFolderDbService->GetRedeemAblePointsAndUser(2);

        foreach ($userList as $key => $user) {

            if ($user->IsEmailConfirmed == "1") {

                $trsOption = new stdClass();
                $trsOption->Amount = 1;
                $trsOption->RecipientEmail = $user->Email;
                $trsOption->ReferenceNumberId = $this->curlTremendousService->PrepareTremendousCallWithMultipleFolderId(explode(",", $user->userfolderIdList));
            
                $this->curlTremendousService->CreateTremendusReward($trsOption, TremendousCallTypeConstant::Email);

            } else {
                //TODO
            }
        }
    }
}


$processRedeemGiftsBatch = new ProcessRedeemGiftsBatch();
$processRedeemGiftsBatch->Start();

<?php
require __DIR__ . '/vendor/autoload.php';
require_once(__DIR__ . '/Configuration.php');

use Common\Helper;
use Services\SessionService;
use Services\UserService;

$userService = new UserService();
$sessionService = new SessionService();


//redirect to login page if user not admin
if ($sessionService->GetUser() == null) {
    header("Location: login.php");
    exit();
}


//Block or Unblock Users
if ( isset( $_GET['ui'] ) && isset( $_GET['s'] ) ) {
	$user 			= new stdClass();
	$user->id 		= $_GET['ui'];
	$user->status 	= $_GET['s'];
	$userBlocks 	= $userService->BlockUnblockUsers( $user );		
	if ( $_GET['s'] == '1' ) {
		$message = 'User UnBlocked Successfully';		
	}elseif ( $_GET['s'] == '0' ) {
		$message = 'User Blocked Successfully';				
	}
	$userLogs =    $userService->GetUsers();
}elseif ( isset($_GET["role_s"]) || isset($_GET["text_s"]) ) {
	$user 			= new stdClass();
	$user->role_s 		= $_GET['role_s'];
	$user->text_s 	= $_GET['text_s'];
	$userLogs =    $userService->GetUsers( $user );	
}else{
	$userLogs =    $userService->GetUsers();	
}



?>
<!DOCTYPE html>
<html>
<title>W3.CSS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="css/w3.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
	.page-content{
		margin-left:25%
	}
	 @media screen and (max-width: 600px){
		.page-content{
			margin-left:41%
		}	    
	}
</style>
<body>

<!-- Sidebar -->
<div class="w3-sidebar w3-light-grey w3-bar-block" style="width:25%">  
  <h3 class="w3-bar-item"><i class="fa fa-bars"></i> Menu</h3>
  <a href="adminLoginLog.php" class="w3-bar-item w3-button w3-border-top"><i class="fa fa-history"></i> Login Log</a>
  <a href="adminUsers.php" class="w3-bar-item w3-button w3-black"><i class="fa fa-user"></i> Users</a>
  <a href="adminCreateUser.php" class="w3-bar-item w3-button "><i class="fa fa-user"></i>Create User</a>
  <a href="adminPaymentTransaction.php" class="w3-bar-item w3-button"><i class="fa fa-exchange"></i> Payment Transactions</a>
  <a href="logout.php" class="w3-bar-item w3-button"><i class="fa fa-sign-out"></i> Logout</a>
</div>

<!-- Page Content -->
<div class="page-content">

<div class="w3-container w3-white w3-border-bottom">
  <h1>Hi Admin</h1>
</div>

<!-- <img src="img_car.jpg" alt="Car" style="width:100%"> -->

<div class="w3-container">
	<h2>Users Detail</h2>	
	<div class="w3-row-padding w3-margin-bottom">
	  <div class="w3-third">
		<select name="user_role" id="user_role" class="w3-select w3-border">
			<option value="user" <?=(isset( $_GET['role_s'] )) ? ($_GET['role_s'] == 'user') ? 'selected':'' :''?>>User</option>
			<option value="admin" <?=(isset( $_GET['role_s'] )) ? ($_GET['role_s'] == 'admin') ? 'selected':'' :''?>>Admin</option>
		</select>
		</div>
		<div class="w3-third">
		
			<input type="text" name="search" class="w3-input w3-border" value="<?=(isset( $_GET['text_s'] )) ? $_GET['text_s']:''?>" id="search">
	
		</div>
		  <div class="w3-third">
					
			<button type="button" value="Search" id="btn_search" class="w3-btn w3-blue-grey">Search</button>		
	
		</div>
	</div>
	<?php if ( isset( $message ) ) {
		$alert = new stdClass();		
		$alert->type = 'success';
		$alert->message = $message;
		echo Helper::showAlert( $alert );		
	}?>
	<table class="w3-table-all">
	    <tr>
	      <th>Image</th>
	      <th>Name</th>
	      <th>Email</th>
	      <th>Role</th>
	      <th>Status</th>
	    </tr>
		<?php if ( !empty($userLogs) ) { ?>	
			<?php while($row = $userLogs->fetch_assoc()) { 
				if ( $row['status'] == '1' ) {
			    	echo '<tr> <td>'.$row['Image'].'</td> <td>'.$row['Name'].'</td> <td>'.$row['Email'].'</td> <td>'.$row['role'].'</td> <td><a class="w3-btn w3-green w3-round-xlarge" href="?ui='.$row['Id'].'&s=0">Block</a></td> </tr>';
				}else{
			    	echo '<tr> <td>'.$row['Image'].'</td> <td>'.$row['Name'].'</td> <td>'.$row['Email'].'</td> <td>'.$row['role'].'</td> <td><a class="w3-btn w3-red w3-round-xlarge" href="?ui='.$row['Id'].'&s=1">UnBlock</a></td> </tr>';
				}
			 } ?>
		<?php } ?>   
  </table>
</div>

</div>

<script src="lib/js/jquery.min.js"></script>
<script type="text/javascript">
	 var uri = window.location.href.toString();
	 if (uri.indexOf("?") > 0) {
	     var clean_uri = uri.substring(0, uri.indexOf("?"));
	     window.history.replaceState({}, document.title, clean_uri);
	 }
	
 $(document).ready(function(){
 	setTimeout(function(){
 		$('.w3-card-2').slideUp();
 	}, 3000);

 	//search code
 	$('#btn_search').click(function(){
 		var role_s=$('#user_role').val();
 		var text_s=$('#search').val();
 		window.location.href="?role_s="+role_s+"&text_s="+text_s;
 	});
 });
</script>      
</body>
</html>

<?php
require __DIR__ . '/vendor/autoload.php';
require_once(__DIR__ . '/Configuration.php');

use Services\GoogleAuthService;
use Services\SessionService;
use Services\UserService;

// Google passes a parameter 'code' in the Redirect Url
if (isset($_GET['code'])) {
    try {

        $gapi = new GoogleAuthService();
        $userService = new UserService();
        $sessionService= new SessionService();
        // Get the access token 
        $data = $gapi->GetAccessToken(GOOGLE_CLIENT_ID, GOOGLE_CLIENT_REDIRECT_URL, GOOGLE_CLIENT_SECRET, $_GET['code']);

        // Get user information
        $userInfo = $gapi->GetUserProfileInfo($data['access_token']);
        $user = new stdClass();
        $user->Email = $userInfo['email'];
        $user->Name  = $userInfo['name'];
        $user->Image = $userInfo['picture'];

        //Check user is blocked or not

        $userBlock =    $userService->CheckUserBlock($user);
        if ( $userBlock == '0' ) {
            $userLoginInfo =    $userService->SaveUser($user);
            $sessionService->SetUser($userLoginInfo);
            //save login log
            $user_detail = new stdClass();
            $user_detail->Id = $userLoginInfo->Id;
            $user_detail->Action = 'login';
            $userLoginDetail =    $userService->SaveLoginDetail($user_detail);
            header("Location: index.php");            
        }else{
            header("Location: login.php");
        }
        
    } catch (Exception $e) {
        header("Location: login.php");
        // echo $e->getMessage();
        // exit();
    }
}
?>

<head>

</head>

<body>
    Loading...
</body>

</html>
<?php

require __DIR__ . '/vendor/autoload.php';
require_once(__DIR__ . '/Configuration.php');
include("shared/header.php");
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

use DataAccessLayer\UserFolderDbService;

$userFolderService = new UserFolderDbService();
$userList = $userFolderService->GetPointsAndUser(-1);

?>

<!DOCTYPE html>
<html>

<head>
    <?php include("shared/termSection.php.php"); ?>
    <?php include("shared/commonCSS.php"); ?>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<style type="text/css">
    .result-page {
        background: #dce2f0
    }

    .custom-navbar {
        height: 300px;
    }

    .custom-navbar nav {
        background: url(images/leaderboard-bg.jpg?v=2)no-repeat;
        height: 325px;
        background-size: cover;
    }

    .stairs {
        margin: 0px auto;
        text-align: center;
        position: relative;
        left: -50%;
    }

    .head-container {
        position: absolute;
        left: 50%;
        bottom: 11px;
    }

    .stairs img {
        width: 300px;
    }

    .pg-head {
        margin: 0px;
        padding: 0px;
        font-size: 20px;
        font-weight: bold;
        text-align: center;
        padding: 15px;
    }

    .user-head-container {
        position: absolute;
        left: 50%;
    }

    .usr-head-inner {
        text-align: center;
        position: relative;
        left: -50%;
        display: flex;
        justify-content: space-around;
        width: 300px;
    }


    .stair-cont {
        position: absolute;
        left: 50%;
    }

    .str-inner {
        text-align: center;
        position: relative;
        left: -50%;
        display: flex;
        justify-content: space-around;
        width: 300px;
        margin-top: -101px;
    }

    .str-list label {
        font-size: 23px;
        font-weight: bold;
        color: #182335
    }

    .str-list small {
        color: #b3b3b3;
        font-size: 14px;
    }

    .str-list {
        display: flex;
        flex-direction: column;
        height: 30px;
        line-height: 20px;
        margin-top: 20px;
    }

    .str-list:nth-child(1) {
        margin-top: 13px;
        padding-left: 3px;
    }

    .str-list:nth-child(2) {
        margin-top: -6px;
    }

    .str-list:nth-child(3) {
        margin-top: 22px;
    }

    .usr-lst img {
        width: 65px;
        height: 65px;
        object-fit: cover;
        border-radius: 50%;
        border: 2px solid #fff;
    }

    .usr-lst small {
        position: absolute;
        top: 46px;
        left: 30px;
        font-weight: bold;
        vertical-align: top;
        line-height: 27px;
        font-size: 14px;
        font-style: normal;
    }

    .hexagon {
        background: url(images/leaderboard-hex.svg)no-repeat;
        width: 35px;
        height: 29px;
        display: block;
        background-size: 34px;
    }

    .usr-lst {
        position: relative;
    }

    .name {
        line-height: 19px;
        font-weight: bold;
        width: 100px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        margin-top: -8px;
        font-size: 13px;
    }

    .usr-lst:nth-child(1) {
        margin-top: 21px;
    }

    .usr-lst:nth-child(3) {
        margin-top: 44px;
    }

    .usr-lst:nth-child(1) .name {}

    .usr-lst:nth-child(3) .name {
        padding-left: 17px;
    }

    .section-list {
        background: #fff;
        margin: 0px 5px;
        margin-top: 12px;
        padding: 0px;
        min-height: 51vh;
    }

    .search-box i {
        position: absolute;
    }

    .search-box {
        background: #fff;
        border-radius: 27px;
        box-shadow: 1px 0 6px 2px #a2a8ad38;
        height: 46px !important;
        margin: 0px 20px !important;
        margin-top: -28px !important;
    }

    .search {
        background: url(images/leaderboard-search.svg);
        width: 20px;
        height: 20px;
        position: absolute;
        right: 18px;
        margin-top: 12px;
    }


    .search-box input[type=text] {
        border-bottom: 0px !important;
        padding: 0px 30px !important;
        box-sizing: border-box;
        margin-top: -15px;
        margin-bottom: 0px;
        vertical-align: middle;
    }

    .search-box input[type=text]:not(.browser-default):focus:not([readonly]) {
        border-bottom: 0px !important;
        box-shadow: 0 1px 0 0 #fff !important;
    }

    .userlist {
        padding-top: 20px;
    }

    .userlist ul {
        list-style: none;
        margin: 0px;
        padding: 0px;
    }

    .userlist ul li img {
        width: 50px;
        height: 50px;
        object-fit: cover;
        border-radius: 50%;
        position: absolute;
    }

    .userlist ul li {
        position: relative;
    }

    .userlist ul li a {
        display: block;
        padding: 20px;
        border-bottom: 1px solid #f4f4f4
    }

    .userlist ul li h2 {
        font-size: 16px;
        color: #353b48;
        font-weight: bold;
        margin: 0px;
        padding: 0px;
    }

    .userlist ul li label {
        padding-left: 65px;
        display: block;
        padding-top: 8px;
    }

    .userlist ul li span {
        font-size: 14px;
    }

    .userlist ul li i {
        position: absolute;
        right: 18px;
        font-weight: bold;
        border: 2px solid #5a27c4;
        font-style: normal;
        color: #5a27c4;
        border-radius: 50%;
        width: 26px;
        height: 26px;
        text-align: center;
        line-height: 22px;
        top: 32px;
        font-size: 13px;
    }

    .search-container {
        position: absolute;
        bottom: -4px;
        width: 100%;


    }

    .top-skin {
        background: #fff;
        height: 30px;


        margin: 0px 5px;
        margin-top: -34px;
        z-index: 1;
        border-radius: 15px 15px 0px 0px;
    }
</style>

<body class="result-page">
    <div class="back-arrow" style="z-index: 999;">

        <a style="color:white" href="/index.php"><i class="fas fa-arrow-left" style="font-size: 20px;"></i></button>
    </div>
    <div class="navbar-fixed custom head custom-navbar">
        <nav>
            <div class="nav-wrapper">

                <h2 class="pg-head">Leaderboard</h2>

                <div class="user-head-container">
                    <div class="usr-head-inner">
                        <?php

                        if (count($userList) >= 2) {

                        ?>
                            <div class="usr-lst">
                                <img src="<?php echo empty($userList[1]->Image) ? "images/user.svg" : $userList[1]->Image; ?>">
                                <small class="hexagon">2</small>
                                <div class="name"><?php echo $userList[1]->UserName; ?></div>
                            </div>

                        <?php
                        } else {

                        ?>

                            <div class="usr-lst">
                                <img src="images/user.svg">
                                <small class="hexagon">2</small>
                                <div class="name">N/A</div>
                            </div>

                        <?php
                        }
                        ?>


                        <?php

                        if (count($userList) >= 1) {

                        ?>
                            <div class="usr-lst">
                                <img src="<?php echo empty($userList[0]->Image) ? "images/user.svg" : $userList[0]->Image; ?>">
                                <small class="hexagon">1</small>
                                <div class="name"><?php echo $userList[0]->UserName ?></div>
                            </div>

                        <?php
                        } else {

                        ?>

                            <div class="usr-lst">
                                <img src="images/user.svg">
                                <small class="hexagon">1</small>
                                <div class="name">NA</div>
                            </div>

                        <?php
                        }
                        ?>



                        <?php

                        if (count($userList) >= 3) {

                        ?>
                            <div class="usr-lst">
                                <img src="<?php echo empty($userList[2]->Image) ? "images/user.svg" : $userList[2]->Image; ?>">
                                <small class="hexagon">3</small>
                                <div class="name"><?php echo $userList[2]->UserName ?></div>
                            </div>

                        <?php
                        } else {

                        ?>

                            <div class="usr-lst">
                                <img src="images/user.svg">
                                <small class="hexagon">3</small>
                                <div class="name">NA</div>
                            </div>

                        <?php
                        }
                        ?>




                    </div>
                </div>

                <div class="head-container">
                    <div class="stairs">
                        <img src="images/leaderboard-stairs.png">

                        <div class="stair-cont">
                            <div class="str-inner">
                                <div class="str-list">
                                    <label><?php echo count($userList) >= 2 ?  $userList[1]->Points : "No"; ?></label>
                                    <small>Points</small>

                                </div>

                                <div class="str-list">
                                    <label><?php echo count($userList) >= 1 ?  $userList[0]->Points : "No"; ?></label>
                                    <small>Points</small>

                                </div>

                                <div class="str-list">
                                    <label><?php echo count($userList) >= 3 ?  $userList[2]->Points : "No"; ?></label>
                                    <small>Points</small>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>



            </div>

            <div class="search-container">
                <div class="search-box input-field">
                    <i class="search"></i>
                    <input type="text" placeholder="search User" name="" id="txtLeaderboardSearch">

                </div>
                <div class="top-skin"></div>
            </div>
        </nav>

    </div>

    <section class="section-list">

        <div class="userlist">
            <ul>

                <?php for ($i = 3; $i < count($userList); $i++) { ?>
                    <li>
                        <a href="#"><img src="<?php echo empty($userList[$i]->Image) ? "images/user.svg" : $userList[$i]->Image; ?>"><label>
                                <h2><?php echo $userList[$i]->UserName ?></h2><span><?php echo $userList[$i]->Points; ?> Points</span>
                            </label>
                        </a>
                        <i><?php echo $i + 1; ?></i>
                    </li>
                <?php } ?>



            </ul>

        </div>

    </section>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script>
        $(document).ready(() => {
            $("#txtLeaderboardSearch").keyup(function() {
                $(".userlist ul li").each((i, itm) => {
                    if ($("#txtLeaderboardSearch").val() == "" || $(itm).find("a label h2").text().toLowerCase().indexOf($("#txtLeaderboardSearch").val().toLowerCase()) > -1) {
                        $(itm).show();
                    } else {
                        $(itm).hide();
                    }
                })
            })
        })
    </script>
</body>

</html>
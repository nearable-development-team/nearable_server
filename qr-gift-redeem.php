<?php

use Services\FolderService;
use Services\SessionService;

include("shared/authorize.php");
$sessionService = new SessionService();
$sessionUser = $sessionService->GetUser();
$folderService = new FolderService();
$folderDetail = $folderService->SearchFolderBySharedId($_GET["usid"])
?>
<html lang="en">

<head>
    <?php include("shared/termSection.php.php"); ?>
    <title>Redeem QR Gift</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://aframe.io/releases/1.2.0/aframe.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/donmccurdy/aframe-extras@v6.1.1/dist/aframe-extras.min.js"></script>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/style.css?v=16" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/aframe-look-at-component@0.8.0/dist/aframe-look-at-component.min.js"></script>
    <script src="lib/arjs-aframe/aframe-ar-nft.js?v=9"></script>

    <script src="lib/js/camera-helper.js?v=121"></script>
</head>

<body>
    <input type="hidden" id="h_userId" name="h_userId" value="<?php echo $sessionUser->Id; ?>">
    <a-scene renderer="logarithmicDepthBuffer: true" vr-mode-ui="enabled: false" embedded arjs="sourceType: webcam; debugUIEnabled: false;">
        <a-assets>

            <a-asset-item id="glbtestmodel" src="/images/ar-models/<?php echo str_replace(".png", ".glb", $folderDetail->FolderIcon);  ?>" response-type="arraybuffer">
            </a-asset-item>

        </a-assets>
        <a-entity camera look-controls position="0 1.6 0">
            <a-entity id="glbtest" gltf-model="#glbtestmodel" animation-mixer="clip:*" position="0 -.3 -2.2" scale="1 1 1">
            </a-entity>
        </a-entity>

    </a-scene>
    <button class="btn t-bg" style="position: absolute;top: 2%;left: 5%;width: 90%;" id="qrRedeemBtn">Redeem Gift</button>
    <a class="btn t-bg t-bg-action" href="/index.php" style="position: absolute;top: 5%;left: 5%;width: 90%;">Cancel</a>

</body>
<script>
    $(document).ready(function() {
        $("#qrRedeemBtn")
            .click(async () => {
                await FolderService.AddSharedFolder({
                    sharedId: "<?php echo $_GET["usid"]; ?>",
                    userId: $("#h_userId").val(),
                    markers: []
                });
                location.href = "<?php echo DOMAIN_NAME; ?>";
            });
    });
</script>

</html>
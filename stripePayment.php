<?php    
    require_once(__DIR__ . '/Configuration.php');
?>
<!DOCTYPE html>
<html>
<head>
  <title>Stripe Payment</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <style type="text/css">
        .panel-title {
        display: inline;
        font-weight: bold;
        }
        .display-table {
            display: table;
        }
        .display-tr {
            display: table-row;
        }
        .display-td {
            display: table-cell;
            vertical-align: middle;
            width: 61%;
        }
        #apple-pay-button {
            display: none;
            background-color: black;
            background-image: -webkit-named-image(apple-pay-logo-white);
            background-size: 100% 100%;
            background-origin: content-box;
            background-repeat: no-repeat;
            width: 100%;
            height: 44px;
            padding: 10px 0;
            border-radius: 10px;
          }
    </style>
    
</head>
<body>
  
<div class="container">
  
    <h1>Enter Credentials</h1>
  
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default credit-card-box">
                <div class="panel-heading display-table" >
                    <div class="row display-tr" >
                        <h3 class="panel-title display-td" >Payment Details</h3>
                        <div class="display-td" >                            
                            <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
                        </div>
                    </div>                    
                </div>
                <div class="panel-body">                    
  
                    <form role="form" action="stripeCallback.php" method="post" class="require-validation"
                                                     data-cc-on-file="false"
                                                    data-stripe-publishable-key="<?=STRIPE_PUBLISHABLE_KEY?>"
                                                    id="payment-form">
                        <!-- @csrf -->                        
                        <div class='form-row row'>
                            <div class='col-xs-12 form-group required'>
                                <label class='control-label'>Card Holder Name</label> <input
                                   type="text" id="name" name="name" class='form-control' required>
                            </div>
                        </div>
                        <div class='form-row row'>
                            <div class='col-xs-12 form-group required'>
                                <label class='control-label'>Email</label> <input
                                   type="email" id="email" name="email" class='form-control' required>
                            </div>
                        </div>
                        <div class='form-row row'>
                            <div class='col-xs-12 form-group required'>
                                <label class='control-label'>Name on Card</label> <input
                                     name="name_card" class='form-control' size='4' type='text' required>
                            </div>
                        </div>
  
                        <div class='form-row row'>
                            <div class='col-xs-12 form-group card required'>
                                <label class='control-label'>Card Number</label> <input
                                     name="card_no" autocomplete='off' class='form-control card-number' size='20'
                                    type='text' required>
                            </div>
                        </div>
  
                        <div class='form-row row'>
                            <div class='col-xs-12 col-md-4 form-group cvc required'>
                                <label class='control-label'>CVC</label> <input name="card_cvc" autocomplete='off'
                                    class='form-control card-cvc' placeholder='ex. 311' size='4'
                                    type='text' required>
                            </div>
                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                <label class='control-label'>Expiration Month</label> <input
                                     name="card_expiry_month" class='form-control card-expiry-month' placeholder='MM' size='2'
                                    type='text' required>
                            </div>
                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                <label class='control-label'>Expiration Year</label> <input
                                     name="card_expiry_year" class='form-control card-expiry-year' placeholder='YYYY' size='4'
                                    type='text' required>
                            </div>
                        </div>
  
                        <div class='form-row row'>
                            <div class='col-md-12 error form-group hide'>
                                <div class='alert-danger alert'>Please correct the errors and try
                                    again.</div>
                            </div>
                        </div>
  
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-primary btn-lg btn-block" type="submit">Pay Now ($100)</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">                                
                                <button id="apple-pay-button"></button>
                            </div>
                        </div>
                          
                    </form>
                </div>
            </div>        
        </div>
    </div>
      
</div>
  
</body>
  
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  
<script type="text/javascript">
$(function() {
    var $form         = $(".require-validation");
  $('form.require-validation').bind('submit', function(e) {
    var $form         = $(".require-validation"),
        inputSelector = ['input[type=email]', 'input[type=password]',
                         'input[type=text]', 'input[type=file]',
                         'textarea'].join(', '),
        $inputs       = $form.find('.required').find(inputSelector),
        $errorMessage = $form.find('div.error'),
        valid         = true;
        $errorMessage.addClass('hide');
 
        $('.has-error').removeClass('has-error');
    $inputs.each(function(i, el) {
      var $input = $(el);
      if ($input.val() === '') {
        $input.parent().addClass('has-error');
        $errorMessage.removeClass('hide');
        e.preventDefault();
      }
    });
  
    if (!$form.data('cc-on-file')) {
      e.preventDefault();
      Stripe.setPublishableKey($form.data('stripe-publishable-key'));
      Stripe.createToken({
        number: $('.card-number').val(),
        cvc: $('.card-cvc').val(),
        exp_month: $('.card-expiry-month').val(),
        exp_year: $('.card-expiry-year').val()
      }, stripeResponseHandler);
    }
  
  });
  
  function stripeResponseHandler(status, response) {
        if (response.error) {
            $('.error')
                .removeClass('hide')
                .find('.alert')
                .text(response.error.message);
        } else {
            // token contains id, last4, and card type
            var token = response['id'];            
            // insert the token into the form so it gets submitted to the server
            $form.find('input[type=text]').empty();
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
            $form.get(0).submit();
        }
    }
  
});

//apple pay code
Stripe.setPublishableKey('pk_test_51Hst1FGiLyUb4BrVF20hLOEo505tAY52eHbVHyIKP0WqalI2S5OLbcRoHnNCRg5SPJNwBd2VFoyn3zEcvHMXE0pU004YNYZ8RD');
Stripe.applePay.checkAvailability(function(available) {
  if (available) {
    document.getElementById('apple-pay-button').style.display = 'block';
  }
});

document.getElementById('apple-pay-button').addEventListener('click', beginApplePay);

function beginApplePay() {
  var paymentRequest = {
    countryCode: 'US',
    currencyCode: 'USD',
    total: {
      label: 'Stripe.com',
      amount: '19.99'
    }
  };
  var session = Stripe.applePay.buildSession(paymentRequest,
    function(result, completion) {

    $.post('/charges', { token: result.token.id }).done(function() {
      completion(ApplePaySession.STATUS_SUCCESS);
      // You can now redirect the user to a receipt page, etc.
      window.location.href = '/success.html';
    }).fail(function() {
      completion(ApplePaySession.STATUS_FAILURE);
    });

  }, function(error) {
    console.log(error.message);
  });

  session.oncancel = function() {
    console.log("User hit the cancel button in the payment window");
  };

  session.begin();
}

</script>
</html>
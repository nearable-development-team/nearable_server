<?php

namespace DataAccessLayer;

use Models\Folder;
use Settings\DbInterface;

class FolderDbService
{
    public function GetFolders()
    {
        $Db = new DbInterface();
        $folders = array();
        $result = $Db->getTable("SELECT `Id`, `Name` FROM `folder`");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $b = new Folder();
                $b->Id = $row["Id"];
                $b->Name = $row["Name"];
                array_push($folders, $b);
            }
        }
        return $folders;
    }

    public function SaveFolder($name)
    {
        $sql = "";
        $exitingId = $this->GetFolderIdByName($name);

        if ($exitingId == "0") {
            $sql = "INSERT INTO folder (Name) VALUES ('" . $name . "')";
        } else {
            $sql = "update  folder set Name='" . $name . "' where Id='" . $exitingId . "'";
        }

        $Db = new DbInterface();
        if (strlen($sql)) {
            $result = $Db->getTable($sql);
            if ($result === true) {
                if ($exitingId == "0") {
                    $exitingId = $Db->GetConnectionObj()->insert_id;
                }
            } else {
                return "0";
            }
        }
        return $exitingId;
    }

    public function GetFolderIdByName($name)
    {
        $Id = "0";
        $Db = new DbInterface();
        $result = $Db->getTable("select Id from folder where Name='" . $name . "'");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $Id = $row["Id"];
            }
        }

        return $Id;
    }
}

<?php

namespace DataAccessLayer;

use Models\User;
use Settings\DbInterface;

class UserDbService
{
    private $Db;
    public function __construct()
    {
        $this->Db = new DbInterface();
    }
    public function SaveUser($user)
    {
        $sql = "";
        $user->Id = $this->GetUserByEmail($user->Email)->Id;
        if ($user->Id == "0") {
            $sql = "INSERT INTO `user` (`Name`,`Email`,`Image`) VALUES ('" . $user->Name . "','" . $user->Email . "','" . $user->Image . "')";
        } else {
            $sql = "update `user` set `Name`='" . $user->Name . "', `Image`='" . $user->Image . "' where Id='" . $user->Id . "'";
        }

        if (strlen($sql)) {
            $result = $this->Db->getTable($sql);
            if ($result === true) {
                if ($user->Id == 0)
                    $user->Id = $this->Db->GetConnectionObj()->insert_id;
            }
            return $this->GetUserById($user->Id);
        }
    }

    public function GetUserByEmail($email)
    {
        $result = $this->Db->getTable("SELECT `Id`, `Name`,`Email`,`IsEmailConfirmed`,`role` FROM `user` where `Email`='" . $email . "'");
        return $this->MapUserFromDb($result);
    }

    private function MapUserFromDb($result)
    {
        $user = new User();
        $user->Id = "0";
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $user->Id = $row["Id"];
                $user->Name = $row["Name"];
                $user->Email = $row["Email"];
                $user->IsEmailConfirmed = $row["IsEmailConfirmed"];
                $user->UserRole = $row["role"];
            }
        }
        return $user;
    }

    public function GetUserById($id)
    {
        $result = $this->Db->getTable("SELECT `Id`, `Name`,`Email`,`IsEmailConfirmed`,`role` FROM `user` where `Id`='" . $id . "'");
        return $this->MapUserFromDb($result);
    }

    public function CheckUserBlock($email)
    {
        $Id = "0";
        $result = $this->Db->getTable("SELECT `Id`, `Name`,`Email` FROM `user` where `Email`='" . $email . "' and `status`='0'");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $Id = $row["Id"];
            }
        }
        return $Id;
    }


    public function SaveLoginDetail($user)
    {
        $sql = "INSERT INTO `user_login_logs` (`user_id`,`login_time`,`logout_time`,`ip_address`,`action`) VALUES ('" . $user->Id . "','" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "', '" . $_SERVER['REMOTE_ADDR'] . "', '" . $user->Action . "')";

        if (strlen($sql)) {
            $result = $this->Db->getTable($sql);
        }
    }

    public function GetUserLog()
    {
        $result = $this->Db->getTable("SELECT user_login_logs.*, (select name from user where id=user_login_logs.user_id) as user_name FROM `user_login_logs` order by user_id ");
        if ($result->num_rows > 0) {
            return $result;
        } else {
            return false;
        }
    }

    public function GetUsers($user = NULL)
    {

        if ($user == NULL) {
            $sql = "SELECT * FROM `user` order by id ";
        } else {
            if (!empty($user->text_s)) {
                $sql = "SELECT * FROM `user` where role='" . $user->role_s . "' && Email='" . $user->text_s . "' order by id ";
            } else {
                $sql = "SELECT * FROM `user` where role='" . $user->role_s . "' order by id ";
            }
        }

        $result = $this->Db->getTable($sql);
        if ($result->num_rows > 0) {
            return $result;
        } else {
            return false;
        }
    }

    public function GetPaymentTransaction()
    {
        $result = $this->Db->getTable("SELECT paypal_payments.*, (select name from user where id=paypal_payments.user_id) as user_name FROM `paypal_payments` where paypal_payments.payment_status='success' order by id ");
        if ($result) {
            if ($result->num_rows > 0) {
                return $result;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function ChecAdminUser($user)
    {
        $result = $this->Db->getTable("SELECT `Id`, `Name`,`Email`,`IsEmailConfirmed`,`role` from `user` where Name='" . $user->name . "' and Password='" . $user->password . "' and role='admin' and status='1'");
        return $this->MapUserFromDb($result);
    }

    public function BlockUnblockUsers($user)
    {
        $sql = "";
        if ($user->status == '1') {
            $sql = "update user set status='1' where Id='" . $user->id . "'";
        } elseif ($user->status == '0') {
            $sql = "update user set status='0' where Id='" . $user->id . "'";
        }
        if (strlen($sql)) {
            $result = $this->Db->getTable($sql);
        }
    }

    public function CreateAdminUser($user)
    {
        $sql = "INSERT INTO `user` (`Name`,`Email`,`Password`, `role`) VALUES ('" . $user->name . "','" . $user->email . "','" . $user->password . "', 'admin')";
        if (strlen($sql)) {
            $result = $this->Db->getTable($sql);
        }
    }

    public function ConfirmUserEmail($userId)
    {
        $this->Db->getTable( "Update `user` set IsEmailConfirmed=1 where Id ='" . $userId . "'");
    }

    public function GetActiveAdminUser()
    {
        $user = new User();
        $user->Id = 0;
        $result = $this->Db->getTable("select * from `user` where role='admin' and status=1");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $user->Id = $row["Id"];
            }
        }
        return $user;
    }


    public function CreatePaymentTransaction($paymentDetail)
    {
        $sql = "INSERT INTO `paypal_payments` (`user_id`,`order_id`,`name_card`, `card_no`, `card_expire_date`, `card_cvc`, `payment_status`, `amount`, `payment_response`) VALUES ('" . $paymentDetail->userid . "','" . $paymentDetail->order_id . "','" . $paymentDetail->name_card . "', '" . $paymentDetail->card_no . "', '" . $paymentDetail->card_expire_date . "', '" . $paymentDetail->card_cvc . "', '" . $paymentDetail->payment_status . "', '" . $paymentDetail->amount . "', '" . $paymentDetail->payment_response . "')";

        if (strlen($sql)) {
            $result = $this->Db->getTable($sql);
        }
    }
}

<?php

namespace DataAccessLayer;

use Models\MerchantToken;
use Settings\DbInterface;

class MerchantTokenDbService
{
    public function SaveMerchantToken($merchantTokenDetail)
    {

        $sql = "";
        $merchantTokenDetail->Id = $this->CheckMerchantTokenByMerchantId($merchantTokenDetail->MerchantId);
        if ($merchantTokenDetail->Id == "0") {
            $sql = "INSERT INTO `merchant_auth_code` (`MerchantId`,`AccessToken`,`RefreshToken`,`SquareMerchantId`) VALUES ('" . $merchantTokenDetail->MerchantId . "','" . $merchantTokenDetail->AccessToken . "','" . $merchantTokenDetail->RefreshToken . "','" . $merchantTokenDetail->SquareMerchantId . "')";
        } else {
            $sql = "update `merchant_auth_code` set `AccessToken`='" . $merchantTokenDetail->AccessToken . "', `RefreshToken`='" . $merchantTokenDetail->RefreshToken . "', `SquareMerchantId`='" . $merchantTokenDetail->SquareMerchantId . "', UpdatedDate=now() where MerchantId='" . $merchantTokenDetail->MerchantId . "'";
        }
        $Db = new DbInterface();
        if (strlen($sql)) {
            $result = $Db->getTable($sql);
            if ($result === true) {
                if ($merchantTokenDetail->Id == 0) {
                    $merchantTokenDetail->Id = $Db->GetConnectionObj()->insert_id;
                }
            }
            return $merchantTokenDetail;
        }
    }

    public function CheckMerchantTokenByMerchantId($merchantId)
    {
        $Id = "0";
        $Db = new DbInterface();
        $result = $Db->getTable("SELECT `Id` FROM `merchant_auth_code` where `MerchantId`='" . $merchantId . "'");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $Id = $row["Id"];
            }
        }
        return $Id;
    }

    public function GetMerchantTokenByMerchantId($merchantId)
    {
        $merchantToken = new MerchantToken();
        $Db = new DbInterface();
        $result = $Db->getTable("SELECT `Id`, AccessToken, RefreshToken, SquareMerchantId,UpdatedDate FROM `merchant_auth_code` where `MerchantId`='" . $merchantId . "'");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $merchantToken->Id = $row["Id"];
                $merchantToken->AccessToken = $row["AccessToken"];
                $merchantToken->RefreshToken = $row["RefreshToken"];
                $merchantToken->SquareMerchantId = $row["SquareMerchantId"];
                $merchantToken->UpdatedDate = $row["UpdatedDate"];
            }
        }
        return $merchantToken;
    }
}

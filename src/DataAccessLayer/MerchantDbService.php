<?php

namespace DataAccessLayer;

use Models\Merchant;
use Settings\DbInterface;

class MerchantDbService
{
    public function SaveMerchant($merchant)
    {
        $merchant->Id = $this->CheckMerchantByEmail($merchant->Email);
        if ($merchant->Id == "0") {
            $sql = "INSERT INTO `merchant` (`Name`,`Email`,`Password`) VALUES ('" . $merchant->Name . "','" . $merchant->Email . "','" . $merchant->Password . "')";
            $Db = new DbInterface();
            if (strlen($sql)) {
                $result = $Db->getTable($sql);
                if ($result === true) {
                    if ($merchant->Id == 0) {
                        $merchant->Id = $Db->GetConnectionObj()->insert_id;
                    }
                }
                return $merchant;
            }
        }
    }

    public function CheckMerchantByEmail($email)
    {
        $Id = "0";
        $Db = new DbInterface();
        $result = $Db->getTable("SELECT `Id`, `Name`,`Email` FROM `merchant` where `Email`='" . $email . "'");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $Id = $row["Id"];
            }
        }
        return $Id;
    }
    public function GetMerchantByEmailandPassword($email, $password)
    {
        $merchant = null;
        $Db = new DbInterface();
        $result = $Db->getTable("SELECT `Id`, `Name`,`Email` FROM `merchant` where `Email`='" . $email . "' and `Password`='" . $password . "'");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $merchant = new Merchant();
                $merchant->Id = $row["Id"];
                $merchant->Name = $row["Name"];
                $merchant->Email = $row["Email"];
            }
        }
        return $merchant;
    }
    public function GetMerchantById($id)
    {
        $merchant = null;
        $Db = new DbInterface();
        $result = $Db->getTable("SELECT m.`Id`, m.`Name`,m.`Email`,mac.UpdatedDate as TokenUpdateDate FROM `merchant` m left join `merchant_auth_code` mac on m.Id = mac.merchantId   where m.`Id`='" . $id . "'");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $merchant = new Merchant();
                $merchant->Id = $row["Id"];
                $merchant->Name = $row["Name"];
                $merchant->Email = $row["Email"];
                $merchant->TokenUpdateDate = $row["TokenUpdateDate"];
            }
        }
        return $merchant;
    }
}

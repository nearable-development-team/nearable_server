<?php

namespace DataAccessLayer;

use Common\Helper;
use Models\File;
use Settings\DbInterface;
use stdClass;

class FileDbService
{


    public function SaveFile($file)
    {
        $sql = "";
        $exitingId = $this->GetFileByUserIdFolderIdName($file);
        if ($exitingId == "0") {
            $sql = "INSERT INTO `file`(`Name`,`Path`,`UserId`,`FolderId`,`UserFolderId`,`UploadedByUserId`) VALUES ('" . $file->Name . "','" . $file->FilePath . "','" . $file->UserId . "','" . $file->FolderId . "','" . $file->UserFolderId . "','" . $file->UploadedByUserId . "');";
        }
        $Db = new DbInterface();
        if (strlen($sql)) {
            $result = $Db->getTable($sql);
            if ($result === true) {
                if ($exitingId == "0")
                    $exitingId = $Db->GetConnectionObj()->insert_id;
            } else {
                return "0";
            }
        }
        return $exitingId;
    }

    public function GetFileByUserIdFolderIdName($file)
    {
        $Id = "0";
        $Db = new DbInterface();
        $result = $Db->getTable("select Id from file where FolderId='" . $file->FolderId  . "' and UserId='" . $file->UserId . "' and Name='" . $file->Name . "'");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $Id = $row["Id"];
            }
        }

        return $Id;
    }


    public function GetFileById($Id)
    {
        $file = new stdClass();
        $Db = new DbInterface();
        $result = $Db->getTable("select * from file where Id='" . $Id  . "'");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $file = new File();
                $file->Id = $row["Id"];
                $file->Name = $row["Name"];
                $file->UserId = $row["UserId"];
                $file->FilePath = $row["Path"];
            }
        }

        return $file;
    }

    public function DeleteFileById($Id)
    {
        $Db = new DbInterface();
        $Db->getTable("delete from file where Id='" . $Id  . "'");
    }

    public function GetFilesByUserIdFolderId($getFileRequest)
    {
        $Id = "0";
        $files = array();

        $Db = new DbInterface();
        $sql = "select file.*, ifnull( user.Name,'Guest') as UserName,ifnull( user.Image,'" . DOMAIN_NAME . "images/user.svg') as UserImage,DATE_FORMAT(file.CreateDate, '%m/%d/%Y %h:%i %p') ImageUploadTimeStamp  from file left join user on file.UploadedByUserId = user.Id  where FolderId='" . $getFileRequest->FolderId  . "' and UserId='" . $getFileRequest->UserId . "'";
        $result = $Db->getTable($sql);
        //echo($sql);

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $b = new File();
                $b->Id = $row["Id"];
                $b->Name = $row["Name"];
                $b->UserName = $row["UserName"];
                $b->FilePath = Helper::GetRelativeImagePath($row["Path"]);
                $b->UserImage = $row["UserImage"];
                $b->ImageUploadTimeStamp = $row["ImageUploadTimeStamp"];
                array_push($files, $b);
            }
        }
        return $files;
    }
}

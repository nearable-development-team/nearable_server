<?php

namespace DataAccessLayer;

use Models\Offer;
use Settings\DbInterface;

class OfferDbService
{
    public function SaveOffer($offer)
    {
        $sql = "INSERT INTO `gift` (`Title`,`ShortDescription`,`Price`,`MerchantId`,`Image`) VALUES ('" . $offer->Title . "','" . $offer->ShortDescription . "','" . $offer->Price . "','" . $offer->MerchantId . "','" . $offer->FilePath . "')";
        $Db = new DbInterface();
        if (strlen($sql)) {
            $result = $Db->getTable($sql);
            if ($result === true) {
                if ($offer->Id == 0) {
                    $offer->Id = $Db->GetConnectionObj()->insert_id;
                }
            }
            return $offer;
        }
    }

    public function GetOfferByMerchant($merchantId)
    {
        $offers = array();
        $Db = new DbInterface();
        $result = $Db->getTable("SELECT `Id`, `Title`,`ShortDescription`,`Price`,`Image` FROM `gift` where `MerchantId`='" . $merchantId . "'");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $offer = new Offer();
                $offer->Id = $row["Id"];
                $offer->Title = $row["Title"];
                $offer->ShortDescription = $row["ShortDescription"];
                $offer->Price = $row["Price"];
                $offer->Image = str_replace('../', '', $row["Image"]);
                array_push($offers, $offer);
            }
        }
        return $offers;
    }
}

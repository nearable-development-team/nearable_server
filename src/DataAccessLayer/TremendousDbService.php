<?php

namespace DataAccessLayer;

use Settings\DbInterface;

class TremendousDbService
{


    public function SaveTremendousCall($tremendous)
    {
        $exitingId = "0";
        $sql = "INSERT INTO `tremendous_calls` (`InputRequest`,`ExternalId`) VALUES ('" . $tremendous->InputRequest . "','" . $tremendous->ExternalId . "')";
        $Db = new DbInterface();
        if (strlen($sql)) {
            $result = $Db->getTable($sql);
            if ($result === true) {
                $exitingId = $Db->GetConnectionObj()->insert_id;
            }
        }
        return $exitingId;
    }

    public function UpdateTremendousCallOutputRequest($tremendous)
    {
        $Db = new DbInterface();
        $Db->getTable("update tremendous_calls set OutputRequest='" . $tremendous->OutputRequest  . "' where Id='" . $tremendous->Id . "'");
    }


    public function SaveTremendousReference()
    {
        $exitingId = "0";
        $sql = "INSERT INTO `tremendous_reference` (`DateCreated`) VALUES (now())";
        $Db = new DbInterface();
        if (strlen($sql)) {
            $result = $Db->getTable($sql);
            if ($result === true) {
                $exitingId = $Db->GetConnectionObj()->insert_id;
            }
        }
        return $exitingId;
    }

    public function SaveTremendousReferenceUserFolderRelation($tremendous)
    {
        $exitingId = "0";
        $sql = "INSERT INTO `tremendous_reference_user_folder_relation` (`TremendousReferenceId`,`UserFolderId`,`DateCreated`) VALUES (" . $tremendous->TremendousReferenceId . "," . $tremendous->UserFolderId . ", now())";
        $Db = new DbInterface();
        if (strlen($sql)) {
            $result = $Db->getTable($sql);
            if ($result === true) {
                $exitingId = $Db->GetConnectionObj()->insert_id;
            }
        }
        return $exitingId;
    }
}

<?php

namespace DataAccessLayer;

use Settings\DbInterface;
use stdClass;

class StripeDbService
{

    private  $Db;
    public function __construct()
    {
        $this->Db = new DbInterface();
    }
    public function CreateOrderForUserFolder($userFolderId)
    {
        $orderId = $this->GetOrderByUserFolderId($userFolderId);
        if ($orderId == "0") {
            $result = $this->Db->getTable("INSERT INTO `order` (`DateCreated`,`UserFolderId`,`Status`) VALUES (now(),'" . $userFolderId . "','pending');");
            if ($result === true) {
                $orderId  = $this->Db->GetConnectionObj()->insert_id;
            }
        }
        return $orderId;
    }

    public function GetOrderByUserFolderId($userFolderId)
    {
        $Id = "0";
        $result = $this->Db->getTable("SELECT * FROM `order` WHERE UserFolderId ='" . $userFolderId . "'");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $Id = $row["Id"];
            }
        }
        return $Id;
    }


    public function SaveStripeAditionalDetails($paymentetail)
    {

        $sql = "INSERT INTO `stripe_card_additional_info` (`CardHolderEmail`,`CardHolderName`,`Password`,`PersonalMessage`,`PlaceId`,`PlaceName`,`RecAddress`,`RecCity`,`RecCountry`,`RecLastName`,`RecPostalCode`,`RecState`,`UserId`) VALUES ('" . $paymentetail->CardHolderEmail . "','" . $paymentetail->CardHolderName . "','" . $paymentetail->Password . "','" . $paymentetail->PersonalMessage . "','" . $paymentetail->PlaceId . "','" . $paymentetail->PlaceName . "','" . $paymentetail->RecAddress . "','" . $paymentetail->RecCity . "','" . $paymentetail->RecCountry . "','" . $paymentetail->RecLastName . "','" . $paymentetail->RecPostalCode . "','" . $paymentetail->RecState . "','" . $paymentetail->UserId . "');";

        if (strlen($sql)) {
            $result = $this->Db->getTable($sql);
            if ($result === true) {
                $paymentetail->StripeAditionalDetailsId = $this->Db->GetConnectionObj()->insert_id;
            }
        }
        return $paymentetail;
    }

    public function UpdateStripeAditionalDetailsStripeId($StripeCardDetailId,  $Id)
    {
        $this->Db->getTable("UPDATE `stripe_card_additional_info` SET `StripeCardDetailId` = '" . $StripeCardDetailId . "' WHERE Id='" . $Id . "';");
    }
    public function UpdateOrderStatusToComplete($orderId)
    {
        $this->Db->getTable("UPDATE `order` SET `Status` = 'complete' WHERE Id= '" . $orderId . "';");
    }


    public function saveStripePaymentTransaction($paymentetail)
    {

        $sql = "";
        $paymentetail->StripePaymentId = $this->CheckPaymentTransactionByOrderId($paymentetail->OrderId);
        if ($paymentetail->StripePaymentId == "0") {
            $sql = "INSERT INTO `stripe_payment`(`OrderId`, `TokenGenerated`, `Email`, `Name`,UserId) VALUES ('" . $paymentetail->OrderId . "','" . $paymentetail->Token . "','" . $paymentetail->CardHolderEmail . "','" . $paymentetail->CardHolderName . "','" . $paymentetail->UserId . "')";
        } else {
            $sql = "UPDATE `stripe_payment` SET `stripeTransactionId`='" . $paymentetail->TransactionId . "', StripePaymentResponse='" . $paymentetail->TransactionResponse . "' WHERE  Id='" . $paymentetail->StripePaymentId . "'";
        }
        if (strlen($sql)) {
            $result = $this->Db->getTable($sql);
            if ($result === true) {
                if ($paymentetail->StripePaymentId == 0) {
                    $paymentetail->StripePaymentId = $this->Db->GetConnectionObj()->insert_id;
                }
            }
            return $paymentetail;
        }
    }


    public function saveStripeCheckoutPaymentTransaction($paymentetail)
    {

        $sql = "";

        if ($paymentetail->StripePaymentTransActionId == "0") {
            $sql = "INSERT INTO `stripe_payment_Transaction`(`OrderId`, `SuccessTransactionId`, `FailureTransactionId`,UserId,Amount,PaymentStatus) VALUES ('" . $paymentetail->OrderId . "','" . $paymentetail->SuccessTransId . "','" . $paymentetail->FailureTransId . "','" . $paymentetail->UserId . "','" . $paymentetail->Amount . "','Pending')";
        } else {
            $sql = "UPDATE `stripe_payment_Transaction` SET `PaymentStatus`='" . $paymentetail->PaymentStatus . "' WHERE  Id='" . $paymentetail->StripePaymentTransActionId . "'";
        }
        if (strlen($sql)) {
            $result = $this->Db->getTable($sql);
            if ($result === true) {
                if ($paymentetail->StripePaymentTransActionId == 0) {
                    $paymentetail->StripePaymentTransActionId = $this->Db->GetConnectionObj()->insert_id;
                }
            }
            return $paymentetail;
        }
    }


    public function GetPaymentTransactionByFailureTransId($orderId)
    {
        $Id = "0";
        $result = $this->Db->getTable("SELECT * FROM `stripe_payment_Transaction` WHERE FailureTransactionId ='" . $orderId . "'");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $Id = $row["Id"];
            }
        }
        return $Id;
    }


    public function GetGiftDetailByUserFolderId($userFolderId)
    {
        $recpDetail = new stdClass();
        $result = $this->Db->getTable("SELECT uf.`RecipientEmail`,uf.`RecipientPhone`,GiftAttachedAmount, uf.`Password`,uf.Id FROM `user_folder` uf inner join `order` o on uf.Id=o.UserFolderId inner join `stripe_payment_Transaction` spt on o.Id=spt.OrderId where uf.Id='" . $userFolderId . "'");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $recpDetail->Email = $row["RecipientEmail"];
                $recpDetail->PhoneNumber = $row["RecipientPhone"];
                $recpDetail->Password = $row["Password"];
                $recpDetail->Amount = $row["GiftAttachedAmount"];
            }
        }
        return $recpDetail;
    }

    public function GetGiftDetailByTransId($transId)
    {
        $recpDetail = new stdClass();
        $result = $this->Db->getTable("SELECT uf.`RecipientEmail`,uf.`RecipientPhone`,GiftAttachedAmount, uf.`Password`,uf.Id,uf.UniqueSharedId,uf.GiftShareType,QRFileLocation FROM `user_folder` uf inner join `order` o on uf.Id=o.UserFolderId inner join `stripe_payment_Transaction` spt on o.Id=spt.OrderId where spt.Id='" . $transId . "'");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $recpDetail->Email = $row["RecipientEmail"];
                $recpDetail->PhoneNumber = $row["RecipientPhone"];
                $recpDetail->Password = $row["Password"];
                $recpDetail->Amount = $row["GiftAttachedAmount"];
                $recpDetail->UniqueSharedId = $row["UniqueSharedId"]; 
                $recpDetail->GiftShareType = $row["GiftShareType"];  
                $recpDetail->QRFileLocation = $row["QRFileLocation"];
            }
        }
        return $recpDetail;
    }


    public function GetPaymentTransactionBySuccessTransId($stid)
    {
        $transactionDetail = new stdClass();
        $transactionDetail->TransactionId = "0";
        $result = $this->Db->getTable("SELECT spt.Id, o.Id as OrderId, o.UserFolderId,spt.PaymentStatus FROM `stripe_payment_Transaction` spt inner join `order` o on o.Id=spt.OrderId  WHERE spt.SuccessTransactionId ='" . $stid . "'");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $transactionDetail->TransactionId = $row["Id"];
                $transactionDetail->OrderId = $row["OrderId"];
                $transactionDetail->UserFolderId = $row["UserFolderId"];
                $transactionDetail->PaymentStatus = $row["PaymentStatus"];
            }
        }
        return $transactionDetail;
    }


    public function CheckPaymentTransactionByOrderId($orderId)
    {
        $Id = "0";
        $result = $this->Db->getTable("SELECT * FROM `stripe_payment` WHERE OrderId ='" . $orderId . "'");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $Id = $row["Id"];
            }
        }
        return $Id;
    }


    public function CreateStripeVirtualCardHolder($paymentetail)
    {

        $sql = "";
        $sql = "INSERT INTO `stripe_virtual_card_place_holder`(`StripeCardPaceHolderId`,`MobileNumber`, `Email`, `Name`, `Type`, `Status`,UserId) VALUES ('" . $paymentetail->StripeCardPaceHolderId . "','','" . $paymentetail->CardHolderEmail . "','" . $paymentetail->CardHolderName . "','" . $paymentetail->PlaceHolderType . "','" . $paymentetail->PlaceHolderStatus . "','" . $paymentetail->UserId . "')";
        if (strlen($sql)) {
            $result = $this->Db->getTable($sql);
            if ($result === true) {
                $paymentetail->StripeVirtualCardHolderId = $this->Db->GetConnectionObj()->insert_id;
            }
            return $paymentetail;
        }
    }

    public function CreateStripeVirtualCard($paymentetail)
    {

        $sql = "";
        $paymentetail->CardDetailId = $this->CheckCardDetailByStripeCardId($paymentetail->StripeCardId);
        if ($paymentetail->CardDetailId == "0") {
            $sql = "INSERT INTO `stripe_card_detail`(`StripeCardId`,`StripeStatus`,`Brand`,`CardType`,UserId) VALUES ('" . $paymentetail->StripeCardId . "','" . $paymentetail->CardStatus . "','" . $paymentetail->Brand . "','" . $paymentetail->CardType . "','" . $paymentetail->UserId . "')";
        } else {
            $sql = "UPDATE `stripe_card_detail` SET `StripeStatus`='" . $paymentetail->CardType . "' WHERE  Id='" . $paymentetail->CardDetailId . "'";
        }
        if (strlen($sql)) {
            $result = $this->Db->getTable($sql);
            if ($result === true) {
                if ($paymentetail->CardDetailId == 0) {
                    $paymentetail->CardDetailId = $this->Db->GetConnectionObj()->insert_id;
                }
            }
            return $paymentetail;
        }
    }


    public function CheckCardDetailByStripeCardId($StripeCardId)
    {

        $Id = "0";
        $result = $this->Db->getTable("SELECT * FROM `stripe_card_detail` WHERE StripeCardId='" . $StripeCardId . "'");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $Id = $row["Id"];
            }
        }
        return $Id;
    }
}

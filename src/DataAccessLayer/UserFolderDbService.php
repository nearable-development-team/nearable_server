<?php

namespace DataAccessLayer;

use Models\Folder;
use Settings\DbInterface;
use stdClass;

class UserFolderDbService
{
    public function GetFoldersByUserId($userId)
    {
        $Db = new DbInterface();
        $folders = array();
        $result = $Db->getTable("SELECT folder.`Id`, folder.`Name`,user_folder.`UniqueSharedId`,
    ifnull( contp.totalImg,0) as TotalFiles,
   ifnull( user.Name,'') as AuthorName,user_folder.ModifiedDate,
    CASE
    WHEN user_folder.SharedBy =0 THEN user_folder.UserId
    ELSE user_folder.SharedBy
    END as AuthorId ,ifnull( contp.topImages,'') TopImages,
    user_folder.`IsArchived`,
    user_folder.`Id` user_folder_Id,
    user_folder.GiftShareType as GiftShareType
FROM user_folder
inner join `folder` on user_folder.FolderId=folder.Id 
left join user on user_folder.SharedBy=user.Id
left join (SELECT UserId,folderId, COUNT(folderId) 
as totalImg,substring_index(group_concat(Path SEPARATOR '$$'), '$$', 3) as topImages 
from file group by userId,folderId ) as contp  
on contp.folderId=user_folder.folderId and (user_folder.UserId=contp.UserId or user_folder.SharedBy=contp.UserId)
where user_folder.UserId='" . $userId . "'");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $b = new Folder();
                $b->Id = $row["Id"];
                $b->Name = $row["Name"];
                $b->SharedFolderId = $row["UniqueSharedId"];
                $b->TotalFiles = $row["TotalFiles"];
                $b->AuthorName = $row["AuthorName"];
                $b->ModifiedDate = $row["ModifiedDate"];
                $b->AuthorId = $row["AuthorId"];
                $b->TopImages = $row["TopImages"];
                $b->IsArchived = $row["IsArchived"];
                $b->UserFolderId = $row["user_folder_Id"];
                $b->GiftShareType = $row["GiftShareType"];
                array_push($folders, $b);
            }
        }
        return $folders;
    }

    public function GetFoldersByLocation($longitude, $latitude)
    {
        $Db = new DbInterface();
        $folders = array();
        $result = $Db->getTable("SELECT folder.`Id`, folder.`Name`,user_folder.`UniqueSharedId`,user_folder.`Longitude`,user_folder.`Latitude`,user_folder.FenceAreaInFeet,user_folder.FolderIcon,
     ((ACOS(SIN('" . $latitude . "' * PI() / 180) * SIN(Latitude * PI() / 180) + COS('" . $latitude . "' * PI() / 180) * 
    COS(Latitude * PI() / 180) * COS(('" . $longitude . "' - Longitude) * PI() / 180)) * 180 / PI()) * 47.48646) as DistanceInFeet,
    IFNull(`IsPrivateFence`,0) IsPrivateFence,
    user_folder.UserId
    FROM user_folder  
    inner join `folder` on user_folder.FolderId=folder.Id 
    where Latitude IS NOT NULL and Longitude IS NOT NULL and SharedBy=0
    group by user_folder.Id,folder.`Id`, folder.`Name`,user_folder.`UniqueSharedId`,user_folder.`Longitude`,user_folder.`Latitude`,DistanceInFeet,FolderIcon,user_folder.IsPrivateFence,user_folder.UserId
    having (DistanceInFeet<=user_folder.FenceAreaInFeet or user_folder.FenceAreaInFeet=0)");

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $b = new Folder();
                $b->Id = $row["Id"];
                $b->Name = $row["Name"];
                $b->SharedFolderId = $row["UniqueSharedId"];
                $b->Longitude = $row["Longitude"];
                $b->Latitude = $row["Latitude"];
                $b->DistanceInFeet = $row["DistanceInFeet"];
                $b->FolderIcon = $row["FolderIcon"];
                $b->IsPrivateFence = $row["IsPrivateFence"];
                $b->AuthorId = $row["UserId"];
                array_push($folders, $b);
            }
        }
        return $folders;
    }

    public function SaveUserFolderRelation($usrfldr)
    {
        $Id = "0";
        $sql = "";
        if ($usrfldr->userId != 0 && $usrfldr->folderId != 0) {
            $sql = "INSERT INTO `user_folder` (`UserId`,`FolderId`, `UniqueSharedId`,`SharedBy`,`Longitude`,`Latitude`,`GeoLocation`,`FenceAreaInFeet`,`FolderIcon`,`IsPrivateFence`,`Password`,`RecipientEmail`,`RecipientPhone`,`GiftAttachedType`,`GiftAttachedAmount`,`GiftShareType`,`PattFileLocation`,`QRFileLocation`)
         VALUES ('" . $usrfldr->userId . "','" . $usrfldr->folderId . "','" . uniqid() . "'," . $usrfldr->sharedBy . ",'" . $usrfldr->longitude . "','" . $usrfldr->latitude . "',POINT('" . $usrfldr->longitude . "','" . $usrfldr->latitude . "'),'" . $usrfldr->fenceAreaInFeet . "','" . $usrfldr->folderIcon . "','" . $usrfldr->IsPrivateFence . "','" . $usrfldr->password . "','" . $usrfldr->RecepientEmail . "','" . $usrfldr->RecipientPhone . "','" . $usrfldr->GiftAttachedType . "','" . $usrfldr->GiftAttachedAmount . "','" . $usrfldr->GiftShareType . "','" . $usrfldr->PattFileLocation . "','" . $usrfldr->QRFileLocation. "')";
        }
        // echo $sql;
        $Db = new DbInterface();
        if (strlen($sql)) {
            $result = $Db->getTable($sql);
            if ($result === true) {
                $Id = $Db->GetConnectionObj()->insert_id;
            }
        }
        return $Id;
    }

    public function GetRelationByFolderIdAndUserID($userId, $folderId)
    {
        $Id = "0";
        $Db = new DbInterface();
        $result = $Db->getTable("select Id from user_folder where UserId='" . $userId . "' and FolderId='" . $folderId . "'");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $Id = $row["Id"];
            }
        }

        return $Id;
    }

    public function GetFolderByID($Id)
    {

        $folder = new Folder();
        $Db = new DbInterface();
        $result = $Db->getTable("select folder.`Id`, folder.`Name`,user_folder.UserId,user_folder.FolderIcon,user_folder.`UniqueSharedId`,user_folder.Id as user_folder_Id,user_folder.IsPrivateFence,user_folder.GiftAttachedAmount ,user_folder.GiftAttachedType from user_folder inner join folder on folder.Id=user_folder.FolderId where user_folder.Id='" . $Id . "'");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $folder->FolderId = $row["Id"];
                $folder->Name = $row["Name"];
                $folder->UserId = $row["UserId"];
                $folder->FolderIcon = $row["FolderIcon"];
                $folder->UserFolderId = $row["user_folder_Id"];
                $folder->SharedFolderId = $row["UniqueSharedId"];
                $folder->IsPrivateFence = $row["IsPrivateFence"];
                $folder->AmountAttached = $row["GiftAttachedAmount"];
                $folder->AmountType = $row["GiftAttachedType"];
            }
        }

        return $folder;
    }
    public function GetQRFolders()
    {
        $folders = array();
        $Db = new DbInterface();
        $result = $Db->getTable("select user_folder.id as UserFolderId, user_folder.GiftAttachedType,user_folder.PattFileLocation,user_folder.QRFileLocation,user_folder.FolderIcon from user_folder  where  ifNULL (user_folder.isRedeemed,0)!=1  and user_folder.GiftShareType='QR'");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $folder = new stdClass();
                $folder->PattFileLocation = $row["PattFileLocation"];
                $folder->GiftAttachedType = $row["GiftAttachedType"];
                $folder->QRFileLocation = $row["QRFileLocation"];
                $folder->FolderIcon = $row["FolderIcon"];
                $folder->UserFolderId = $row["UserFolderId"];
                array_push($folders, $folder);
            }
        }

        return $folders;
    }
  
    public function GetRedeemAblePointsAndUser($minPoinCollected = 0)
    {

        $folders = array();
        $Db = new DbInterface();
        $result = $Db->getTable("select userid,u.email,u.name,SUM(`GiftAttachedAmount`) as totalCollectedPoint,GROUP_CONCAT(uf.Id) as userFolderIdList, u.Image,u.IsEmailConfirmed from user_folder uf 
        inner join user u on u.Id=uf.userid 
        where `GiftAttachedType`='point' and `SharedBy` >0 and ifNULL (isRedeemed,0)=0  and u.role='user'
        GROUP by userid
        having SUM(`GiftAttachedAmount`)>" . $minPoinCollected . " order by totalCollectedPoint DESC");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $folder = new Folder();
                $folder->UserId = $row["UserId"];
                $folder->Email = $row["email"];
                $folder->UserName = $row["name"];
                $folder->Points = $row["totalCollectedPoint"];
                $folder->userFolderIdList = $row["userfolderIdList"];
                $folder->Image = $row["Image"];
                $folder->IsEmailConfirmed = $row["IsEmailConfirmed"];
                array_push($folders, $folder);
            }
        }

        return $folders;
    }


    public function GetPointsAndUser($minPoinCollected = 0)
    {

        $folders = array();
        $Db = new DbInterface();
        $result = $Db->getTable("select u.Id userid,u.email,u.name,ifnull(SUM(`GiftAttachedAmount`),0) as totalCollectedPoint,GROUP_CONCAT(uf.Id) as userFolderIdList, u.Image from user u
        LEFT join  user_folder uf  on u.Id=uf.userid and `GiftAttachedType`='point'  and `SharedBy` >0  
        where u.role='user'
        GROUP by uf.userid,u.email,u.name,u.Image
        having totalCollectedPoint>" . $minPoinCollected . " order by totalCollectedPoint DESC");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $folder = new Folder();
                $folder->UserId = $row["UserId"];
                $folder->Email = $row["email"];
                $folder->UserName = $row["name"];
                $folder->Points = $row["totalCollectedPoint"];
                $folder->userFolderIdList = $row["userfolderIdList"];
                $folder->Image = $row["Image"];
                array_push($folders, $folder);
            }
        }

        return $folders;
    }


    public function updateRedeemedFolders($userfolderIdList)
    {
        $Db = new DbInterface();
        $result = $Db->getTable("update user_folder set isRedeemed=1 where Id in (" . $userfolderIdList . ")");
    }

    public function updateIsGiftClaimedFolder($userfolderId)
    {
        $Db = new DbInterface();
        $result = $Db->getTable("update user_folder set IsGiftClaimed=1 where Id =" . $userfolderId);
    }

    public function GetFolderByUniqueId($UniqueSharedId)
    {
        $folder = new Folder();
        $Db = new DbInterface();
        $sql = "select folder.`Id`, folder.`Name`,user_folder.UserId,user_folder.FolderIcon,user_folder.Id userFolderId,user_folder.GiftAttachedType,user_folder.GiftAttachedAmount,user_folder.IsGiftClaimed, user_folder.GiftShareType,user_folder.QRFileLocation from user_folder inner join folder on folder.Id=user_folder.FolderId where user_folder.UniqueSharedId='" . $UniqueSharedId . "'";
        $result = $Db->getTable($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $folder->Id = $row["Id"];
                $folder->Name = $row["Name"];
                $folder->UserId = $row["UserId"];
                $folder->FolderIcon = $row["FolderIcon"];
                $folder->UserFolderId = $row["userFolderId"];
                $folder->SharedFolderId = $UniqueSharedId;
                $folder->AmountAttached = $row["GiftAttachedAmount"];
                $folder->AmountType = $row["GiftAttachedType"];
                $folder->IsGiftClaimed = $row["IsGiftClaimed"];
                $folder->GiftShareType = $row["GiftShareType"];
                $folder->QRFileLocation = $row["QRFileLocation"];
                return $folder;
            }
        }

        return $folder;
    }

    public function AuthenticateFolder($userfolderId, $phone, $token)
    {
        $folder = new Folder();
        $Db = new DbInterface();
        $sql = "select folder.`Id`, folder.`Name`,user_folder.UserId,user_folder.FolderIcon,user_folder.Id userFolderId from user_folder inner join folder on folder.Id=user_folder.FolderId where user_folder.Id='" . $userfolderId . "' and user_folder.Password='" . $token . "' and user_folder.RecipientPhone='" . $phone . "'";
        $result = $Db->getTable($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $folder->Id = $row["Id"];
                $folder->Name = $row["Name"];
                return $folder;
            }
        }

        return $folder;
    }

    public function ArchiveFolder($folder)
    {
        $Db = new DbInterface();
        $result = $Db->getTable("update user_folder set IsArchived=1 where Id='" . $folder->Id . "'");
    }

    public function UpdateGiftAmount($folder)
    {
        $Db = new DbInterface();
        $result = $Db->getTable("update user_folder set GiftAttachedAmount='" . $folder->Amount . "' where Id='" . $folder->UserFolderId . "'");
    }
}

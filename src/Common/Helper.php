<?php

namespace Common;

class Helper
{

    static  function StartsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return substr($haystack, 0, $length) === $needle;
    }

    static function EndsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if (
            !$length
        ) {
            return true;
        }
        return substr($haystack, -$length) === $needle;
    }

    static function GetRelativeImagePath($filePath)
    {
        return str_replace(USER_DATA_DIRECTORY, USER_DATA_DIRECTORY_NAME, $filePath);
    }

    static function showAlert($alert)
    {

        $returnData = '';
        if ($alert->type == 'success') {
            $returnData = '<div class="w3-container w3-section w3-green w3-card-2">';
            $returnData .= '<p>' . $alert->message . '</p>';
            $returnData .= '</div>';
        } elseif ($alert->type == 'danger') {
            $returnData = '<div class="w3-container w3-section w3-green w3-card-2">';
            $returnData .= '<p>' . $alert->message . '</p>';
            $returnData .= '</div>';
        }

        return $returnData;
    }

    static  function IncludeServiceCharges($amount)
    {
        if ($amount == 10)
            return $amount + 0.99;
        if ($amount == 15.00)
            return $amount + 1.50;
        if ($amount == 25.00)
            return $amount + 1.75;
        if ($amount == 50.00)
            return $amount + 2.99;
        if ($amount == 75.00)
            return $amount + 3.00;
        if ($amount == 100.00)
            return $amount + 3.50;
        if ($amount == 125.00)
            return $amount + 4.35;
        if ($amount == 150.00)
            return $amount + 5.00;

        return $amount + 10.00;
    }

    static function GeneratePasword()
    {
        //Generate a random string.
        $token = openssl_random_pseudo_bytes(16);

        //Convert the binary data into hexadecimal representation.
        $token = bin2hex($token);

        //Print it out for example purposes.
        return $token;
    }
}

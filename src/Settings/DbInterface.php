<?php

namespace Settings;

use mysqli;

class DbInterface
{
    private $conn;
    function __construct()
    {
        $servername = DB_SERVER_NAME;
        $username = DB_USERNAME;
        $password = DB_PASSWORD;
        $database = DATABASE_NAME;


        $this->conn = new mysqli($servername, $username, $password, $database);

        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
    }

    function GetConnectionObj()
    {
        return $this->conn;
    }

    function getTable($sql)
    {
        $result = $this->conn->query($sql);
        return $result;
    }

    function ExecuteMultipleQuery($sql)
    {
        $result = mysqli_multi_query($this->conn, $sql);
        return $result;
    }
}

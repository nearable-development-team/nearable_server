<?php

namespace Enums;

class TremendousCallTypeConstant
{
    public const SMS = "SMS";
    public const Email = "Email";
}

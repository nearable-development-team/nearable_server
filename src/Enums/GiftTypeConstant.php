<?php

namespace Enums;

class GiftTypeConstant
{
    public const Amount = "Amount";
    public const Point = "Point";
}
<?php

namespace ViewModels;

class FileUploadRequest
{
    public $Name;
    public $Base64;
    public $FolderName;
    public $UserId;
}

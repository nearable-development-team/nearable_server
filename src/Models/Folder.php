<?php

namespace Models;

class Folder
{
    public $Id;
    public $Name;
    public $SharedFolderId;
    public $AuthorName;
    public $TotalFiles;
    public $ModifiedDate;
    public $AuthorId;
    public $TopImages;
}

<?php

namespace Models;

class MerchantToken
{
    public $Id;
    public $AccessToken;
    public $RefreshToken;
    public $SquareMerchantId;
}

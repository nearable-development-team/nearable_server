<?php

namespace Models\Stripe;

class StripeCardPlaceholder
{
    public $name;
    public $email;
    public $phone_number;
    public $status;
    public $type;
    public $billing;
}

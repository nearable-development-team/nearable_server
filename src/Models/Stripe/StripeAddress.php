<?php

namespace Models\Stripe;

class StripePlaceHolderAddress
{
    public $line1;
    public $city;
    public $phone_number;
    public $state;
    public $postal_code;
    public $country;
}

<?php

namespace Models;

class User
{
    public $Id;
    public $Name;
    public $Email;
    public $IsEmailConfirmed; 
    public $UserRole;
}

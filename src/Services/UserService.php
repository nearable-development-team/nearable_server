<?php

namespace Services;

use DataAccessLayer\UserDbService;

class UserService
{

    private $userDbService;
    private $sessionService;
    public function __construct()
    {
        $this->userDbService = new UserDbService();
        $this->sessionService = new SessionService();
    }
    public function SaveUser($user)
    {
        return $this->userDbService->SaveUser($user);
    }


    public function SaveLoginDetail($user)
    {
        return $this->userDbService->SaveLoginDetail($user);
    }

    public function CheckUserBlock($user)
    {
        return $this->userDbService->CheckUserBlock($user->Email);
    }

    public function GetUserLog()
    {
        return $this->userDbService->GetUserLog();
    }

    public function GetUsers($user = NULL)
    {
        return $this->userDbService->GetUsers($user);
    }

    public function GetPaymentTransaction()
    {
        return $this->userDbService->GetPaymentTransaction();
    }

    public function BlockUnblockUsers($user)
    {
        return $this->userDbService->BlockUnblockUsers($user);
    }

    public function ChecAdminUser($user)
    {
        return $this->userDbService->ChecAdminUser($user);
    }

    public function CreateAdminUser($user)
    {
        return $this->userDbService->CreateAdminUser($user);
    }

    public function CreatePaymentTransaction($paymentDetail)
    {
        return $this->userDbService->CreatePaymentTransaction($paymentDetail);
    }

    public function GetActiveAdminUser()
    {
        return $this->userDbService->GetActiveAdminUser();
    }

    public function ConfirmUserEmail()
    {
        $user = $this->sessionService->GetUser();
        if ($user != null)
            return $this->userDbService->ConfirmUserEmail($user->Id);
    }
}

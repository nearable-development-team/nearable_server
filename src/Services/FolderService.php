<?php

namespace Services;

use Common\Helper;
use DataAccessLayer\FolderDbService;
use DataAccessLayer\StripeDbService;
use DataAccessLayer\UserFolderDbService;
use Enums\GiftTypeConstant;
use Enums\TremendousCallTypeConstant;
use Models\Folder;
use stdClass;

class FolderService
{
    private $stripeService;
    private $stripeDbService;
    private $curlTremendousService;
    private $userFolderDbService;
    private $firebaseDbService;
    private $smsService;
    private $userService;
    private $sessionService;
    private $fileService;
    private $fileSystemService;
    private $folderDbService;
    private $qrCodeService;
    public function __construct()
    {
        $this->stripeService = new StripeService();
        $this->stripeDbService = new StripeDbService();
        $this->curlTremendousService = new CurlTremendousService();
        $this->userFolderDbService = new UserFolderDbService();
        $this->firebaseDbService = new FirebaseDbService();
        $this->emailService = new EmailService();
        $this->smsService = new SMSService();
        $this->userService = new UserService();
        $this->sessionService = new SessionService();
        $this->fileService = new FileService();
        $this->fileSystemService = new FileSystemService();
        $this->folderDbService = new FolderDbService();
        $this->qrCodeService = new QRCodeService();
    }
    public function GetFolders()
    {
        return $this->folderDbService->GetFolders();
    }

    public function ArchiveFolder($folder)
    {
        return $this->userFolderDbService->ArchiveFolder($folder);
    }

    public function GetFoldersByUser($user)
    {
        $folders = $this->userFolderDbService->GetFoldersByUserId($user->UserId);

        for ($i = 0; $i < count($folders); $i++) {
            $folders[$i]->TopImages =  explode('$$', $folders[$i]->TopImages, 3);
            for ($j = 0; $j < count($folders[$i]->TopImages); $j++) {
                $folders[$i]->TopImages[$j] = Helper::GetRelativeImagePath($folders[$i]->TopImages[$j]);
            }
        }
        $result = new stdClass();
        $result->Folders = $folders;
        $result->BasePath = DOMAIN_NAME;
        return $result;
    }

    public function GetFoldersByLocation($user)
    {
        $folders = $this->userFolderDbService->GetFoldersByLocation($user->Longitude, $user->Latitude);

        $result = new stdClass();
        $result->Folders = $folders;
        $result->BasePath = DOMAIN_NAME;
        return $result;
    }
    public function GetQRFolders()
    {
        return $this->userFolderDbService->GetQRFolders();
    }
    public function AddFolder($request)
    {
        return $this->folderDbService->SaveFolder($request->Name);
    }
    public function SearchFolderBySharedId($sharedFolderId)
    {
        return $this->userFolderDbService->GetFolderByUniqueId($sharedFolderId);
    }

    public function AddFolderInBulkByUser($decoded_params)
    {
        $adminUser = $this->userService->GetActiveAdminUser();
        $mrks = array();
        if ($adminUser->Id > 0) {
            for ($i = 0; $i < count($decoded_params); $i++) {
                $decoded_params[$i]->UserId = $adminUser->Id;
                $rusl = $this->AddFolderByUser($decoded_params[$i]);
                $mr = new stdClass();
                $mr->latitude = $decoded_params[$i]->Latitude;
                $mr->longitude = $decoded_params[$i]->Longitude;
                $mr->detail = new stdClass();
                $mr->detail->folderId = $rusl->UserFolderId;
                $mr->detail->folderIcon = $decoded_params[$i]->FolderIcon;
                array_push($mrks, $mr);
            }
        }
        return $mrks;
    }

    public function AddFolderByUser($request)
    {
        $folderRequest = new Folder();
        $folderRequest->Name = $request->FolderName;
        $folderId = $this->AddFolder($folderRequest);

        $this->fileSystemService->CreateDirectory($request->UserId, $request->FolderName);

        $usrfldr = new stdClass();

        $usrfldr->userId = $request->UserId;
        $usrfldr->folderId = $folderId;
        $usrfldr->sharedBy = 0;
        $usrfldr->latitude = $request->Latitude;
        $usrfldr->longitude = $request->Longitude;
        $usrfldr->fenceAreaInFeet = $request->FenceAreaInFeet;
        $usrfldr->folderIcon = $request->FolderIcon;
        $usrfldr->IsPrivateFence = $request->IsPrivateFence;
        $usrfldr->password = Helper::GeneratePasword();
        $usrfldr->RecepientEmail = "";
        $usrfldr->RecipientPhone = $request->PhoneNumber;
        $usrfldr->GiftAttachedType = $request->GiftAttachedType;
        $usrfldr->GiftAttachedAmount = $request->WorthValue;
        $usrfldr->GiftShareType = $request->GiftShareType;
        $usrfldr->PattFileLocation = "";
        $usrfldr->QRFileLocation = "";
        if ($request->GiftShareType == "QR") {
            $usrfldr->PattFileLocation = $this->fileService->SavePattFile($request->PattCode);
            $usrfldr->QRFileLocation = $this->fileService->SaveBase64QRImage($request->QRBaseImage);
        }

        $userFolderId = $this->userFolderDbService->SaveUserFolderRelation($usrfldr);
        return $this->userFolderDbService->GetFolderByID($userFolderId);
    }

    public function JoinByUnquieFolderId($folder)
    {
        $folderinfo = $this->userFolderDbService->GetFolderByUniqueId($folder->SharedFolderId);

        if($folderinfo->UserId== $folder->UserId){
            return -1;
        }
        if ($folderinfo->IsGiftClaimed == "0") {
            $ownerUserId = $folderinfo->UserId;
            $folderId = $folderinfo->Id;
            $userId = $folder->UserId;
            $FolderIcon = $folderinfo->FolderIcon;

            $usrfldr = new stdClass();
            $usrfldr->userId = $userId;
            $usrfldr->folderId = $folderId;
            $usrfldr->sharedBy = $ownerUserId;
            $usrfldr->latitude = null;
            $usrfldr->longitude = null;
            $usrfldr->fenceAreaInFeet = 0;
            $usrfldr->folderIcon = $FolderIcon;
            $usrfldr->IsPrivateFence = null;
            $usrfldr->password = null;
            $usrfldr->RecepientEmail = null;
            $usrfldr->RecipientPhone = null;
            $usrfldr->GiftAttachedType = $folderinfo->AmountType;
            $usrfldr->GiftAttachedAmount =  $folderinfo->AmountAttached;
            $usrfldr->GiftShareType =  $folderinfo->GiftShareType;
            $rs = $this->userFolderDbService->SaveUserFolderRelation($usrfldr);
            if ($usrfldr->GiftShareType != "QR")
                $this->firebaseDbService->DeleteFirebaseDbMarker($folderinfo->UserFolderId);
                
            $this->userFolderDbService->updateIsGiftClaimedFolder($folderinfo->UserFolderId);
            if ($folderinfo->AmountType == GiftTypeConstant::Amount) {
                $this->SendGiftToUserByUserFolderId($folderinfo->UserFolderId);
            }
            return $rs;
        } else {
            return 0;
        }
    }


    public function AuthenticateFolder($folder)
    {
        $folderinfo = $this->userFolderDbService->AuthenticateFolder($folder->UserfolderId, $folder->PhoneNumber, $folder->Token);
        return $folderinfo;
    }

    public function PreparePaymentTransaction($customerDetailsAry)
    {
        $customerDetailsAry->SuccessTransId = bin2hex(random_bytes(5));
        $customerDetailsAry->FailureTransId = bin2hex(random_bytes(5));
        $customerDetailsAry->OrderId = $this->stripeDbService->CreateOrderForUserFolder($customerDetailsAry->UserFolderId);
        $customerDetailsAry->UserId = $this->sessionService->GetUser()->Id;
        $customerDetailsAry->StripePaymentTransActionId = "0";

        $usfolder = new stdClass();
        $usfolder->Amount = $customerDetailsAry->Amount;
        $usfolder->UserFolderId = $customerDetailsAry->UserFolderId;
        $this->userFolderDbService->UpdateGiftAmount($usfolder);
        // add fee
        $customerDetailsAry->Amount = Helper::IncludeServiceCharges($customerDetailsAry->Amount);
        $this->stripeDbService->saveStripeCheckoutPaymentTransaction($customerDetailsAry);
        return $this->stripeService->CreateCheckoutSession($customerDetailsAry);
    }

    public function GetFolderById($details)
    {
        return  $this->userFolderDbService->GetFolderByID($details->userFolderId);
    }

    public function UpdatePaymentTransactionStatusByFailure($trns)
    {
        $Id =  $this->stripeDbService->GetPaymentTransactionByFailureTransId($trns->FailureTransId);
        $trns->StripePaymentTransActionId = $Id;
        $trns->PaymentStatus = 'Failed';
        $this->stripeDbService->saveStripeCheckoutPaymentTransaction($trns);
    }

    public function UpdatePaymentTransactionStatusBySuccess($trns)
    {
        $response = new stdClass();
        $transactionDetail =  $this->stripeDbService->GetPaymentTransactionBySuccessTransId($trns->SuccessTransId);
        $detail = $this->stripeDbService->GetGiftDetailByTransId($transactionDetail->TransactionId);
        $response->UniqueSharedId = $detail->UniqueSharedId;
        $response->GiftShareType = $detail->GiftShareType;
        $response->QRFileLocation = $detail->QRFileLocation;
        if ($transactionDetail->PaymentStatus != "Pending") {
            $response->Status = 'alreadyUpdated';
            return $response;
        }
        $trns->PaymentStatus = 'Success';
        $trns->StripePaymentTransActionId = $transactionDetail->TransactionId;
        $this->stripeDbService->saveStripeCheckoutPaymentTransaction($trns);
        $this->stripeDbService->UpdateOrderStatusToComplete($transactionDetail->OrderId);

        if ($response->GiftShareType != "QR") {
            $this->smsService->SendSMS($detail->PhoneNumber, "Token for your geogift is " . $detail->Password);
        }
        $response->Status = "success";
        return $response;
    }
    public function SendGiftToUserByUserFolderId($userFolderId)
    {
        $detail = $this->stripeDbService->GetGiftDetailByUserFolderId($userFolderId);

        $trsOption = new stdClass();
        $trsOption->Amount = $detail->Amount;
        $trsOption->RecipientPhone = $detail->PhoneNumber;
        $trsOption->ReferenceNumberId = $this->curlTremendousService->PrepareTremendousCallWithSingleFolderId($userFolderId);

        $this->curlTremendousService->CreateTremendusReward($trsOption, TremendousCallTypeConstant::SMS);

        // $myfile = fopen(ROOT_APPLICATION . "/emailTemplates/giftToken.html", "r") or die("Unable to open file!");
        // $emailcontent = fread($myfile, filesize(ROOT_APPLICATION . "/emailTemplates/giftToken.html"));
        // $emailcontent = str_replace("{{ Email }}", $detail->Email, $emailcontent);
        // $emailcontent = str_replace("{{ Token }}", $detail->Password, $emailcontent);
        // $this->emailService->SendEmail($detail->Email, "Claim your gift #GeoGifts.app", $emailcontent);
    }

    public function GetQRByUniqueShareId($uniqueSharedId)
    {
        $url = DOMAIN_NAME . "qr-gift-redeem.php?usid=" . $uniqueSharedId;
        return  $this->qrCodeService->CreateQRDataURI($url);
    }
    
}

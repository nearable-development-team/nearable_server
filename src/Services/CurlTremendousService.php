<?php

namespace Services;

use DataAccessLayer\TremendousDbService;
use Enums\TremendousCallTypeConstant;
use stdClass;

class CurlTremendousService
{

  private $tremendousDbService;
  public function __construct()
  {
    $this->tremendousDbService = new TremendousDbService();
  }

  public function CreateTremendusReward($options, $type)
  {
    $rewardCallsInput = new stdClass();
    $rewardCallsInput->Amount = $options->Amount;
    $rewardCallsInput->ExternalId = 'GG' . $options->ReferenceNumberId;

    if ($type == TremendousCallTypeConstant::Email) {
      $rewardCallsInput->RecipientPhone = $options->RecipientEmail;
      $rewardCallsInput->InputRequest = $this->GetTremendusEmailRewardCall($options);
    }
    if ($type == TremendousCallTypeConstant::SMS) {
      $rewardCallsInput->RecipientPhone = $options->RecipientPhone;
      $rewardCallsInput->InputRequest = $this->GetTremendusSMSRewardCall($options);
    }

    $this->SendTremendusRewardCall($rewardCallsInput);
  }


  private function GetTremendusEmailRewardCall($options)
  {
    $json = '{
  "external_id": "' . $options->ExternalId . '",
  "payment": {
    "funding_source_id": "' . TREMENDOUS_FUNDING_SOURCE_ID . '"
  },
  "reward": {
    "value": {
      "denomination": ' . $options->Amount . ',
      "currency_code": "USD"
    },
    "campaign_id": "' . TREMENDOUS_CAMPAIGN_ID . '",
    "recipient": {
         "email": "' . $options->RecipientEmail . '",
         "name":"Geogift_' . $options->ExternalId . '"
    },
    "delivery": {
      "method": "EMAIL",
      "meta": {}
    }
  }
}';

    return $json;
  }

  private function GetTremendusSMSRewardCall($options)
  {
    $json = '{
  "external_id": "' . $options->ExternalId . '",
  "payment": {
    "funding_source_id": "' . TREMENDOUS_FUNDING_SOURCE_ID . '"
  },
  "reward": {
    "value": {
      "denomination": ' . $options->Amount . ',
      "currency_code": "USD"
    },
    "campaign_id": "' . TREMENDOUS_CAMPAIGN_ID . '",
    "recipient": {
         "phone": "' . $options->RecipientPhone . '",
         "name":"Geogift_' . $options->ExternalId . '"
    },
    "delivery": {
      "method": "PHONE",
      "meta": {}
    }
  }
}';
    return $json;
  }


  private function SendTremendusRewardCall($options)
  {

    $curl = curl_init();
    $inp = new stdClass();
    $inp->ExternalId = $options->ExternalId;
    $inp->InputRequest = $options->InputRequest;
    $inp->Id = $this->tremendousDbService->SaveTremendousCall($inp);

    curl_setopt_array($curl, array(
      CURLOPT_URL => TREMENDOUS_API_URL . '/api/v2/orders',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => $inp->InputRequest,
      CURLOPT_HTTPHEADER => array(
        'Authorization: Bearer ' . TREMENDOUS_API_TOKEN,
        'Content-Type: application/json'
      ),
    ));
    $inp->OutputRequest = curl_exec($curl);
    curl_close($curl);

    $this->tremendousDbService->UpdateTremendousCallOutputRequest($inp);
  }


  public function PrepareTremendousCallWithSingleFolderId($folderId)
  {
    $referenceNumberId = $this->tremendousDbService->SaveTremendousReference();
    $referencUserFolderRelation = new stdClass();
    $referencUserFolderRelation->TremendousReferenceId = $referenceNumberId;
    $referencUserFolderRelation->UserFolderId = $folderId;
    $this->tremendousDbService->SaveTremendousReferenceUserFolderRelation($referencUserFolderRelation);
    return $referenceNumberId;
  }

  public function PrepareTremendousCallWithMultipleFolderId($folderIds)
  {
    $referenceNumberId = $this->tremendousDbService->SaveTremendousReference();

    foreach ($folderIds as $key => $folderId) {
      $referencUserFolderRelation = new stdClass();
      $referencUserFolderRelation->TremendousReferenceId = $referenceNumberId;
      $referencUserFolderRelation->UserFolderId = $folderId;
      $this->tremendousDbService->SaveTremendousReferenceUserFolderRelation($referencUserFolderRelation);
    }

    return $referenceNumberId;
  }
}

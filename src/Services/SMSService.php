<?php

namespace Services;

use Twilio\Rest\Client;

class SMSService
{
    public function  SendSMS($to, $content)
    {
        $twilio = new Client(TWILLIO_ACCOUNT_SID, TWILLIO_TOKEN);

        $message = $twilio->messages
            ->create(
                $to, // to 
                array(
                    "messagingServiceSid" => TWILLIO_MESSAGE_SERVICE_ID,
                    "body" => $content
                )
            );
    }
}

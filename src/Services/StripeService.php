<?php

namespace Services;

use DataAccessLayer\StripeDbService;
use stdClass;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Issuing\Card;
use Stripe\Issuing\Cardholder;
use Stripe\Stripe;

class StripeService
{
    private $stripeService;
    private $stripeDbService;

    function __construct()
    {
        $this->stripeService = new Stripe();
        $this->stripeService->setVerifySslCerts(false);
        $this->stripeService->setApiKey(STRIPE_SECRET_KEY);
        $this->stripeDbService = new StripeDbService();
    }

    public function CreateCardPlaceHolder($placeholder)
    {
        return Cardholder::Create([
            'name' => $placeholder->CardHolderName,
            'email' => $placeholder->CardHolderEmail,
            //'phone_number' => '+18008675309',
            'status' => 'active',
            'type' => 'individual',
            'billing' => [
                'address' => [
                    'line1' => '1234 Main Street',
                    'city' => 'San Francisco',
                    'state' => 'CA',
                    'postal_code' => '94111',
                    'country' => 'US',
                ],
            ],
        ]);
    }
    public function CreateVirtualCard(string $cardHolderId)
    {
        return Card::Create([
            'cardholder' => $cardHolderId,
            'type' => 'virtual',
            'currency' => 'usd',
        ]);
    }
    public function ActivateCard(string $cardId)
    {
        Card::Update(
            $cardId,
            ['status' => 'active']
        );
    }

    public function RetrieveCard(string $cardId)
    {
        $card =  Card::retrieve([
            'id' => $cardId,
            'expand' => ['number', 'cvc']
        ]);

        return  $card;
    }

    private function AddCustomer($customerDetailsAry)
    {
        $customer = new Customer();
        $customerDetails = $customer->create($customerDetailsAry);
        return $customerDetails;
    }

    private function chargeAmountFromCard($cardDetails)
    {
        $customerDetailsAry = array(
            'email' => $cardDetails->CardHolderEmail,
            'source' => $cardDetails->Token
        );
        $customerResult = $this->AddCustomer($customerDetailsAry);
        $charge = new Charge();
        $cardDetailsAry = array(
            'customer' => $customerResult->id,
            'amount' => ($cardDetails->Amount + $cardDetails->ServiceCharge) * 100,
            'currency' => "USD",
            'description' => "Creating Virtual card from this payment",
            'metadata' => array(
                'order_id' => $cardDetails->OrderId
            )
        );
        $result = $charge->create($cardDetailsAry);
        return $result; //->jsonSerialize();
    }
    public function ChargeAndCreateVirtualCard($params)
    {

        $response = new stdClass();
        $params->ServiceCharge = 5;
        $params->OrderId = uniqid();


        $params = $this->stripeDbService->SaveStripeAditionalDetails($params);

        $this->stripeDbService->saveStripePaymentTransaction($params);
        $paymentDetail = $this->chargeAmountFromCard($params);
        $params->TransactionId =  $paymentDetail->id;
        $params->TransactionResponse =  json_encode($paymentDetail);
        $this->stripeDbService->saveStripePaymentTransaction($params);

        $cardPlaceHolderResult =   $this->CreateCardPlaceHolder($params);
        $params->PlaceHolderType = "individual";
        $params->StripeCardPaceHolderId = $cardPlaceHolderResult->id;
        $params->PlaceHolderStatus = $cardPlaceHolderResult->status;

        $this->stripeDbService->CreateStripeVirtualCardHolder($params);

        $cardResult =  $this->CreateVirtualCard($cardPlaceHolderResult->id);
        $params->StripeCardId = $cardResult->id;
        $params->Brand = $cardResult->brand;
        $params->CardStatus = $cardResult->status;
        $params->CardType = $cardResult->type;
        $this->stripeDbService->CreateStripeVirtualCard($params);
        $this->stripeDbService->UpdateStripeAditionalDetailsStripeId($params->CardDetailId, $params->StripeAditionalDetailsId);
        $this->ActivateCard($cardResult->id);
        $params->CardStatus = "active";
        $this->stripeDbService->CreateStripeVirtualCard($params);
        // $params->CardDetails = $this->RetrieveCard($cardResult->id);

        return $params;
    }

    public function CreateCheckoutSession($customerDetailsAry)
    {
     
        $session = \Stripe\Checkout\Session::create([
            'payment_method_types' => ['card'],
            'client_reference_id'=>$customerDetailsAry->OrderId,
            'line_items' => [[
              'price_data' => [
                'currency' => 'usd',
                'product_data' => [
                  'name' => 'Virutal Gift',
                ],
                'unit_amount' => $customerDetailsAry->Amount*100,
              ],
              'quantity' => 1,
            ]],
            'mode' => 'payment',
            'success_url' => 'https://geogifts.app/stripeCallback.php?tp=success&trnsId='.$customerDetailsAry->SuccessTransId,
            'cancel_url' => 'https://geogifts.app/stripeCallback.php?tp=cancel&trnsId='.$customerDetailsAry->FailureTransId,
          ]);
          $obj = new stdClass();
          $obj->SessionId=$session->id;
        return  $obj;
    }
    
}

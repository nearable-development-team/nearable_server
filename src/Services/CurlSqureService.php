<?php

namespace Services;

use stdClass;

$authorize_url = "https://connect.squareupsandbox.com/oauth2/authorize";
$token_url = "https://connect.squareupsandbox.com/oauth2/token";
$square_api_url = "https://connect.squareupsandbox.com/";
$callback_uri = "https://nearable.app/ServerApi/SquareOAuth.php";
$client_id = "sandbox-sq0idb-XsAxvMt3sf2hl4FOrmnrbA";
$client_secret = "sandbox-sq0csb-yY__KmsC7AuiYvw2F7JXEPiV1-3HYjqQmO8mBVZkFfk";

class CurlSqureService
{

    public function GetAccessToken($authorization_code)
    {
        global $token_url, $client_id, $client_secret, $callback_uri;
        $header = array("Square-Version: 2020-03-25", "Content-Type: application/json");
        $content = new stdClass();
        $content->grant_type = 'authorization_code';
        $content->code = $authorization_code;
        $content->redirect_uri = $callback_uri;
        $content->client_id = $client_id;
        $content->client_secret = $client_secret;

        return json_decode(self::SendCurlPostHttpCall($token_url, $header, $content));
    }

    private function SendCurlPostHttpCall($url, $header, $content)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode($content, true),
        ));
        $response = curl_exec($curl);
        curl_close($curl);

        if ($response === false) {
            echo "Failed";
            echo curl_error($curl);
            echo "Failed";
        } elseif (json_decode($response)->error) {
            echo "Error:<br />";
            echo $response;
        }
        return $response;
    }

    public function GetAPIHttpCall($relative_url, $tokenDetail)
    {
        global $square_api_url;
        $curl = curl_init();
        $header = array("Authorization: Bearer {$tokenDetail->AccessToken}", "Square-Version: 2020-03-25", "Content-Type: application/json");

        curl_setopt_array($curl, array(
            CURLOPT_URL => $square_api_url . $relative_url,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    public function PostAPIHttpCall($relative_url, $tokenDetail, $content)
    {
        global $square_api_url;
        $curl = curl_init();
        $header = array("Authorization: Bearer {$tokenDetail->AccessToken}");

        curl_setopt_array($curl, array(
            CURLOPT_URL => $square_api_url . $relative_url,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode($content, true),
        ));
        $response = curl_exec($curl);
        curl_close($curl);

        if ($response === false) {
            echo "Failed";
            echo curl_error($curl);
            echo "Failed";
        } elseif (json_decode($response)->error) {
            echo "Error:<br />";
            echo $tokenDetail->AccessToken;
            echo $response;
        }
        return $response;
    }
}

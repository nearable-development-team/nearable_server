<?php


namespace Services;

class SessionService
{
    public function  SetUser($user)
    {
        $_SESSION["userDetail"] = json_encode($user);
    }
    public function  GetUser()
    {
        if (isset($_SESSION["userDetail"])) {
            return  json_decode($_SESSION["userDetail"]);
        }
        return null;
    }
    public function  UnSet()
    {
        unset($_SESSION["userDetail"]);
    }

}

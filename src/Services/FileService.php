<?php

namespace Services;

use DataAccessLayer\FileDbService;
use DataAccessLayer\FolderDbService;
use DataAccessLayer\UserFolderDbService;
use Models\File;
use stdClass;
use ViewModels\GetFilesResponse;

class FileService
{
    public function SaveBase64ImageToMultipleFolder($FileUploadRequest)
    {

        $directoryService = new FileSystemService();
        $folderDbService = new FolderDbService();
        $fileDbService = new FileDbService();
        $files = array();
        foreach ($FileUploadRequest->FolderName as $key => $folder) {
            $file = new File();
            $ImageFolderPath = $directoryService->CreateDirectory($folder->AuthorId, $folder->Name);
            $file->FilePath = $this->saveBase64ImagePng($FileUploadRequest->Name, $FileUploadRequest->Base64, $ImageFolderPath);
            $file->FolderId = $folderDbService->GetFolderIdByName($folder->Name);
            $file->UserId = $folder->AuthorId;
            $file->Name = $FileUploadRequest->Name;
            $file->UserFolderId = $folder->UserFolderId;
            $file->UploadedByUserId = $FileUploadRequest->UploadedByUserId;
            $fileDbService->SaveFile($file);
            array_push($files, $fileDbService->SaveFile($file));
        }
        return $files;
    }
    public function SaveBase64Image($FileUploadRequest)
    {
        $file = new File();
        $directoryService = new FileSystemService();
        $folderDbService = new FolderDbService();
        $fileDbService = new FileDbService();

        $ImageFolderPath = $directoryService->CreateDirectory($FileUploadRequest->UserId, $FileUploadRequest->FolderName);

        $file->FilePath = $this->saveBase64ImagePng($FileUploadRequest->Name, $FileUploadRequest->Base64, $ImageFolderPath);
        $file->FolderId = $folderDbService->GetFolderIdByName($FileUploadRequest->FolderName);
        $file->UserId = $FileUploadRequest->UserId;
        $file->Name = $FileUploadRequest->Name;
        $file->UserFolderId = $FileUploadRequest->UserFolderId;
        $file->UploadedByUserId = $FileUploadRequest->UploadedByUserId;

        return $fileDbService->SaveFile($file);
    }
    
    public function SavePattFile($pattCode)
    {
        $imageDir = PATT_FILES_PATH;
        $pattFilesFolder = PATT_FILES_FOLDER;
        $fileName = date('Y-m-d H:i:s', strtotime('now')) . ".patt";
        file_put_contents("{$imageDir}/{$fileName}", $pattCode);
        return "{$pattFilesFolder}/{$fileName}";
    }
    
    public function SaveBase64QRImage($base64Image)
    {
        $fileName = date('Y-m-d H:i:s', strtotime('now')) . ".png";
        $qrFilesFolder = QR_FILES_FOLDER;
        $this->saveBase64ImagePng($fileName, $base64Image, QR_FILES_PATH);
        return "{$qrFilesFolder}/{$fileName}";
    }


    public function DeleteFile($fileDownloadRequest)
    {
        $fileDbService = new FileDbService();
        $file = $fileDbService->GetFileById($fileDownloadRequest->FileId);
        unlink(ROOT_APPLICATION . $file->FilePath);
        $file = $fileDbService->DeleteFileById($fileDownloadRequest->FileId);

        return "deleted";
    }

    public function SaveBase64ImageBySharedId($FileUploadRequest)
    {
        $userFolderDbService = new UserFolderDbService();
        $folder = $userFolderDbService->GetFolderByUniqueId($FileUploadRequest->SharedFolderId);
        $FileUploadRequest->FolderName = $folder->Name;
        $FileUploadRequest->UserId = $folder->UserId;
        $FileUploadRequest->UserFolderId = $folder->UserFolderId;
        return $this->SaveBase64Image($FileUploadRequest);
    }


    public function GetImages($GetImageRequest)
    {
        $fileDbService = new FileDbService();

        $fileReponse = new GetFilesResponse();
        $fileReponse->BasePath = DOMAIN_NAME;
        //$GetImageRequest->FolderId = $folderDbgr Service->GetFolderIdByName($GetImageRequest->FolderName);
        $fileReponse->Images = $fileDbService->GetFilesByUserIdFolderId($GetImageRequest);
        return  $fileReponse;
    }

    public function GetImagesBySharedFolderId($folder)
    {
        $fileDbService = new FileDbService();
        $userFolderDbService = new UserFolderDbService();
        $GetImageRequest = new stdClass();
        $fileReponse = new GetFilesResponse();
        $fileReponse->BasePath = DOMAIN_NAME;
        $folderInfo = $userFolderDbService->GetFolderByUniqueId($folder->SharedFolderId);
        $GetImageRequest->FolderId = $folderInfo->Id;
        $GetImageRequest->UserId = $folderInfo->UserId;
        $fileReponse->Images = $fileDbService->GetFilesByUserIdFolderId($GetImageRequest);
        return  $fileReponse;
    }
    private function saveBase64ImagePng($fileName, $data, $imageDir)
    {
        $type = array('jpg', 'jpeg', 'gif', 'png');
        if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {
            $data = substr($data, strpos($data, ',') + 1);
            $type = strtolower($type[1]); // jpg, png, gif

            if (!in_array($type, ['jpg', 'jpeg', 'gif', 'png'])) {
                throw new \Exception('invalid image type');
            }

            $data = base64_decode($data);

            if ($data === false) {
                throw new \Exception('base64_decode failed');
            }
        } else {
            throw new \Exception('did not match data URI with image data');
        }

        file_put_contents("{$imageDir}/{$fileName}", $data);
        return "{$imageDir}/{$fileName}";
    }
}

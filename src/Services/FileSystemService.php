<?php

namespace Services;

class FileSystemService
{

    public function CreateMerchantDirectory($merchantId)
    {
        $basePath = MERCHANT_DATA_DIRECTORY;
        $this->makeDirectory($basePath . '/' . $merchantId . '/offerImages');
        return $basePath . '/' . $merchantId . '/offerImages';
    }

    public function CreateDirectory($userid, $directoryName)
    {
        $basePath = USER_DATA_DIRECTORY;
        $this->makeDirectory($basePath . '/' . $userid . '/' . $directoryName);
        return $basePath . '/' . $userid . '/' . $directoryName;
    }

    public function makeDirectory($path, $permissions = 0777)
    {
        return is_dir($path) || mkdir($path, $permissions, true);
    }
}

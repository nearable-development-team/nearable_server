<?php

namespace Services;

use DataAccessLayer\MerchantDbService;
use DataAccessLayer\MerchantTokenDbService;
use stdClass;

class MerchantService
{
    public function SaveMerchant($merchant)
    {
        $merchantDb = new MerchantDbService();
        $saveResult = $merchantDb->SaveMerchant($merchant);
        return   $merchantDb->GetMerchantById($saveResult->Id);
    }

    public function LoginMerchant($merchant)
    {
        $merchantDb = new MerchantDbService();
        $loginResult = $merchantDb->GetMerchantByEmailandPassword($merchant->Email, $merchant->Password);
        return $merchantDb->GetMerchantById($loginResult->Id);
    }
    public function SaveMerchantToken($merchantToken)
    {
        $merchantTokenDbService = new MerchantTokenDbService();
        return $merchantTokenDbService->SaveMerchantToken($merchantToken);
    }

    public function IsTokenUpdated($merchant)
    {
        $merchantTokenDbService = new MerchantTokenDbService();
        $merchantTokenDetail = $merchantTokenDbService->GetMerchantTokenByMerchantId($merchant->Id);
        $response = new stdClass();

        $response->IsTokenUpdated = $merchant->TokenUpdateDate != $merchantTokenDetail->UpdatedDate;
        $response->TokenUpdateDate = $merchantTokenDetail->UpdatedDate;
        return $response;
    }
}

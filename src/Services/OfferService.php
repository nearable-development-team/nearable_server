<?php

namespace Services;

use DataAccessLayer\OfferDbService;
use ViewModels\OfferResponse;

class OfferService
{
    public function SaveOffer($offer)
    {
        $offerDb = new OfferDbService();
        $directoryService = new FileSystemService();
        $ImageFolderPath = $directoryService->CreateMerchantDirectory($offer->MerchantId);
        $offer->FilePath = $this->saveBase64ImagePng(rand(), $offer->Image, $ImageFolderPath);
        return $offerDb->SaveOffer($offer);
    }

    private function saveBase64ImagePng($fileName, $data, $imageDir)
    {
        $type = "png";
        if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {
            $data = substr($data, strpos($data, ',') + 1);
            $type = strtolower($type[1]); // jpg, png, gif

            if (!in_array($type, ['jpg', 'jpeg', 'gif', 'png'])) {
                throw new \Exception('invalid image type');
            }

            $data = base64_decode($data);

            if ($data === false) {
                throw new \Exception('base64_decode failed');
            }
        } else {
            throw new \Exception('did not match data URI with image data');
        }

        file_put_contents("{$imageDir}/{$fileName}", $data);
        return "{$imageDir}/{$fileName}";
    }

    public function GetOfferByMerchant($merchant)
    {
        $offerDb = new OfferDbService();
        $response = new OfferResponse();
        $response->BasePath = DOMAIN_NAME;
        $response->Offers = $offerDb->GetOfferByMerchant($merchant->Id);
        return $response;
    }
}

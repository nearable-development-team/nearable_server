<?php

namespace Services;

use Exception;
use SendGrid;

class EmailService
{
    public function  SendEmail($to, $subject, $content)
    {
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom(SEND_GRID_SENDER, SEND_GRID_SENDER);
        $email->setSubject($subject);
        $email->addTo($to, $to);
        $email->addContent("text/plain", $content);
        $email->addContent(
            "text/html",
            $content
        );
        $sendgrid = new SendGrid(SEND_GRID_KEY, array('turn_off_ssl_verification' => true));
        try {
            $response = $sendgrid->send($email);
        } catch (Exception $e) {
            echo 'Caught exception: ' . $e->getMessage() . "\n";
        }
    }
}

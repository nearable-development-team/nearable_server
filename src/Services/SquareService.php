<?php

namespace Services;

use DataAccessLayer\MerchantTokenDbService;
use Services\CurlSqureService;
use stdClass;

class SquareService
{
    function __construct()
    {
    }
    public function GetMerchantDetail($merchantId)
    {
        $merchantTokenDbService = new MerchantTokenDbService();
        $curlSqureService = new CurlSqureService();

        $merchantTokenDetail = $merchantTokenDbService->GetMerchantTokenByMerchantId($merchantId);
        $callResult = json_decode($curlSqureService->GetAPIHttpCall("v2/merchants", $merchantTokenDetail));
        $result = array();
        foreach ($callResult->merchant as $key => $merchant) {
            $resultEntity = new stdClass();
            $resultEntity->BusinessName = $merchant->business_name;
            $resultEntity->Country = $merchant->country;
            $resultEntity->LanguageCode = $merchant->language_code;
            $resultEntity->Currency = $merchant->currency;
            $resultEntity->Status = $merchant->status;
            $resultEntity->MainLocationId = $merchant->main_location_id;
            array_push($result, $resultEntity);
        }

        return $result;
    }

    public function GetLocations($merchant)
    {
        $merchantTokenDbService = new MerchantTokenDbService();
        $curlSqureService = new CurlSqureService();

        $merchantTokenDetail = $merchantTokenDbService->GetMerchantTokenByMerchantId($merchant->Id);
        $callResult = json_decode($curlSqureService->GetAPIHttpCall("v2/locations", $merchantTokenDetail));
        return $callResult;
    }
}

<?php include("shared/authorize.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <?php include("shared/termSection.php.php"); ?>
    <?php include("shared/commonCSS.php"); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body style="margin: 0;">
    <div class="back-arrow">
        <a href="/index.php"><i class="material-icons">arrow_back</i></button>
    </div>
    <section class="map-section">
        <div id="gmap" style="height: 92vh;"></div>
    </section>
    <?php include("shared/footer.php"); ?>
    <script>
        gmapObj = new GoogleMapComponent();
        gmapObj.init(document.querySelector("#gmap")).then(() => {
            UI.LoadMapMarkers(gmapObj);
        });
    </script>
</body>

</html>
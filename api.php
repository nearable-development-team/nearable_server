<?php
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/Configuration.php';

use Models\Response;
use Services\FileService;
use Services\FolderService;
use Services\MerchantService;
use Services\SquareService;
use Services\StripeService;
use Services\UserService;

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
// set_time_limit(300);


class APIEndPoints
{

    private $userService;
    private $folderService;
    private $fileService;
    private $stripeService;
    private $merchantService;
    private $squareService;
    public $type;
    function __construct()
    {
        $this->userService = new UserService();
        $this->folderService = new FolderService();
        $this->fileService = new FileService();
        $this->stripeService = new StripeService();
        $this->merchantService = new MerchantService();
        $this->squareService = new SquareService();
    }

    public function ProcessRequest($type, $decoded_params)
    {
        $response  = new stdClass();
        /**
         * 0=ok,
         * 1=invalid,
         * 2=sessionOut,
         * 3=error,
         */
        $Status = 0;
        $Message = "";
        $pageSize = 20;

        try {
            switch (strtolower($type)) {

                case "adduser": //add  user
                    {
                        $response->Result = $this->userService->SaveUser($decoded_params);
                        $response->Status = 1;
                        break;
                    }
                case "getfoldersbyuser": // get all folders by User
                    {
                        try {
                            $response->Result = $this->folderService->GetFoldersByUser($decoded_params);
                            $response->Status = 1;
                            break;
                        } catch (Exception $th) {
                            print_r($th);
                        }
                    }
                case "addfolderbyuser": //add folder by user
                    {
                        $response->Result = $this->folderService->AddFolderByUser($decoded_params);
                        $response->Status = 1;
                        break;
                    }
                case "addfolderbyuserbulk": //add folder by user
                    {
                        $mrks = $this->folderService->AddFolderInBulkByUser($decoded_params);
                        $response->Result = $mrks;
                        $response->Status = 1;
                        break;
                    }
                case "uploadbase64file": // uploadimage in base64
                    {
                        $response->Result = $this->fileService->SaveBase64Image($decoded_params);
                        $response->Status = 1;
                        break;
                    }
                case "uploadbase64filetomultifolder": // uploadimage in base64
                    {
                        $response->Result = $this->fileService->SaveBase64ImageToMultipleFolder($decoded_params);
                        $response->Status = 1;
                        break;
                    }

                case "deletefile": // download file
                    {
                        $response->Result = $this->fileService->DeleteFile($decoded_params);
                        $response->Status = 1;
                        break;
                    }

                case "getimages": // Get Images by Folder Unique Id
                    {
                        try {
                            $response->Result = $this->fileService->GetImages($decoded_params);
                            $response->Status = 1;
                            break;
                        } catch (\Throwable $th) {
                            print_r($th);
                        }
                    }

                case "searchsharedfolderid": // search by folder Id
                    {
                        $folder = $this->folderService->SearchFolderBySharedId($decoded_params->SharedFolderId);
                        $rs = new stdClass();
                        $rs->Name = $folder->Name;
                        $rs->SharedFolderId = $folder->SharedFolderId;
                        $response->Result = $rs;
                        $response->Status = 1;
                        break;
                    }

                case "getimagebyuniquefolderid": // Get Images by Folder Unique Id
                    {
                        $response->Result = $this->fileService->GetImagesBySharedFolderId($decoded_params);
                        $response->Status = 1;
                        break;
                    }
                case "uploadbase64filebysharedid": // uploadimage in base64
                    {
                        $response->Result = $this->fileService->SaveBase64ImageBySharedId($decoded_params);
                        $response->Status = 1;
                        break;
                    }

                case "addfolder": // add folder
                    {
                        $response->Result = $this->folderService->AddFolder($decoded_params);
                        $response->Status = 1;
                        break;
                    }

                case "joinbyunquiefolderid": // add folder
                    {
                        $response->Result = $this->folderService->JoinByUnquieFolderId($decoded_params);
                        $this->folderService->AddFolderInBulkByUser($decoded_params->Markers);
                        $response->Status = 1;
                        break;
                    }
                case "archivefolder": // archive folder
                    {
                        $response->Result = $this->folderService->ArchiveFolder($decoded_params);
                        $response->Status = 1;
                        break;
                    }
                case "getfolderbyid": // getfolderbyid
                    {
                        $response->Result = $this->folderService->GetFolderById($decoded_params);
                        $response->Status = 1;
                        break;
                    }

                case "getfolderbylocation": // get folder by location
                    {
                        $response->Result = $this->folderService->GetFoldersByLocation($decoded_params);
                        $response->Status = 1;
                        break;
                    }

                case "getmapmarkericons": // get folder by location
                    {
                        $dir = "../map-marker-icons";
                        $response->Result = $this->dirToArray($dir);
                        $response->Status = 1;
                        break;
                    }
                case "addmerchant": // Add Merchant
                    {

                        $result = $this->merchantService->SaveMerchant($decoded_params);
                        if ($result->Id > 0) {
                            $response->Result = $result;
                            $response->Status = 1;
                        } else {
                            $response->Message = "Email already exists!! try logging in.";
                            $response->Status = 3;
                        }

                        break;
                    }

                case "loginmerchant": // Login Merchant
                    {
                        $result = $this->merchantService->LoginMerchant($decoded_params);
                        if ($result != null) {
                            $response->Result = $result;
                            $response->Status = 1;
                        } else {
                            $response->Message = "Email/Password is wrong!!";
                            $response->Status = 3;
                        }
                        break;
                    }

                case "addoffer": // Add Offer
                    {
                        $result = $this->offerService->SaveOffer($decoded_params);
                        if ($result->Id > 0) {
                            $response->Result = $result;
                            $response->Status = 1;
                        } else {
                            $response->Message = "Can't Save";
                            $response->Status = 3;
                        }

                        break;
                    }

                case "getofferbymerchant": // Get Offer
                    {
                        $response->Result = $this->offerService->GetOfferByMerchant($decoded_params);
                        $response->Status = 1;
                        break;
                    }
                case "getmerchantsquaredetail": // get merchant Square Detail
                    {
                        $response->Result = $this->squareService->GetMerchantDetail($decoded_params->Id);
                        $response->Status = 1;
                        break;
                    }
                case "istokenupdated": // get merchant Square Detail
                    {
                        $response->Result = $this->merchantService->IsTokenUpdated($decoded_params);
                        $response->Status = 1;
                        break;
                    }

                case "getsquarelocations": // Get Square Location
                    {
                        $response->Result = $this->squareService->GetLocations($decoded_params);
                        $response->Status = 1;
                        break;
                    }

                case "chargeandcreatevirtualcard": // Charge And Create VirtualCard
                    {

                        $response->Result = $this->stripeService->ChargeAndCreateVirtualCard($decoded_params);
                        $folderRequest = new stdClass();
                        $folderRequest->FolderName =    $decoded_params->PlaceName;
                        $folderRequest->UserId = $decoded_params->UserId;
                        $folderRequest->Latitude = $decoded_params->Latitude;
                        $folderRequest->Longitude = $decoded_params->Longitude;
                        $folderRequest->IsPrivateFence = 1;
                        $folderRequest->FenceAreaInFeet = 0;
                        $folderRequest->UserId = $decoded_params->UserId;

                        $this->folderService->AddFolderByUser($folderRequest);
                        $response->Status = 1;
                        break;
                    }

                case "authenticatefolder": // Authenticate Folder
                    {
                        $response->Result = $this->folderService->AuthenticateFolder($decoded_params);
                        $response->Status = 1;
                        break;
                    }
                case "createcheckoutsession": // createcheckoutsession
                    {
                        $response->Result = $this->folderService->PreparePaymentTransaction($decoded_params);
                        $response->Status = 1;
                        break;
                    }
                case "confirmuseremailid": // ConfirmUserEmailId
                    {
                        $response->Result = $this->userService->ConfirmUserEmail();
                        $response->Status = 1;
                        break;
                    }

                case "getqrimage": // getQRImage
                    {
                        $response->Result = "/".$this->folderService->SearchFolderBySharedId($decoded_params->UniqueSharedId);
                        $response->Status = 1;
                        break;
                    }
                default: {
                        $response = new Response();
                        $response->Status = 1;
                        $response->Message = "Invalid request";
                    }
            }
        } catch (Exception $exception) {
            $Message = $exception->getMessage();
            print_r($exception);
        };
        return $response;
    }

    public function isValidJSON($str)
    {
        json_decode($str);
        return json_last_error() == JSON_ERROR_NONE;
    }

    public function dirToArray($dir)
    {

        $result = array();

        $cdir = scandir($dir);
        foreach ($cdir as $key => $value) {
            if (!in_array($value, array(".", ".."))) {
                if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) {
                    $result[$value] = $this->dirToArray($dir . DIRECTORY_SEPARATOR . $value);
                } else {
                    $result[] = $value;
                }
            }
        }

        return $result;
    }

    public static function EchoJsonResponse($response)
    {
        header("Access-Control-Allow-Origin: *");
        header('Access-Control-Allow-Credentials: true');

        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        echo json_encode($response);
    }
}


$api = new  APIEndPoints();
$decoded_params = new stdClass();


$json_params = file_get_contents("php://input");
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $api->type = $_GET["type"];
    if (strlen($json_params) > 0 && $api->isValidJSON($json_params)) {
        $decoded_params = json_decode($json_params);
    }
} else if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET["type"])) {
    $api->type = $_GET["type"];
}

$api->EchoJsonResponse($api->ProcessRequest($api->type, $decoded_params));

<?php include("shared/authorize.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include("shared/termSection.php.php"); ?>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Geo gift QR code Scanner</title>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />

    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;500&display=swap" rel="stylesheet">
    <!-- Tool used 
https://github.com/mebjas/html5-qrcode -->
    <script src="/lib/js/html5-qrcode.min.js"></script>
    <style>
        .fas {
            display: inline-block;
            padding: 5px;
            font-size: 30px;
        }

        .scanner-section {
            width: 95%;
            margin: 1% auto;
        }

        .cntr {
            width: 99%;
        }
    </style>

</head>

<body>
    <a href="/" style="position: absolute;margin: 2.5% 0% 0% 2%;color:white;"><i class="fas fa-arrow-left" style="font-size: 20px;"></i></a>
    <h5 class="center" style="margin: 0%;background-color: #5a27c4;padding: 3%;color: white;">Scan QR Code</h5>
    <div class="cntr" style="background-color: white;padding: 10px 20px;">

        <div class="center" style="margin: 2% auto 10% auto;"><span style="color: gray;">
                Place QR code inside the frame to scan. Please avoid shake to get result quickly
            </span></div>


        <div class="scanner-section">
            <div id="reader" style="position: relative;"><video muted="true" playsinline="" style="width: 301px;"></video><canvas id="qr-canvas" width="301" height="226" style="width: 301px; height: 226px; display: none;"></canvas></div>
        </div>
        <div class="center" style="color:gray">
            Scaning Code...
        </div>
        <div style="margin-top:20px;color: #5a27c4;" class="center">
            <i class="fas fa-camera"></i>
            <i class="fas fa-qrcode"></i>
            <i class="fas fa-barcode"></i>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script>
        const html5QrCode = new Html5Qrcode("reader");
        const qrCodeSuccessCallback = (decodedText, decodedResult) => {
            html5QrCode.stop();
            location.href = decodedText

        };
        const config = {
            fps: 10,
            qrbox: 250
        };


        // If you want to prefer back camera
        html5QrCode.start({
            facingMode: "environment"
        }, config, qrCodeSuccessCallback);
    </script>
</body>

</html>
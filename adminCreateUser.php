<?php
require __DIR__ . '/vendor/autoload.php';
require_once(__DIR__ . '/Configuration.php');

use Common\Helper;
use Services\SessionService;
use Services\UserService;

$sessionService = new SessionService();

//redirect to login page if user not admin
if ($sessionService->GetUser() == null) {
	header("Location: login.php");
	exit();
}

$userService = new UserService();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if ($_POST["user_password"] != $_POST["user_confirm_password"]) {
		$message = 'Password and Confirm Password are not same.';
	} else {
		//save Login detail
		$user = new stdClass();
		$user->name =  $_POST["user_name"];
		$user->email =  $_POST["user_email"];
		$user->password =  md5($_POST["user_password"]);
		$userDetail =    $userService->CreateAdminUser($user);
		$message = 'New Admin User Created Successfully.';
	}
}


?>
<!DOCTYPE html>
<html>
<title>W3.CSS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="css/w3.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
	.page-content {
		margin-left: 25%
	}

	@media screen and (max-width: 600px) {
		.page-content {
			margin-left: 41%
		}
	}
</style>

<body>

	<!-- Sidebar -->
	<div class="w3-sidebar w3-light-grey w3-bar-block" style="width:25%">
		<h3 class="w3-bar-item"><i class="fa fa-bars"></i> Menu</h3>
		<a href="adminLoginLog.php" class="w3-bar-item w3-button w3-border-top"><i class="fa fa-history"></i> Login Log</a>
		<a href="adminUsers.php" class="w3-bar-item w3-button"><i class="fa fa-user"></i> Users</a>
		<a href="adminCreateUser.php" class="w3-bar-item w3-button w3-black"><i class="fa fa-user"></i>Create User</a>
		<a href="adminPaymentTransaction.php" class="w3-bar-item w3-button"><i class="fa fa-exchange"></i> Payment Transactions</a>
		<a href="logout.php" class="w3-bar-item w3-button"><i class="fa fa-sign-out"></i> Logout</a>
	</div>

	<!-- Page Content -->
	<div class="page-content">

		<div class="w3-container w3-white w3-border-bottom">
			<h1>Admin Create User</h1>
		</div>

		<!-- <img src="img_car.jpg" alt="Car" style="width:100%"> -->

		<div class="w3-container">
			<h2>Enter Login Detail</h2>
			<?php if (isset($message)) {
				$alert = new stdClass();
				$alert->type = 'success';
				$alert->message = $message;
				echo Helper::showAlert($alert);
			} ?>
		</div>

		<div class="w3-container w3-half w3-margin-top">

			<form class="w3-container w3-card-4" method="post">

				<p>
					<input class="w3-input" type="text" name="user_name" style="width:90%" autofocus required>
					<label>Name</label>
				</p>
				<p>
				<p>
					<input class="w3-input" type="email" name="user_email" style="width:90%" autofocus required>
					<label>Email</label>
				</p>
				<p>
					<input class="w3-input" type="password" name="user_password" style="width:90%" required>
					<label>Password</label>
				</p>
				<p>
					<input class="w3-input" type="password" name="user_confirm_password" style="width:90%" required>
					<label>Confirm Password</label>
				</p>

				<p>
					<button class="w3-button w3-section w3-teal w3-ripple"> Create </button>
				</p>

			</form>

		</div>

	</div>

	<script src="lib/js/jquery.min.js"></script>
	<script type="text/javascript">
		var uri = window.location.href.toString();
		if (uri.indexOf("?") > 0) {
			var clean_uri = uri.substring(0, uri.indexOf("?"));
			window.history.replaceState({}, document.title, clean_uri);
		}

		$(document).ready(function() {
			setTimeout(function() {
				$('.w3-card-2').slideUp();
			}, 3000);
		});
	</script>
</body>

</html>
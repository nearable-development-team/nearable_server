<?php

require __DIR__ . '/vendor/autoload.php';
require_once(__DIR__ . '/Configuration.php');
include("shared/header.php");
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use Services\FolderService;
use Services\QRCodeService;

$folderService = new FolderService();
$qrCodeService = new QRCodeService();

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php include("shared/commonCSS.php"); ?>
</head>

<body class="thanks-screen">


	<?php


	if (isset($_GET["tp"]) && isset($_GET["trnsId"])) {

		if ($_GET["tp"] == "success") {
			$trs = new stdClass();
			$trs->SuccessTransId = $_GET["trnsId"];
			$updateResponse =	$folderService->UpdatePaymentTransactionStatusBySuccess($trs);
	?>

			<div class="thank-container">
				<div class="thank-info">
					<img class="thumb" src="images/order_confirmed.svg">
					<i></i>
					<h2>Thank You</h2>

					<?php
					if ($updateResponse->Status == "alreadyUpdated") {
						echo '<label>Transaction already proccessed</label>';
					}

					?>
					<p>Your Transaction ID</p>
					<label><?php echo $_GET["trnsId"]; ?></label>
					<?php if ($updateResponse->GiftShareType == "QR") { ?>
						<img style="display: block; width: 100%;" src="<?php echo "/".$updateResponse->QRFileLocation; ?>">
					<?php } ?>
					<a href="index.php" class="waves-effect waves-light btn t-bg t-bg-action">Back to home</a>

				</div>


			</div>
		<?php

		} else {
			$trs = new stdClass();
			$trs->FailureTransId = $_GET["trnsId"];
			$folderService->UpdatePaymentTransactionStatusByFailure($trs);
		?>


			<div class="thank-container">
				<div class="thank-info">
					<img class="thumb" src="images/order_confirmed.svg">
					<i></i>
					<h2>!! Payment has been canceled !!</h2>
					<p>Your Transaction ID</p>
					<label><?php echo $_GET["trnsId"]; ?></label>
					<a href="index.php" class="waves-effect waves-light btn t-bg t-bg-action">Back to home</a>

				</div>


			</div>


		<?php
		}
	} else {
		?>

		<div class="thank-container">
			<div class="thank-info">
				<img class="thumb" src="images/order_confirmed.svg">
				<i></i>
				<h2>!! Invalid Url !!</h2>
				<p>Your Transaction ID</p>
				<label><?php echo $_GET["trnsId"]; ?></label>
				<a href="index.php" class="waves-effect waves-light btn t-bg t-bg-action">Back to home</a>

			</div>


		</div>


	<?php

	}

	?>


</body>

</html>
<?php include("shared/authorize.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <?php include("shared/termSection.php.php"); ?>
    <?php include("shared/commonCSS.php"); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body style="margin: 0;" onload="UI.LoadFolders()">
    <div class="navbar-fixed">
        <nav class="nav-extended">
            <div class="nav-wrapper">
                <a class="back-arrow t-color" href="/index.php"><i class="material-icons">arrow_back</i></a>

                <div class="inputfield">
                    <input id="folderKeywordFilter" type="search" placeholder="search" onkeyup="UI.FolderKeywordFilter()">
                    <i class="material-icons">search</i>
                </div>

            </div>
            <div class="nav-content cstab-bar">
                <ul class="tabs ">
                    <li class="tab" onclick="UI.FolderTabFilter('')"><a href="#folderList" class="active">All</a></li>
                    <li class="tab" onclick="UI.FolderTabFilter('archived')"><a href="#arc">Archived</a></li>
                    <li class="tab " onclick="UI.FolderTabFilter('sharedOnly')"><a href="#shr">Shared</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <section class="folder-section" style="margin-top: 70px;">
        <div id="folderList" class="row">
        </div>
    </section>

    <?php include("shared/footer.php"); ?>
</body>

</html>